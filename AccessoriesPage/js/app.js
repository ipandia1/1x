element = "";

(function() {
    'use strict';
    var app = angular.module('app', ['ngAnimate', 'ngAria', 'ngAria', 'ngCookies', 'ngMessages', 'ngSanitize', 'ngTouch', 'ui.bootstrap', 'ngMaterial']);
	
	angular.module("uib/template/modal/window.html", []).run(["$templateCache", function($templateCache) {
	  $templateCache.put("uib/template/modal/window.html",
		"  <button type=\"button\" class=\"close pull-right uib-close\" ng-click=\"close($event)\"><i aria-hidden=\"true\" class=\"ico-closeIcon uib-close\" ng-click=\"close($event)\"></i></button><div class=\"modal-dialog {{size ? 'modal-' + size : ''}}\"><div class=\"modal-content\" uib-modal-transclude></div></div>\n" +
		"");
	}]);

	 
    app.controller('generalController', ['$scope', function($scope) {

    }])
    
	
	.controller('custommodalcontroller', ['$scope', function($scope) {
          
		  var modalID = document.getElementById('custom-myModal');
		  
		  $scope.closeCustomModal = function($scope)
		  {
			  modalID.style.display = "none";
		  }
            
    }])

 .controller('checkAutopay', ['$scope', function($scope) {
          
		  var autopayoff = 1;
		  $scope.imgfile="img/on.jpg";
          $scope.changeautopay = function() {
			if(autopayoff == 1)
			{
			$scope.imgfile="img/off.jpg";
			autopayoff = 0;
			}
			else
			{
			$scope.imgfile="img/on.jpg";
			autopayoff = 1;
			}
		};
            
    }])
	 .controller('checkAutopayoff', ['$scope', function($scope) {
		  var autopayoff = 0;
		  $scope.imgfile="img/off.jpg";
          $scope.changeautopay = function() {
			if(autopayoff == 1)
			{
			$scope.imgfile="img/off.jpg";
			autopayoff = 0;
			}
			else
			{
			$scope.imgfile="img/on.jpg";
			autopayoff = 1;
			}
		};
            
    }])
  .controller('DropdownController', function ($scope) {
			
        $scope.value=true;
        $scope.value2=false;
			
        $scope.DropDownValue=function(val){
            $scope.selectedVal=val;
            $scope.value=false;
            $scope.value2=true;
        }
  })
        .controller('CarouselDemoCtrl', ['$scope', function ($scope) {
            $scope.myInterval = 5000;
            $scope.noWrapSlides = false;
            $scope.active = 0;
            var slides = $scope.slides = [];
            var currIndex = 0;
            $scope.addSlide = function () {
                slides.push({
                    image: 'img/device.png',
                    //text: ['Nice image', 'Awesome photograph', 'That is so cool'][slides.length % 3],
                    id: currIndex++
                });
            };

            for (var i = 0; i < 4; i++) {
                $scope.addSlide();
            }

            $scope.next = function () {
                alert("next");
            }

        }])
  .controller('ModalDemoCtrl1', ['$scope', function($scope)  {
      $scope.displayModal = false;
     $scope.openModal = function() {
     $scope.displayModal = true;
      $scope.ModalBlack = {
           "background":"rgba(0,0,0,0.9)"
          }

        };

              $scope.closeModal = function() {
                   $scope.displayModal = false;
                   $scope.ModalBlack = {
                   "background": "rgba(255,255,255)"

                  }

        };

 

    }])
.controller('radioController', ['$scope', function ($scope) {
    $scope.isSelected1 = false;
    $scope.isSelected2 = false;
    $scope.radioVal1 = false;
    $scope.radioVal2 = false;
    $scope.radioisSelected = true;

    $scope.checked = function (checkOption) {
        if (checkOption == "option1") {
            $scope.isSelected1 = true;
        }
        else {
            $scope.isSelected1 = false;
        }


        if (checkOption == "option2") {
            $scope.isSelected2 = true;
        }
        else {
            $scope.isSelected2 = false;
        }

    }
    $scope.setValue = function (event) {
        $scope.radioisSelected = false;
        if (event.target.value == "option1") {
            $scope.isSelected1 = true;
        }
        else {
            $scope.isSelected1 = false;
        }
        if (event.target.value == "option2") {
            $scope.isSelected2 = true;
        }
        else {
            $scope.isSelected2 = false;
        }
    }

    $scope.showContent = function (id) {
        if (id == "div1") {
            $scope.radioVal1 = !$scope.radioVal1;
            if ($scope.radioVal1 == true) {
                $scope.expand1 = true;
                $scope.expand2 = false;
            }
            else {
                $scope.expand1 = false;
            }       
        }
        else {
            $scope.radioVal1 = false;
            $scope.expand1 = false;
        }
        if (id == "div2") {
            $scope.radioVal2 = !$scope.radioVal2;
            if ($scope.radioVal2 == true) {
                $scope.expand2 = true;
                $scope.expand1 = false;
            }
            else {
                $scope.expand2 = false;
            }
        }
        else {
            $scope.radioVal2 = false;
            $scope.expand2 = false;
        }
    }

}])


        .controller('colorPickerController', ['$scope', function ($scope) {
            $scope.isSelected1 = false;
            $scope.isSelected2 = false;
            $scope.isSelected3 = false;
            $scope.isSelected4 = false;
            $scope.isSelected5 = false;
            $scope.isSelected6 = false;
            $scope.colorisSelected = true;
            $scope.setValue = function (event) {
                $scope.colorisSelected = false;
                if (event.target.value == "space_gray") {
                    $scope.isSelected1 = true;
                }
                else {
                    $scope.isSelected1 = false;
                }
                if (event.target.value == "light_gray") {
                    $scope.isSelected2 = true;
                }
                else {
                    $scope.isSelected2 = false;
                }
                if (event.target.value == "gold") {
                    $scope.isSelected3 = true;
                }
                else {
                    $scope.isSelected3 = false;
                }
                if (event.target.value == "rose_gold") {
                    $scope.isSelected4 = true;
                }
                else {
                    $scope.isSelected4 = false;
                }
                if (event.target.value == "rose_gold2") {
                    $scope.isSelected5 = true;
                }
                else {
                    $scope.isSelected5 = false;
                }
                if (event.target.value == "rose_gold3") {
                    $scope.isSelected6 = true;
                }
                else {
                    $scope.isSelected6 = false;
                }
            }
            $scope.color1=function(){
                $scope.isSelected1 = true;
            }

        }])


	  .controller('ModalDemoCtrl1', ['$scope', function($scope)  {
	    $scope.displayModal = false;
		$scope.openModal = function() {
		    $scope.displayModal = true;
		    $scope.ModalBlack = {
		        "background-color":"black"
		    }
        };
		$scope.closeModal = function() {
		    $scope.displayModal = false;
		    $scope.ModalBlack = {
		        "background-color": "white"
		    }
        };

    }])

    .controller('ModalInstanceCtrl', ['$scope', '$uibModalInstance', function($scope, $uibModalInstance) {

        $scope.ok = function() {
            $uibModalInstance.close();
        };

        $scope.cancel = function() {
            $uibModalInstance.dismiss('cancel');
        };
    }])
	
	.controller('styleCtrl', ['$scope', function($scope) {

	    $scope.increment=105;
		
		$scope.initialWidth=0;
		$scope.initialMarginLeft=-239;
		
		$scope.newWidth=0+$scope.increment;
		$scope.newMarginLeft=(-239+$scope.increment);
		
		
		$scope.mywidth= {
        "width":$scope.newWidth+"px "
		}
		
		$scope.customMarginLeft= {
        "margin-left":$scope.newMarginLeft+"px"
		}
		
		
    }])

    .controller('scrollCtrl', ['$scope', '$element', function($scope, $element) {

        $scope.visibleElement = true;

        angular.element(window).bind("scroll", function() {
             //console.log("scrolling screen"+window.innerHeight);
             
             element = document.getElementById('stickyfooter');

            if(element.getBoundingClientRect().top+40<=window.innerHeight)
            {
                $scope.visibleElement = false;
                 //console.log("scrolling Ele "+$scope.visibleElement );  
                $scope.$apply();
            }
            else
            {
                $scope.visibleElement = true;
                 //console.log("scrolling Ele "+$scope.visibleElement );  
                $scope.$apply();
            }
        });
    }])

    app.controller('accessoriesCtrl', ['$scope', '$http', function ($scope, $http) {
        $http.get("./json/accessories.json").then(function (response) {
            $scope.accessories = response.data.accessories;
        });

         $scope.strikeText = function (strikeText) {
           return $scope.strikeAccessoryemi = "<del>" + strikeText + "</del>"
        };
        $scope.footerHide = false;
        $scope.checkboxClick = function (checked) {
           
                 $scope.footerHide = true;
           
         };
    }])
    
        app.controller('accessoriesCtrlnoemi', ['$scope', '$http', function ($scope, $http) {
        $http.get("./json/accessories-no-eip.json").then(function (response) {
            $scope.accessories = response.data.accessories;
        });

         $scope.strikeText = function (strikeText) {
           return $scope.strikeAccessory = "<del>" + strikeText + "</del>"
        };
    }])

    app.controller('cartTabsCtrl', ['$scope', '$window', function ($scope,$window) {
        $scope.selectedTabIndex = 0;
        $scope.orderDetailViewed = false;
        $scope.shippingPageViewed = false;
        $scope.paymentPageViewed = false;
        $scope.newAddress = false;
        $scope.existingAddress = true;
        $scope.existingPromoCode = true;
        $scope.hide = false;
        $scope.checked = true;
        $scope.checked1 = true;

        $scope.checkboxClick = function () {
           
                $scope.hide = !$scope.hide;
                $scope.checked = !$scope.checked;
            
        };

        $scope.add = function () {
            $scope.newAddress = true;
            $scope.existingAddress = false;
        }
        $scope.addPromoCode = function () {
            $scope.promoCode = true;
            $scope.existingPromoCode = false;
        }
        $scope.applyCode = function () {
            $scope.successText = true;
            $scope.promoCode = true;
        }

        $scope.page1 = false;
        $scope.page2 = false;
        $scope.page3 = false;
        
        $scope.accordianData = [{
            "heading": "1. Order detail",
            "check": false,
            "id": "0"
        }, {
            "heading": "2. Shipping",
            "check": false,
            "id": "1"
        }, {
            "heading": "3. Payment",
            "check": false,
            "id": "2"
        }];

        $scope.onLoadFun = function (data) {
            for (var i in $scope.accordianData) {
                if ($scope.accordianData[0] != data) {
                    $scope.accordianData[0].expanded = true;
                    $scope.page1 = true;
                }
            }
        }

        $scope.collapseAll = function (data) {

            for (var i in $scope.accordianData) {
                if ($scope.accordianData[i] != data) {
                    $scope.accordianData[i].expanded = false;
                }
            }

            if ($scope.accordianData[0].heading == data.heading) {
                $scope.accordianData[0].expanded = true;
                $scope.page1 = true;
                $scope.page2 = false;
                $scope.page3 = false;
            }
            else if ($scope.accordianData[1].heading == data.heading) {
                $scope.accordianData[1].expanded = true;

                $scope.page1 = false;
                $scope.page2 = true;
                $scope.page3 = false;
            }
            else if ($scope.accordianData[2].heading == data.heading) {
                $scope.accordianData[2].expanded = true;
                $scope.page1 = false;
                $scope.page2 = false;
                $scope.page3 = true;
            }
        };

        $scope.moveToShipping = function () {
            if ($window.innerWidth < 768) { /*Accordian View Logic for Mobile*/
                $scope.accordianData[0].expanded = false;
                $scope.accordianData[1].expanded = true;
                $scope.page1 = false;
                $scope.page2 = true;
                $scope.page3 = false;
                $scope.accordianData[0].check = true;
                $scope.accordianData[0].heading = "Order detail";
            } else { /*Tab View Logic for Tablets, desktops*/
                $scope.orderDetailViewed = true;
                $scope.selectedTabIndex = 1;
            }
            
         }

         $scope.moveToPayment = function () {
             if ($window.innerWidth < 768) { /*Accordian View for Mobile*/ 
                console.log($window.innerWidth);
                $scope.accordianData[1].expanded = false;
                $scope.accordianData[2].expanded = true;
                $scope.page1 = false;
                $scope.page2 = false;
                $scope.page3 = true;
                $scope.accordianData[1].check = true;
                $scope.accordianData[1].heading = "Shipping";
             } else {  /*Tab View Logic for Tablets, desktops*/
                $scope.shippingPageViewed = true;
                $scope.selectedTabIndex = 2;
             }
         }

         $scope.checkMark = function (labelText) {
             return "<i class='fa fa-check color-green'></i> " + labelText;
         };
         
    }])

    app.controller('accessoriesCarouselCtrl', ['$scope', '$http', function ($scope, $http) {
        $scope.offers = "";
        $scope.offerDataIndex = 0;
        $scope.offerMaxIndex = 0;
        $http.get("./json/offers.json").then(function (response) {
            $scope.offers = response.data.offers;
            $scope.offerDataDesc = $scope.offers[$scope.offerDataIndex].desc;
            $scope.offerDataUrl = $scope.offers[$scope.offerDataIndex].url;
            $scope.offerMaxIndex = $scope.offers.length - 1;
        });
        
        $scope.moveToNext = function () {
            
            if($scope.offerDataIndex == $scope.offerMaxIndex) {
                $scope.offerDataIndex = -1;
            }
                $scope.offerDataIndex++;
                $scope.offerDataDesc = $scope.offers[$scope.offerDataIndex].desc;
                $scope.offerDataUrl = $scope.offers[$scope.offerDataIndex].url;
                
             
          };
          $scope.moveToPrevious = function () {
              if($scope.offerDataIndex == 0) {
                $scope.offerDataIndex = $scope.offerMaxIndex + 1;
              }
              $scope.offerDataIndex--;
                $scope.offerDataDesc = $scope.offers[$scope.offerDataIndex].desc;
                $scope.offerDataUrl = $scope.offers[$scope.offerDataIndex].url;
                
             
         };
    }])

    app.controller('accessoriesFooterCtrl', ['$scope',  function ($scope) {
        $scope.footerHide = false;
        $scope.checkboxAccessories = function () {
            $scope.footerHide = !$scope.footerHide;
        };

        $scope.payMonthly=true;
        $scope.payFull=false;
        $scope.payMonthlyFunction=function(){
            $scope.payMonthly=true;
            $scope.payFull=false;
        }
        $scope.payFullFunction=function(){
            $scope.payMonthly=false;
            $scope.payFull=true;
        }
    }])
    app.directive('setHeightPayment', function ($window) {
        return{
            link: function (scope, element, attrs) {
                scope.winHeight = function () {
                    if($window.innerWidth >= 768)
                    {
                        element.css('min-height',  ($window.innerHeight-366)+'px');
                    }
                }
                scope.winHeight();
                
                angular.element(window).bind('resize', function () {
                    scope.winHeight();
                    scope.$digest();
                }); 
            }
        }
    })
    app.directive('setHeightShipping', function ($window) {
        return{
            link: function (scope, element, attrs) {
                scope.winHeight = function () {
                    if($window.innerWidth >= 768)
                    {
                        element.css('min-height',  ($window.innerHeight-144)+'px');
                    }
                }
                scope.winHeight();
                
                angular.element(window).bind('resize', function () {
                    scope.winHeight();
                    scope.$digest();
                }); 
            }
        }
    })
    app.directive('setHeightOrder', function ($window) {
        return{
            link: function (scope, element, attrs) {
                scope.winHeight = function () {
                    if($window.innerWidth >= 768)
                    {
                        element.css('min-height',  ($window.innerHeight-144)+'px');
                    }
                }
                scope.winHeight();
                
                angular.element(window).bind('resize', function () {
                    scope.winHeight();
                    scope.$digest();
                }); 
            }
        }
    })
    app.directive('setHeightScroll', function ($window) {
        return{
            link: function (scope, element, attrs) {
                scope.winHeight = function () {
                    if($window.innerWidth >= 768)
                    {
                        element.css('min-height',  ($window.innerHeight-194)+'px');
                    }
                }
                scope.winHeight();
                
                angular.element(window).bind('resize', function () {
                    scope.winHeight();
                    scope.$digest();
                }); 
            }
        }
    })
    app.controller('accessoriesPdpCtrl', ['$scope', '$http','$window', function ($scope, $http,$window) {
	   // $scope.myInterval = 5000;
	    $scope.noWrapSlides = false;
	    $scope.active = 0;
	    var slides = $scope.slides = [];
	    var currIndex = 0;
	    var windowHeight = $scope.windowHeight = $window.innerHeight;
	    var windowwidth = $scope.windowwidth = $window.innerWidth;
	    //alert(windowHeight);
	    //alert($window.innerWidth);
	    //alert($scope.windowHeight);
	    //alert($scope.windowwidth);
	    if ($scope.windowwidth >= 767) {
	      
	            $scope.myStyle = {
	                //"max-height": ($scope.windowHeight - 120) + "px",
	                //"overflow": "auto"
	            }
	        }
	        else {
	            $scope.myStyle = {
	                "max-height": "100%",
	                "overflow": "none"
	            }
	        }
	    


	   	    
	    $http.get("./json/accessories-pdp.json").then(function (response) {
	        $scope.slides = response.data.slideimages;
	    });
	}])

})();

