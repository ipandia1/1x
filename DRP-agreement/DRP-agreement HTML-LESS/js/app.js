(function() {
    'use strict';
    var app = angular.module('app', ['ngAnimate', 'ngAria', 'ngAria', 'ngCookies', 'ngMessages', 'ngSanitize', 'ngTouch', 'ui.bootstrap']);
	
	angular.module("uib/template/modal/window.html", []).run(["$templateCache", function($templateCache) {
	  $templateCache.put("uib/template/modal/window.html",
		"  <button type=\"button\" class=\"close pull-right uib-close\" ng-click=\"close($event)\"><i aria-hidden=\"true\" class=\"ico-closeIcon uib-close\" ng-click=\"close($event)\"></i></button><div class=\"modal-dialog {{size ? 'modal-' + size : ''}}\"><div class=\"modal-content\" uib-modal-transclude></div></div>\n" +
		"");
	}]);
	
    app.controller('generalController', ['$scope', function($scope) {

    }])
    
	
	.controller('custommodalcontroller', ['$scope', function($scope) {
          
		  var modalID = document.getElementById('custom-myModal');
		  
		  $scope.closeCustomModal = function($scope)
		  {
			  modalID.style.display = "none";
		  }
            
    }])

 .controller('checkAutopay', ['$scope', function($scope) {
          
		  var autopayoff = 1;
		  $scope.imgfile="img/on.jpg";
          $scope.changeautopay = function() {
			if(autopayoff == 1)
			{
			$scope.imgfile="img/off.jpg";
			autopayoff = 0;
			}
			else
			{
			$scope.imgfile="img/on.jpg";
			autopayoff = 1;
			}
		};
            
    }])
	 .controller('checkAutopayoff', ['$scope', function($scope) {
		  var autopayoff = 0;
		  $scope.imgfile="img/off.jpg";
          $scope.changeautopay = function() {
			if(autopayoff == 1)
			{
			$scope.imgfile="img/off.jpg";
			autopayoff = 0;
			}
			else
			{
			$scope.imgfile="img/on.jpg";
			autopayoff = 1;
			}
		};
            
    }])
  .controller('DropdownController', function ($scope) {
			
        $scope.value=true;
        $scope.value2=false;
			
        $scope.DropDownValue=function(val){
            $scope.selectedVal=val;
            $scope.value=false;
            $scope.value2=true;
        }
  })
        .controller('CarouselDemoCtrl', ['$scope', function ($scope) {
            $scope.myInterval = 5000;
            $scope.noWrapSlides = false;
            $scope.active = 0;
            var slides = $scope.slides = [];
            var currIndex = 0;
            $scope.addSlide = function () {
                slides.push({
                    image: 'img/device.png',
                    //text: ['Nice image', 'Awesome photograph', 'That is so cool'][slides.length % 3],
                    id: currIndex++
                });
            };

            for (var i = 0; i < 4; i++) {
                $scope.addSlide();
            }

            $scope.next = function () {
                alert("next");
            }

        }])
  .controller('ModalDemoCtrl1', ['$scope', function($scope)  {
      $scope.displayModal = false;
     $scope.openModal = function() {
     $scope.displayModal = true;
      $scope.ModalBlack = {
           "background":"rgba(0,0,0,0.9)"
          }

        };

              $scope.closeModal = function() {
                   $scope.displayModal = false;
                   $scope.ModalBlack = {
                   "background": "rgba(255,255,255)"

                  }

        };

 

    }])
.controller('radioController', ['$scope', function ($scope) {
    $scope.isSelected1 = false;
    $scope.isSelected2 = false;
    $scope.radioVal1 = false;
    $scope.radioVal2 = false;
    $scope.radioisSelected = true;

    $scope.checked = function (checkOption) {
        if (checkOption == "option1") {
            $scope.isSelected1 = true;
        }
        else {
            $scope.isSelected1 = false;
        }


        if (checkOption == "option2") {
            $scope.isSelected2 = true;
        }
        else {
            $scope.isSelected2 = false;
        }

    }
    $scope.setValue = function (event) {
        $scope.radioisSelected = false;
        if (event.target.value == "option1") {
            $scope.isSelected1 = true;
        }
        else {
            $scope.isSelected1 = false;
        }
        if (event.target.value == "option2") {
            $scope.isSelected2 = true;
        }
        else {
            $scope.isSelected2 = false;
        }
    }

    $scope.showContent = function (id) {
        if (id == "div1") {
            $scope.radioVal1 = !$scope.radioVal1;
            if ($scope.radioVal1 == true) {
                $scope.expand1 = true;
                $scope.expand2 = false;
            }
            else {
                $scope.expand1 = false;
            }       
        }
        else {
            $scope.radioVal1 = false;
            $scope.expand1 = false;
        }
        if (id == "div2") {
            $scope.radioVal2 = !$scope.radioVal2;
            if ($scope.radioVal2 == true) {
                $scope.expand2 = true;
                $scope.expand1 = false;
            }
            else {
                $scope.expand2 = false;
            }
        }
        else {
            $scope.radioVal2 = false;
            $scope.expand2 = false;
        }
    }

}])


        .controller('colorPickerController', ['$scope', function ($scope) {
            $scope.isSelected1 = false;
            $scope.isSelected2 = false;
            $scope.isSelected3 = false;
            $scope.isSelected4 = false;
            $scope.isSelected5 = false;
            $scope.isSelected6 = false;
            $scope.colorisSelected = true;
            $scope.setValue = function (event) {
                $scope.colorisSelected = false;
                if (event.target.value == "space_gray") {
                    $scope.isSelected1 = true;
                }
                else {
                    $scope.isSelected1 = false;
                }
                if (event.target.value == "light_gray") {
                    $scope.isSelected2 = true;
                }
                else {
                    $scope.isSelected2 = false;
                }
                if (event.target.value == "gold") {
                    $scope.isSelected3 = true;
                }
                else {
                    $scope.isSelected3 = false;
                }
                if (event.target.value == "rose_gold") {
                    $scope.isSelected4 = true;
                }
                else {
                    $scope.isSelected4 = false;
                }
                if (event.target.value == "rose_gold2") {
                    $scope.isSelected5 = true;
                }
                else {
                    $scope.isSelected5 = false;
                }
                if (event.target.value == "rose_gold3") {
                    $scope.isSelected6 = true;
                }
                else {
                    $scope.isSelected6 = false;
                }
            }
            $scope.color1=function(){
                $scope.isSelected1 = true;
            }

        }])


	  .controller('ModalDemoCtrl1', ['$scope', function($scope)  {
	    $scope.displayModal = false;
		$scope.openModal = function() {
		    $scope.displayModal = true;
		    $scope.ModalBlack = {
		        "background-color":"black"
		    }
        };
		$scope.closeModal = function() {
		    $scope.displayModal = false;
		    $scope.ModalBlack = {
		        "background-color": "white"
		    }
        };

    }])

    .controller('ModalInstanceCtrl', ['$scope', '$uibModalInstance', function($scope, $uibModalInstance) {

        $scope.ok = function() {
            $uibModalInstance.close();
        };

        $scope.cancel = function() {
            $uibModalInstance.dismiss('cancel');
        };
    }])
    app.directive('setHeightBottom', function ($window) {
        return{
            link: function (scope, element, attrs) {
                scope.winHeight = function () {
                     if ($window.innerWidth < 768) {
                        var OffsetHeight1 = $window.innerHeight;
                         var theHeightR1 = 553;
                        if(OffsetHeight1 > theHeightR1) {

                            element.css('position', 'fixed');
                            element.css('bottom', 0);
                            element.css('width', '100%');
                            
                          
                        }
                        else {
                            element.css('position', 'initial');
                            element.css('width', 'initial');
                           
                        }
                    }

                    else {
                        var OffsetHeight1 = document.body.offsetHeight;
                        element.css('min-height', (0) + 'px');
                    }
                }
                scope.winHeight();
                
                angular.element(window).bind('resize', function () {
                    scope.winHeight();
                    scope.$digest();
                }); 
            }
        }
    })
	
	
})();
