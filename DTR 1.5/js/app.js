(function () {
  'use strict';
  var app = angular.module('app', ['ngAnimate', 'ngAria', 'ngAria', 'ngCookies', 'ngMessages', 'ngSanitize', 'ngTouch', 'ui.bootstrap']);
  app.controller('generalController', ['$scope', function ($scope) {

  }])

    .controller('AccordionDemoCtrl', ['$scope', function ($scope) {
      $scope.oneAtATime = true;

      $scope.groups = [{
        title: 'Dynamic Group Header - 1',
        content: 'Dynamic Group Body - 1'
      }, {
        title: 'Dynamic Group Header - 2',
        content: 'Dynamic Group Body - 2'
      }];

      $scope.items = ['Item 1', 'Item 2', 'Item 3'];

      $scope.addItem = function () {
        var newItemNo = $scope.items.length + 1;
        $scope.items.push('Item ' + newItemNo);
      };

      $scope.status = {
        isCustomHeaderOpen: false,
        isFirstOpen: true,
        isFirstDisabled: false
      };
    }])


    .controller('AlertDemoCtrl', ['$scope', function ($scope) {
      $scope.alerts = [
        { type: 'danger', msg: 'Oh snap! Change a few things up and try submitting again.' },
        { type: 'success', msg: 'Well done! You successfully read this important alert message.' }
      ];

      $scope.addAlert = function () {
        $scope.alerts.push({ msg: 'Another alert!' });
      };

      $scope.closeAlert = function (index) {
        $scope.alerts.splice(index, 1);
      };
    }])

    .controller('ButtonsCtrl', ['$scope', function ($scope) {
      $scope.singleModel = 1;

      $scope.radioModel = 'Middle';

      $scope.checkModel = {
        left: false,
        middle: true,
        right: false
      };

      $scope.checkResults = [];

      $scope.$watchCollection('checkModel', function () {
        $scope.checkResults = [];
        angular.forEach($scope.checkModel, function (value, key) {
          if (value) {
            $scope.checkResults.push(key);
          }
        });
      });
    }])

    .controller('CarouselDemoCtrl', ['$scope', function ($scope) {
      $scope.myInterval = 5000;
      $scope.noWrapSlides = false;
      $scope.active = 0;
      var slides = $scope.slides = [];
      var currIndex = 0;
      $scope.addSlide = function () {
        slides.push({
          image: 'http://placehold.it/140x227',
          //text: ['Nice image', 'Awesome photograph', 'That is so cool'][slides.length % 3],
          id: currIndex++
        });
      };

      for (var i = 0; i < 3; i++) {
        $scope.addSlide();
      }

      $scope.next = function () {
        alert("next");
      }

    }])

    .controller('CarouselDemoCtrl1', ['$scope', function ($scope) {
      $scope.myInterval = 5000;
      $scope.noWrapSlides = false;
      $scope.active = 0;
      var slides = $scope.slides = [];
      var currIndex = 0;
      $scope.addSlide = function () {
        slides.push({
          image: 'http://placehold.it/226x121',
          //text: ['Nice image', 'Awesome photograph', 'That is so cool'][slides.length % 3],
          id: currIndex++
        });
      };

      for (var i = 0; i < 3; i++) {
        $scope.addSlide();
      }

      $scope.next = function () {
        alert("next");
      }

    }])


    .controller('DropdownCtrl', ['$scope', '$log', function ($scope, $log) {
      $scope.status = {
        isopen: false
      };

      $scope.toggled = function (open) {
        $log.log('Dropdown is now: ', open);
      };

      $scope.toggleDropdown = function ($event) {
        $event.preventDefault();
        $event.stopPropagation();
        $scope.status.isopen = !$scope.status.isopen;
      };

      $scope.appendToEl = angular.element(document.querySelector('#dropdown-long-content'));
      $scope.selectedOption = "";
      $scope.change = function (name) {
        $scope.selectedOption = name;
      }
      $scope.onKeyDownDropDown = function (event) {
        if (event.keyCode == 13) {
          $scope.status.isopen = !$scope.status.isopen;
        }
      }
      //    $scope.onBlurDropdown = function(evenr) {
      //     $scope.status.isopen = !$scope.status.isopen;
      //    }
    }])


    .controller('ModalDemoCtrl', ['$scope', '$uibModal', '$log', function ($scope, $uibModal, $log) {

      $scope.openOneButton = function () {

        var modalInstance = $uibModal.open({
          animation: $scope.animationsEnabled,
          templateUrl: 'myOneModalContent.html',
          controller: 'ModalInstanceCtrl',
          resolve: {
            items: function () {
              return $scope.items;
            }
          }
        });

        modalInstance.result.then(function (selectedItem) {
          $scope.selected = selectedItem;
        }, function () {
          $log.info('Modal dismissed at: ' + new Date());
        });
      };

      $scope.openTwoButton = function () {

        var modalInstance = $uibModal.open({
          animation: $scope.animationsEnabled,
          templateUrl: 'myTwoModalContent.html',
          controller: 'ModalInstanceCtrl',
          resolve: {
            items: function () {
              return $scope.items;
            }
          }
        });

        modalInstance.result.then(function (selectedItem) {
          $scope.selected = selectedItem;
        }, function () {
          $log.info('Modal dismissed at: ' + new Date());
        });
      };


    }])

    .controller('ModalInstanceCtrl', ['$scope', '$uibModalInstance', function ($scope, $uibModalInstance) {

      $scope.ok = function () {
        $uibModalInstance.close();
      };

      $scope.cancel = function () {
        $uibModalInstance.dismiss('cancel');
      };
    }])

    /*.controller('ProgressDemoCtrl', ['$scope', function($scope) {
        $scope.max = 200;
 
        $scope.random = function() {
            var value = Math.floor(Math.random() * 100 + 1);
            var type;
 
            if (value < 25) {
                type = 'success';
            } else if (value < 50) {
                type = 'info';
            } else if (value < 75) {
                type = 'warning';
            } else {
                type = 'danger';
            }
 
            $scope.showWarning = type === 'danger' || type === 'warning';
 
            $scope.dynamic = value;
            $scope.type = type;
        };
 
        $scope.random();
 
        $scope.randomStacked = function() {
            $scope.stacked = [];
            var types = ['success', 'info', 'warning', 'danger'];
 
            for (var i = 0, n = Math.floor(Math.random() * 4 + 1); i < n; i++) {
                var index = Math.floor(Math.random() * 4);
                $scope.stacked.push({
                    value: Math.floor(Math.random() * 30 + 1),
                    type: types[index]
                });
            }
        };
 
        $scope.randomStacked();
    }])*/

    /*    .controller('TabsDemoCtrl', ['$scope', '$window', function($scope, $window) {
            $scope.tabs = [
                { title: 'Dynamic Title 1', content: 'Dynamic content 1' },
                { title: 'Dynamic Title 2', content: 'Dynamic content 2', disabled: true }
            ];
    
            $scope.model = {
                name: 'Tabs'
            };
        }])*/

    /*    .controller('TooltipDemoCtrl', ['$scope', '$sce', function($scope, $sce) {
            $scope.dynamicTooltip = 'Hello, World!';
            $scope.dynamicTooltipText = 'dynamic';
            $scope.htmlTooltip = $sce.trustAsHtml('I\'ve been made <b>bold</b>!');
            $scope.placement = {
                options: [
                    'top',
                    'top-left',
                    'top-right',
                    'bottom',
                    'bottom-left',
                    'bottom-right',
                    'left',
                    'left-top',
                    'left-bottom',
                    'right',
                    'right-top',
                    'right-bottom'
                ],
                selected: 'top'
            };
        }])*/

    .controller('TabsDemoCtrl', ['$scope', '$window', function ($scope, $window) {
      $scope.tabs = [
        { title: 'Dynamic Title 1', content: 'Dynamic content 1' },
        { title: 'Dynamic Title 2', content: 'Dynamic content 2', disabled: true }
      ];

      $scope.model = {
        name: 'Tabs'
      };
      /*Hyd SEZ*/
      //$scope.tab = 1;
      $scope.isSet1 = true;
      $scope.rating1 = {
        "color": "#e20074"
      }
      $scope.setTab = function (newTab) {
        if (newTab == 1) {
          $scope.isSet1 = true;
          $scope.isSet2 = false;
          $scope.isSet3 = false;
          $scope.isSet4 = false;
          $scope.isSet5 = false;
          $scope.rating1 = {
            "color": "#e20074"
          }
          $scope.rating2 = {
            "color": "#bebebe"
          }
          $scope.rating3 = {
            "color": "#bebebe"
          }
          $scope.rating4 = {
            "color": "#bebebe"
          }
          $scope.rating5 = {
            "color": "#bebebe"
          }

        }
        else if (newTab == 2) {
          $scope.isSet2 = true;
          $scope.isSet1 = false;
          $scope.isSet3 = false;
          $scope.isSet4 = false;
          $scope.isSet5 = false;
          $scope.rating2 = {
            "color": "#e20074"
          }
          $scope.rating1 = {
            "color": "#bebebe"
          }
          $scope.rating3 = {
            "color": "#bebebe"
          }
          $scope.rating4 = {
            "color": "#bebebe"
          }
          $scope.rating5 = {
            "color": "#bebebe"
          }
        }
        else if (newTab == 3) {
          $scope.isSet3 = true;
          $scope.isSet2 = false;
          $scope.isSet1 = false;
          $scope.isSet4 = false;
          $scope.isSet5 = false;
          $scope.rating3 = {
            "color": "#e20074"
          }
          $scope.rating2 = {
            "color": "#bebebe"
          }
          $scope.rating1 = {
            "color": "#bebebe"
          }
          $scope.rating4 = {
            "color": "#bebebe"
          }
          $scope.rating5 = {
            "color": "#bebebe"
          }
        }
        else if (newTab == 4) {
          $scope.isSet4 = true;
          $scope.isSet2 = false;
          $scope.isSet3 = false;
          $scope.isSet1 = false;
          $scope.isSet5 = false;
          $scope.rating4 = {
            "color": "#e20074"
          }
          $scope.rating2 = {
            "color": "#bebebe"
          }
          $scope.rating3 = {
            "color": "#bebebe"
          }
          $scope.rating1 = {
            "color": "#bebebe"
          }
          $scope.rating5 = {
            "color": "#bebebe"
          }
        }
        else if (newTab == 5) {
          $scope.isSet5 = true;
          $scope.isSet2 = false;
          $scope.isSet3 = false;
          $scope.isSet4 = false;
          $scope.isSet1 = false;
          $scope.rating5 = {
            "color": "#e20074"
          }
          $scope.rating2 = {
            "color": "#bebebe"
          }
          $scope.rating3 = {
            "color": "#bebebe"
          }
          $scope.rating4 = {
            "color": "#bebebe"
          }
          $scope.rating1 = {
            "color": "#bebebe"
          }
        }
      };



      //$scope.isSet = function(tabNum){
      //return $scope.tab === tabNum;
      //};

      $scope.maxSize = 5;
      $scope.bigTotalItems = 175;
      /*Hyd SEZ ends here*/

    }])

    /*copyed by koushik starts*/
    .controller('SeemoreCtrl1', ['$scope', function ($scope) {
      $scope.description = "With 3D Touch, Live Photos, 7000 series aluminum, A9 chip, advanced cameras, 4.7-inch Retina HD display and so much more, you'll see how - with IPhone 6s - the only thing that's changed is everything.With 3D Touch, Live Photos, 7000 series aluminum, A9 chip, advanced cameras, 4.7-inch Retina HD display and so much more, you'll see how - with IPhone 6s - the only thing that's changed is everything";

      /*$scope.legal="With 3D Touch, Live Photos, 7000 series aluminum, A9 chip, advanced cameras, 4.7-inch Retina HD display and so much more, you'll see how - with IPhone 6s - the only thing that's changed is everything.With 3D Touch, Live Photos, 7000 series aluminum, A9 chip, advanced cameras, 4.7-inch Retina HD display and so much more, you'll see how - with IPhone 6s - the only thing that's changed is everything"*/
      $scope.isAddALineEnabled = true;
      $scope.isLimitedAccess = true;

    }])
    /*copyed by koushik ends*/
    /* Offshore Team */
    .controller('CheckListProsCtrl', function ($scope) {
      $scope.pros = [
        'Great for texting',
        'Great web browsing',
        'Long Battery life',
        'Easy of use',
        'Great camera',
        'Useful apps',
        'Fun games',
        'WiFi',
        'GPS',
        '4G LTE',
        'Durable',
        'Large screen',
        'Touch screen',
        'Music',
        'Keyboard',
        'Speakerphone',
        'Volume level',
        'Processor',
        'Memory'
      ];
      $scope.maxProTextBox = 10;
      $scope.proTextBoxArray = []; //to store data of textbox
      var ZERO = 0;
      //create new textbox(ng-repeat logic) when clicked on last text box and is empty
      $scope.insertProTextBox = function (index) {
        var noOfTextBoxes = $scope.proTextBoxArray.length;
        if (noOfTextBoxes <= $scope.maxProTextBox) {
          if (noOfTextBoxes == ZERO && $scope.proTextBoxArray[ZERO].length > ZERO) {
            $scope.proTextBoxArray.push('');
          }
          else if (index == (noOfTextBoxes) && $scope.proTextBoxArray[noOfTextBoxes].length > ZERO) {
            $scope.proTextBoxArray.push('');
          }

        }
      }
    })

    .controller('CheckListConsCtrl', function ($scope) {
      $scope.cons = [
        'Expensive',
        'Hard to use',
        'Web browsing',
        'Battery',
        'Camera',
        'Screen',
        'Touchscreen',
        'Keyboard',
        'Heavy',
        'Bulky',
        'Speakerphone',
        'Volume level',
        'Processor',
        'Memory',
        'Buggy'
      ];
      $scope.maxConTextBox = 10;
      $scope.conTextBoxArray = [];//to store data of textbox
      var ZERO = 0;
      //create new textbox(ng-repeat logic) when clicked on last text box and is empty
      $scope.insertConTextBox = function (index) {
        var noOfTextBoxes = $scope.conTextBoxArray.length;
        if (noOfTextBoxes <= $scope.maxConTextBox) {
          if (noOfTextBoxes == ZERO && $scope.conTextBoxArray[ZERO].length > ZERO) {
            $scope.conTextBoxArray.push('');
          }
          else if (index == (noOfTextBoxes) && $scope.conTextBoxArray[noOfTextBoxes].length > ZERO) {
            $scope.conTextBoxArray.push('');
          }
        }
      }
    })

    .controller('myInfoReview', function ($scope) {
    })
    .controller('starCtrl', ['$scope', function ($scope) {
      $scope.rate = 0;
      $scope.EaseofUserate = 0;
      $scope.Batteryliferate = 0;
      $scope.Featuresrate = 0;
      $scope.CallQualityrate = 0;
      $scope.max = 5;
      $scope.isReadonly = false;

      var ratings = [{
        "name": "poor",
        "id": 1
      },
      {
        "name": "Fair",
        "id": 2
      },
      {
        "name": "Average",
        "id": 3
      }, {
        "name": "Good",
        "id": 4
      },
      {
        "name": "Excellent",
        "id": 5
      }];
      $scope.hoveringOver = function (value) {
        $scope.overStar = ratings[value - 1].name;
      };
      $scope.setvalue = function (index) {
        $scope.rate = index;
        $scope.test = ratings[$scope.rate - 1].name;
      }


      $scope.onEaseofUsehover = function (value) {
        $scope.EaseofUsemouseovervalue = ratings[value - 1].name;
      };

      $scope.setEaseofUsevalue = function (index) {
        $scope.EaseofUserate = index;
        $scope.EaseofUseratedvalue = ratings[$scope.EaseofUserate - 1].name;
      }
      $scope.onEaseofUsemouseout = function () {
        $scope.EaseofUsemouseovervalue = '';
        $scope.EaseofUseratedvalue = ratings[$scope.EaseofUserate - 1].name;

      };

      $scope.onBatterylifehover = function (value) {
        $scope.Batterylifemouseovervalue = ratings[value - 1].name;
      };

      $scope.setBatterylifevalue = function (index) {
        $scope.Batteryliferate = index;
        $scope.Batteryliferatedvalue = ratings[$scope.Batteryliferate - 1].name;
      }
      $scope.onBatterylifemouseout = function () {
        $scope.Batterylifemouseovervalue = '';
        $scope.Batteryliferatedvalue = ratings[$scope.Batteryliferate - 1].name;

      };

      $scope.onFeatureshover = function (value) {
        $scope.Featuresmouseovervalue = ratings[value - 1].name;
      };

      $scope.setFeaturesvalue = function (index) {
        $scope.Featuresrate = index;
        $scope.Featuresratedvalue = ratings[$scope.Featuresrate - 1].name;
      }
      $scope.onFeaturesmouseout = function () {
        $scope.Featuresmouseovervalue = '';
        $scope.Featuresratedvalue = ratings[$scope.Featuresrate - 1].name;

      };

      $scope.onCallQualityhover = function (value) {
        $scope.CallQualitymouseovervalue = ratings[value - 1].name;
      };

      $scope.setCallQualityvalue = function (index) {
        $scope.CallQualityrate = index;
        $scope.CallQualityratedvalue = ratings[$scope.CallQualityrate - 1].name;
      }
      $scope.onCallQualitymouseout = function () {
        $scope.CallQualitymouseovervalue = '';
        $scope.CallQualityratedvalue = ratings[$scope.CallQualityrate - 1].name;

      };


      $scope.ratingStates = [
        { stateOn: 'glyphicon-ok-sign', stateOff: 'glyphicon-ok-circle' },
        { stateOn: 'fa fa-star p-l-5-md p-l-5-lg fa-2x', stateOff: 'fa fa-star-o p-l-5-md p-l-5-lg fa-2x' },


      ];


    }])
    .controller('progressiveCtrl', ['$scope', function ($scope) {
      $scope.isReadonly = false;
      $scope.changeOnHover = true;
      $scope.maxValue = 5;
      $scope.star = 2;
      $scope.ratingValue = 1;
      $scope.currentindex = 1;

    }])
    /*Offshore Team */
    .controller('storeModalCtrl', ['$scope', function ($scope) {
      $scope.visible = false;
      $scope.isVisible = function () {
        $scope.visible = true;
        document.body.scrollTop = 0; /*for safari*/
        document.documentElement.scrollTop = 0; /*for chrome*/
      }
      $scope.close = function () {
        $scope.visible = false;
      };
      $scope.inputtextVal = 'Seattle, WA, USA';
      $scope.locationDetails = [
        { 'street1': 'Downtown - Seattle (430)', 'miles': '∙ 0.2 miles', 'street2': '1527 6th Ave, Seattle, WA, 98101', 'timing': 'Open now ∙ 10am-8pm', 'waitTime': 'In-store wait: 5min', 'text1': 'Appointment', 'text2': 'Get in line', 'text3': 'Call us', 'stock': 'In stock', 'isInStock': true },
        { 'street1': '4th & Main', 'miles': '∙ 0.7 miles', 'street2': '308 4th Ave S, Seattle, WA, 98104', 'timing': 'Open now ∙ 10am-8pm', 'waitTime': 'In-store wait: 5min', 'text1': 'Appointment', 'text2': 'Get in line', 'text3': 'Call us', 'stock': 'Only a few left', 'isInStock': true },
        { 'street1': 'Republican St', 'miles': '∙ 1.2 miles', 'street2': '431 Broadway E Ste F, Seattle, WA 98102', 'timing': 'Open now ∙ 10am-8pm', 'waitTime': 'In-store wait: 5min', 'stock': 'Out of stock', 'isInStock': false },
        { 'street1': 'Stone Way', 'miles': '∙ 0.7 miles', 'street2': '1216 N 45th St, Seattle, WA, 98103', 'timing': 'Open now ∙ 10am-8pm', 'waitTime': 'In-store wait: 5min', 'stock': 'Inventory status not available', 'isInStock': false },
        { 'street1': 'Broadway E & E Republican St', 'miles': '∙ 1.2 miles', 'street2': '431 Broadway E Ste F, Seattle, WA 98102', 'timing': 'Open now ∙ 10am-8pm', 'waitTime': 'In-store wait: 5min', 'text1': 'Appointment', 'text2': 'Get in line', 'text3': 'Call us', 'stock': 'In stock', 'isInStock': true }
      ];
      $scope.title = 'More stores nearby.';
      $scope.content1 = 'Only show stores with all products in stock.';
      $scope.content2 = 'Inventory status doesn’t hold or guarantee products (even with an appt.).';
    }])

  app.controller('cartController', ['$scope', function ($scope) {
    $scope.inputtextVal = 'JayElle@gmail.com';
    $scope.title1 = 'Save your cart for later.';
    $scope.title2 = 'Save your cart.';
    $scope.location = 'Downtown - Seattle (430)';
    $scope.visible = false;
    $scope.isVisible = function () {
      $scope.visible = true;
      document.body.scrollTop = 0; /*for safari*/
      document.documentElement.scrollTop = 0; /*for chrome*/
    };
    $scope.close = function () {
      $scope.visible = false;
    };
  }])
  app.controller('InStoreCtrl', ['$scope', function ($scope) {
    $scope.visible = false;
    $scope.isVisible = function () {
      $scope.visible = true;
      document.body.scrollTop = 0; /*for safari*/
      document.documentElement.scrollTop = 0; /*for chrome*/
    }
    $scope.close = function () {
      $scope.visible = false;
    };
    $scope.inputtextVal = 'Seattle, WA, USA';
    $scope.locationDetails = [
      { 'street1': 'Downtown - Seattle (430)', 'miles': '∙ 0.2 miles', 'street2': '1527 6th Ave, Seattle, WA, 98101', 'timing': 'Open now ∙ 10am-8pm', 'waitTime': 'In-store wait: 5min', 'text1': 'Appointment', 'text2': 'Get in line', 'text3': 'Call us', 'stock': 'In stock', 'isInStock': true }
    ];
    $scope.title = 'More stores nearby.';
    $scope.content1 = 'Only show stores with all products in stock.';
    $scope.content2 = 'Inventory status doesn’t hold or guarantee products (even with an appt.).';
  }])
  app.controller('accessoryCardCtrl', ['$scope', '$http', function ($scope, $http) {
    $scope.WithEIPCondition = true;
    $scope.stockDetails = false;
    $http.get("./json/acard.json").then(function (response) {
      if ($scope.WithEIPCondition) {
        $scope.details = response.data.WithEIP;
      } else {
        $scope.details = response.data.NoEIP;
      }
    });

  }])
    .filter('priceFilter', function () {
      return function (data, position) {
        var output = '';
        if (data != undefined) {
          var price = data;
          var i = price.indexOf('.');
          var partOne = price.slice(0, i).trim();
          var partTwo = price.slice(i + 1, price.length).trim();
          if (position == "dollar") {
            output = partOne;
          }
          else if (position == "cent") {
            output = partTwo;
          }
        }
        return output;
      }
    })
    .directive('accessoryCard', function () {
      return {
        restrict: 'E',
        scope: true,
        templateUrl: 'partials/accessory-card.html'
      };
    })
    .controller('modalController', ['$scope', '$http', function ($scope, $http) {
      $scope.byod = true;
      $scope.visible = false;

      $http.get("./json/modal.json").then(function (response) {
        $scope.productDetails = response.data
        if ($scope.byod) {
          $scope.details = response.data.imagesB;
        } else {
          $scope.details = response.data.imagesA;
        }
      });
      $scope.isVisible = function () {
        $scope.visible = true;
        document.body.scrollTop = 0; /*for safari*/
        document.documentElement.scrollTop = 0; /*for chrome*/
      };
      $scope.close = function () {
        $scope.visible = false;
      };
    }]);
  app.controller('mycartControlller', ['$scope', '$http', '$window', function ($scope, $http, $window) {
    $scope.checked = [false, false];
    $scope.mobile = false;
    $scope.isShowBanner = false;
    $scope.isDownpayment = false;
    $scope.closeOverlayModal = true;        
    $scope.divHeight = function () {
      setTimeout(function () {
        let group = document.querySelectorAll('.EqHeightDiv');
        let addonsDiv = document.querySelectorAll('.addonsDiv');
        let addonsDivHeight = document.querySelectorAll('.addonsDiv')[0].offsetHeight;
        let planDivHeight = document.querySelectorAll('.planDiv')[0].offsetHeight;
        let h1 = group[0].clientHeight;
        let h2 = addonsDivHeight + planDivHeight + 15;
        let h3 = group[2].clientHeight;
        let maxHeight = 0;
        if (window.innerWidth > 991) {
          maxHeight = ((h1 >= h2 && h1 >= h3) ? h1 : ((h2 >= h1 && h2 >= h3) ? h2 : ((h3 >= h1 && h3 >= h2) ? h3 : '')));
          for (let i = 0; i <= group.length - 1; i++) {
            group[i].style.height = maxHeight + 'px';
            addonsDiv[0].style.height = maxHeight - (planDivHeight + 15) + 'px';
          }
        } else {
          for (let i = 0; i <= group.length - 1; i++) {
            group[i].style.height = 'unset';
            addonsDiv[0].style.height = 'unset';
          }
        }
      }, 1000);
    }

    $scope.init = function () {
      if (window.innerWidth < 768) {
        $scope.mobile = true;
      } else {
        $scope.mobile = false;
      }
      $scope.divHeight();
    };

    $scope.adjustHeight = function () {
      setTimeout(function () {
        $scope.init();
      }, 300);
    };

    angular.element(document).ready(function () {
      setTimeout(function () {
        $scope.init();
      }, 300);
      
      document.getElementsByClassName('overlay-out')[0].classList.add("negativeZIndex");
    });

    angular.element($window).on('resize', function (scope) {
      $scope.$apply(function () {
        if ($window.innerWidth < 768) {
          $scope.mobile = true;
        } else {
          $scope.mobile = false;
        }
        $scope.divHeight();
        setTimeout(function () {
          $scope.init();
        }, 100);
      })
    });
    $http.get("./json/myCart.json").then(function (response) {
      $scope.data = response.data;
      $scope.planDetails = response.data.plans;
      $scope.linesDevices = response.data.moreLineDevices;
      $scope.setBorder($scope.planDetails);
    });
    $http.get("./json/myCartUpdate.json").then(function (response) {
      $scope.mycartData = response.data;
      $scope.modalData = response.data.plans;
      $scope.planDetails = response.data.plans;
      $scope.linesDevices = response.data.moreLineDevices;
      $scope.lineDetails = $scope.planDetails.filter(function (obj) {
        return obj.bestValue === true
      })[0];
      $scope.selectedOption = "";
      $scope.comparedData = $scope.modalData[0];
    });
    $http.get("./json/myCartUpdate-V1.json").then(function (response) {
      $scope.userName = response.data.userName;
      $scope.mycartData1 = response.data;
      $scope.modalData1 = response.data.plans;
      $scope.planDetails1 = response.data.plans;
      $scope.linesDevices1 = response.data.moreLineDevices;
      $scope.lineDetails1 = $scope.planDetails1.filter(function (obj) {
        return obj.bestValue === true
      })[0];
    });
    $scope.changeDropdown = function (name, i) {

      $scope.selectedOption = name;
      $scope.comparedData = $scope.modalData[i];
      const changeDropdown1 = document.getElementById('changeDropdown1');
      changeDropdown1.focus();
    }
    $scope.choose = function (name, i) {
      $scope.selectedOption2 = name;
      $scope.comparedData2 = $scope.modalData[i];
      const chooseDropdown = document.getElementById('chooseDropdown');
      chooseDropdown.focus();
    }
    $scope.compareEnable = function (name) {
      $scope.compare = true;
      const comparePlanSection = document.getElementById('comparePlanSection');
      setTimeout("comparePlanSection.focus()", 100);

    }
    $scope.activeIndex = function (data) {
      $scope.activeIndexItem = data;
    }
    $scope.setBorder = function (data) {
      for (var i = 0; i < data.length; i++) {
        if (data[i].bestValue) {
          $scope.activeIndex(i);
        }
      }
    }
    $scope.onKeyDown = function (event, index) {
      if ((event.keyCode == 37 && !$scope.mobile) || (event.keyCode == 38 && !$scope.mobile)) {
        if ($scope.activeIndexItem == 0) {
          $scope.activeIndexItem = $scope.planDetails.length - 1;
        }
        else {
          $scope.activeIndexItem -= 1;
        }
        document.getElementsByClassName('plan-container')[$scope.activeIndexItem].focus();
      }
      if ((event.keyCode == 39 && !$scope.mobile) || (event.keyCode == 40 && !$scope.mobile)) {
        if ($scope.activeIndexItem == $scope.planDetails.length - 1) {
          $scope.activeIndexItem = 0;
        }
        else {
          $scope.activeIndexItem += 1;
        }
        document.getElementsByClassName('plan-container')[$scope.activeIndexItem].focus();
      }

    }
    $scope.onKeyDownCheckbox = function (event, index) {
      if (event.keyCode == 13) {
        $scope.checked[index] = !$scope.checked[index];
      }
    }

    $scope.modalOpen = function () {
      const body = document.getElementsByTagName('body')[0];
      body.classList.add('noScroll');
      $scope.modelTab();
    }
    $scope.modelFocus = function (e, status) {
      if (e.keyCode == 9 && !status && !$scope.selectedOption2) {
        $scope.modelTab();
      }
      if (e.keyCode == 9 && $scope.compare) {
        const compareButton = document.getElementById('compareButton');
        if (compareButton) {
          compareButton.focus();
        }
      }

    }
    $scope.modelTab = function () {
      const dialog = document.getElementById('modalBodyclose');
      dialog.focus();
    }
    $scope.trapFocus = function (e) {
      if (e.keyCode == 9) {
        $scope.modelTab();
      }
    }
    $scope.close1 = function () {
      this.visible = false;
      const body = document.getElementsByTagName('body')[0];
      $scope.selectedOption2 = '';
      $scope.compare = false;
      body.classList.remove('noScroll');
    }
    $scope.showBanner = function () {
      $scope.isShowBanner = true;
      $scope.isDownpayment = true;
    }
    $scope.hideBanner = function () {
      $scope.isShowBanner = false;
    }
    $scope.bannerFocus = function () {
      document.getElementById('banner1').focus();
    }
    $scope.betterPlanFocus = function () {
      document.getElementById('cartIcon').focus();
      $scope.isShowBanner = false;
    }
    $scope.dialogModalClick = function() {
      
      if($scope.closeOverlayModal){
        $scope.pricingModalclose();
      }
      $scope.closeOverlayModal = true;
    }
    $scope.test2 = function() {
      $scope.closeOverlayModal = false;
    }
    $scope.pricingModalclose = function () {
      $scope.pricingModalVisible = false;
      setTimeout(function(){
      document.getElementsByClassName('overlay-out')[0].classList.remove("zIndexOne");        
        document.getElementsByClassName('overlay-out')[0].classList.add("negativeZIndex");
      },500);
	    /*document.getElementById('viewYourPricing').focus();*/
      document.getElementById('viewYourPricingmobile').focus();
      document.getElementsByTagName('body')[0].classList.remove('noScroll');
    };

    $scope.pricingModal = function (title) {      
      document.getElementsByClassName('overlay-out')[0].classList.remove("negativeZIndex")
      $scope.pricingModalVisible = !$scope.pricingModalVisible;
      
      if ($scope.pricingModalVisible) {
        document.getElementById("pricingModalBody").focus();
        document.getElementsByTagName('body')[0].classList.add('noScroll');
        $scope.modalInvokedFrom = title;
      }
      else {
        document.getElementsByTagName('body')[0].classList.remove('noScroll');
        if (!$scope.mobile) {
          document.getElementById($scope.modalInvokedFrom).focus();
        }
      }
    }

    $scope.backTabEvent = function (event, mainId) {
      const allElements = document.getElementById(mainId).querySelectorAll
        ('button, [href], input, select, textarea, [tabindex]:not([tabindex="-1"])');
      if(event.keyCode === 27) {
        $scope.pricingModalclose();
        document.getElementById('viewYourPricing').focus();
        document.getElementById('viewYourPricingmobile').focus();
      }
      
      if (document.activeElement === document.getElementById('openingFocus')) {
        if (event.shiftKey && event.keyCode === 9 ) {
          if (event.shiftKey) {
            event.preventDefault();
            setTimeout(function () {
              document.getElementById('pricingModalBody').focus();
            }, 0);
          }
        }
      }
      if (document.activeElement === document.getElementById('pricingModalBody')) {
        if (event.keyCode === 9 ) {
          if (event.shiftKey) {
            event.preventDefault();
            setTimeout(function () {
              document.getElementById('closeIcon').focus();
            }, 0);
          }
        }
      }
      if (document.activeElement === document.getElementById(mainId)) {
        if (event.keyCode === 9) {
          if (event.shiftKey) {
            event.preventDefault();
            setTimeout(function () {
              allElements[allElements.length - 1].focus();
            }, 0);
          }
        }
      }
      if (document.activeElement === allElements[allElements.length - 1]) {
        if (event.keyCode === 9) {
          if (event.shiftKey) { }
          else {
            event.preventDefault();
            setTimeout(function () {
              document.getElementById('pricingModalBody').focus();
            }, 0);
          }

        }
      }
    }


  }])
    .directive('elemReady', function ($parse) {
      return {
        priority: -10000,
        restrict: 'A',
        link: function ($scope, elem, attrs) {
          elem.ready(function () {
            $scope.$apply(function () {
              var func = $parse(attrs.elemReady);
              func($scope);
              $scope.adjustHeight();
            })
          })
        }
      }
    })
    // multistep code starts
    .directive('multiStep', function () {
      return {
        restrict: 'E',
        scope: true,
        templateUrl: 'partials/multi-step.html',
      }


    })
    .controller('stepCounterController', ['$scope', '$http', function ($scope, $http) {
      $scope.steps = 4;
      var width = 100 / $scope.steps + '%';
      $scope.stepWidth = {
        'width': width
      }
      $scope.stepCompleteIcon = "./img/step-counter/right-green.png"
      $scope.stepDetails = [{
        title: 'Personal Info',
        icon: './img/step-counter/1.png',
        iconActive: './img/step-counter/1.png',
        state: 'active'
      },
      {
        title: 'Credit Check',
        icon: './img/step-counter/2.png',
        iconActive: './img/step-counter/2-active.png',
        state: ''

      },
      {
        title: 'Shipping & Payment',
        icon: './img/step-counter/3.png',
        iconActive: './img/step-counter/3-active.png',
        state: ''
      },
      {
        title: 'Review & Submit',
        icon: './img/step-counter/4.png',
        iconActive: './img/step-counter/4-active.png',
        state: ''
      }
      ]
      $scope.totalSteps = $scope.stepDetails.length;
      $scope.currentStep = 1;
      $scope.continue = function () {
        $scope.stepDetails[$scope.currentStep - 1].state = "complete"
        $scope.stepDetails[$scope.currentStep].state = "active"
        $scope.currentStep += 1;
      }
      $scope.back = function () {
        $scope.currentStep -= 1;
        $scope.stepDetails[$scope.currentStep - 1].state = "active"
        $scope.stepDetails[$scope.currentStep].state = ""
      }
    }])
    // multistep code 
    // Filter to handle $599.00 format
    .filter('currencyFilter', function () {
      return function (data, position) {
        var output = '';
        if (position == 'dollar') {
          output = '$';
        } else if (position == 'mantessa') {
          output = data.replace(/[$]/g, '');
          output = output.split(".")[0];
        } else if (position == 'decimal') {
          output = data.split(".").pop();
        }
        return output;
      }
    })

    // Order Modal Controller 
    .controller('orderModalctrl', ['$scope', '$http', function ($scope, $http) {
      // order details modal starts		
      $scope.visible = false;
      $scope.orderDetailsModalOpen = true;
      $scope.openModal = function () {
        $scope.visibleModal = !$scope.visibleModal;
        document.getElementById('orderModalclose').focus();
      }
      $scope.close = function () {
        $scope.visible = false;
      };
      $scope.closeModal = function () {
        $scope.visibleModal = false;
      };
      $scope.isVisible = function () {
        $scope.visible = true;
        document.body.scrollTop = 0; /*for safari*/
        document.documentElement.scrollTop = 0; /*for chrome*/
      };
      $scope.dialogModalClick = function () {
        if( $scope.orderDetailsModalOpen) {
          $scope.closeModal();
          $scope.close();
        }
        $scope.orderDetailsModalOpen = true;
      }
      $scope.dialogClick = function() {
        $scope.orderDetailsModalOpen = false;
      }
      $http.get("./json/step3_checkout_od.json").then(function (response) {
        //$scope.details = response.data.order_detail_hiy;
        $scope.details = response.data.order_detail_cd;
        //  console.log($scope.details);
      })
      $scope.linedetails = [];
      $scope.showLineDetails = function (index) {
        $scope.linedetails[index] = true;
      }
      $scope.hideLineDetails = function (index) {
        $scope.linedetails[index] = false;
      }
      $scope.orderDetailsModalTab = function () {
        $scope.visible = !$scope.visible;
        $scope.visibleModal = false;
        document.getElementById('modalBodyclose').focus();
      }
      $scope.backTabEvent = function (event, mainId) {

        const allElements = document.getElementById(mainId).querySelectorAll
          ('button, [href], input, select, textarea, [tabindex]:not([tabindex="-1"])');
        if (document.activeElement === allElements[0]) {

          if (event.keyCode === 9) {
            if (event.shiftKey) {
              event.preventDefault();
              setTimeout(function () {
                allElements[allElements.length - 1].focus();
              }, 0);
            }
          }
        }
        if (document.activeElement === document.getElementById(mainId)) {
          if (event.keyCode === 9) {
            if (event.shiftKey) {
              event.preventDefault();
              setTimeout(function () {
                allElements[allElements.length - 1].focus();
              }, 0);
            }
          }
        }
        if (document.activeElement === allElements[allElements.length - 1]) {
          if (event.keyCode === 9) {
            if (event.shiftKey) { }
            else {
              event.preventDefault();
              setTimeout(function () {
                document.getElementById(mainId).focus();
              }, 0);
            }

          }
        }
      }
      $scope.keyPress = function (keyCode) {
        if (keyCode === 27 && ($scope.visibleModal || $scope.visible)) {
          $scope.visibleModal = false;
          $scope.visible = false;
          document.getElementById('orderDetailsBtn').focus();
        }
      }
      // order details ends
    }])
    //Authentication code starts
    .controller('authenticationController', ['$scope', '$http', function ($scope, $http) {
      $scope.visible = false;
      $scope.close = function () {
        $scope.visible = false;
      };
      $scope.authenticationModalTab = function () {
        $scope.visible = !$scope.visible;
      }
      $scope.backTabEvent = function (event, mainId) {
        const allElements = document.getElementById(mainId).querySelectorAll('button, [href], input, select, textarea, [tabindex]:not([tabindex="-1"])');
        if (document.activeElement === allElements[0]) {
          if (event.keyCode === 9) {
            if (event.shiftKey) {
              event.preventDefault();
              setTimeout(function () {
                allElements[allElements.length - 1].focus();
              }, 0);
            }
          }
        }
        if (document.activeElement === document.getElementById(mainId)) {
          if (event.keyCode === 9) {
            if (event.shiftKey) {
              event.preventDefault();
              setTimeout(function () {
                allElements[allElements.length - 1].focus();
              }, 0);
            }
          }
        }
        if (document.activeElement === allElements[allElements.length - 1]) {
          if (event.keyCode === 9) {
            if (event.shiftKey) { } else {
              event.preventDefault();
              setTimeout(function () {
                document.getElementById(mainId).focus();
              }, 0);
            }

          }
        }
      }
    }]);
  //Authentication code ends
   //loginModalController code starts
    app.controller('loginModalController', ['$scope', '$http', '$window', function ($scope, $http, $window) {
      $scope.loginModalVisible = false;
      $scope.loginModalClose = true;
      $scope.mobile=false;
      angular.element($window).on('resize', function (scope) {
        $scope.$apply(function () {
          if ($window.innerWidth < 768) {
            $scope.mobile = true;
          } else {
            $scope.mobile = false;
          }
        })
      });
      $scope.close = function () {
        $scope.loginModalVisible = false;
      };
      $scope.loginModalTab = function () {
        $scope.loginModalVisible = !$scope.loginModalVisible;
      }
      $scope.dialogContainerClick = function() {
        if($scope.loginModalClose) {
          $scope.close();
        }
        $scope.loginModalClose = true;
      };
      $scope.dialogClick = function() {
        $scope.loginModalClose = false;
      };
      $scope.backTabEvent = function (event, mainId, lastId) {
        const allElements = document.getElementById(mainId).querySelectorAll('button, [href], input, select, textarea, [tabindex]:not([tabindex="-1"])');
        if (document.activeElement === allElements[0]) {
          if (event.keyCode === 9) {
            if (event.shiftKey) {
              event.preventDefault();
              setTimeout(function () {
               document.getElementById(lastId).focus();
              }, 0);
            }
          }
        }
        if (document.activeElement === document.getElementById(mainId)) {
          if (event.keyCode === 9) {
            if (event.shiftKey) {
              event.preventDefault();
              setTimeout(function () {
                document.getElementById(lastId).focus();
              }, 0);
            }
          }
        }
        if (document.activeElement === document.getElementById(lastId)) {
          if (event.keyCode === 9) {
            if (event.shiftKey) { } else {
              event.preventDefault();
              setTimeout(function () {
                document.getElementById(mainId).focus();
              }, 0);
            }

          }
        }
      }
      $scope.keyPress = function(keyCode){
        if(keyCode === 27 && $scope.loginModalVisible) {
         $scope.loginModalVisible = false;
         document.getElementById('modalBtn').focus();
        }
      }
    }])

    // loading screen controller starts
    .controller('loadingScreenController', ['$scope', '$http', function ($scope, $http) {
      $scope.timer_running = false;
      $scope.loadingSteps = ["We're looking for your pricing","We're running a quick credit check"];
      $scope.StepIndex = 0;
        $scope.init = function () {
          $scope.timer_running = true;
        
        }
      }  
    ])
    //directive for laoding bar
    .directive('loadingbar', function($timeout) {
      return {
        restrict : 'E',
        template : "<div class='progress'><div class='progress-bar progress-bar-{{type}}' role='progressbar' ng-style='loadingWidth'></div></div>",
        replace : true,
        scope: {
                start: '@',
                type: '@',
            },
            controller : ['$scope', '$timeout', function($scope, $timeout) {
              $scope.onTimeout = function() {
                  $scope.startAt++;
                  $scope.current = $scope.startAt;
                  if($scope.startAt >= $scope.max) {
                    $scope.stop();
                  }
                  $scope.percent_complete = Number(100);
                  var width = $scope.percent_complete+ '%';
                  $scope.loadingWidth = {
                  'width': width
                  }
              }
  
              $scope.stop = function() {
                $timeout.cancel($scope.mytimeout);
              }
            }],
  
        link : function (scope, elem, attrs, ctrl) {
  
          attrs.$observe('start', function(value) {
              if(value === 'true') {
                scope.startAt = 0;
                scope.mytimeout = $timeout(scope.onTimeout,1000);
              }else if(value ==='false') {
                scope.stop();
              }	
          });
        }
      }
    });
  // loading screen controller starts
})();

