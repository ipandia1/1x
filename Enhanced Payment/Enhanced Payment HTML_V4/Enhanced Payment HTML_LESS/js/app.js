﻿element = "";
templateID="";
oldcardimg = "";
(function() {
    'use strict';
    var app = angular.module('app', ['ngAnimate', 'ngAria', 'ngAria', 'ngCookies', 'ngMessages', 'ngSanitize',  'ui.bootstrap', 'ngMaterial']);
    
	
    angular.module("uib/template/modal/window.html", []).run(["$templateCache", function($templateCache) {
        $templateCache.put("uib/template/modal/window.html",
          "  <button type=\"button\" class=\"close pull-right uib-close\" ng-click=\"close($event)\"><i aria-hidden=\"true\" class=\"ico-closeIcon uib-close\" ng-click=\"close($event)\"></i></button><div class=\"modal-dialog {{size ? 'modal-' + size : ''}}\"><div class=\"modal-content\" uib-modal-transclude></div></div>\n" +
          "");
    }]);

    

      app.directive('setHeight', function ($window) {
        return{
            link: function (scope, element, attrs) {
                scope.winHeight = function () {
                    if($window.innerWidth<768)
                    {
                        element.css('min-height',  ($window.innerHeight-110)+'px');
                    }
                    else
                    {
                        element.css('min-height',  0+'px');
                    }
                }
                scope.winHeight();
                
                angular.element(window).bind('resize', function () {
                    scope.winHeight();
                    scope.$digest();
                }); 
            }
        }
    })

    app.directive('format', ['$filter', function ($filter) {
        return {
            require: '?ngModel',
            link: function (scope, elem, attrs, ctrl) {
                
                if (!ctrl) return;
                ctrl.$parsers.unshift(function (viewValue) {
                    var plainNumber = viewValue.replace(/[^\d|\-+|\.+]/g, '');
                    plainNumber = plainNumber.replace(/(\d{4})/g, '$1 ').replace(/(^\s+|\s+$)/, '');
                    elem.val(plainNumber);
                    return plainNumber;
                });
            }
        };
    }]);
 
    app.directive('expiryformat', ['$filter', function ($filter) {
        return {
            require: '?ngModel',
            link: function (scope, elem, attrs, ctrl) {
                if (!ctrl) return;
                ctrl.$parsers.unshift(function (plainNumber) {
                    if (plainNumber.length == 2) {
                        plainNumber = plainNumber + "/";
                        
                    }
                    elem.val(plainNumber);
                    return plainNumber;
                });
            }
        };
    }]); 

    app.directive('clearinput', ['$compile', '$timeout', '$mdDialog', function ($compile, $timeout, $mdDialog) {

        return {
            scope:{},
            require: 'ngModel',
            link: function (scope, element, attribute, ngModel, $event) {
                if (attribute.ngModel === "payment.routingNumber" || attribute.ngModel === "payment.accountNumber" || attribute.ngModel === "payment.cvv")
                {
                    var template = $compile('<div ng-show="clear" ng-mousedown="clearinput();" class="cross_posn"><i class="fa fa-times-circle margin-mobile"></i></div><span ng-show="information"><i class="fa fa-info-circle iconCls-info color-magenta margin-mobile"  class="iconCls" style="cursor: pointer; cursor: hand;" ng-mousedown="showModal()" /></i></span>')(scope);
                    element.after(template);
                }
                else
                {
                    var template = $compile('<div ng-show="clear" ng-mousedown="clearinput();" class="cross_posn"><i class="fa fa-times-circle margin-mobile" style="cursor: pointer; cursor: hand;"></i></div>')(scope);
                     element.after(template);
                }
                
                scope.showModal = function () {

                    templateID="";
                    if (attribute.ngModel === "payment.cvv")
                    {
                        if(scope.$parent.checkModals==='0')
                        {
                            templateID = "modal3.html";
                        }
                        else if(scope.$parent.checkModals==='1')
                        {
                            templateID = "modal2.html";
                        }
                        else if(scope.$parent.checkModals==='2')
                        {
                            templateID = "modal1.html";
                        }
                    }
                    else 
                    {
                        templateID = "acctModal.html";
                    }
                    
                    $mdDialog.show({

                        templateUrl: ''+templateID,
                        clickOutsideToClose: true,
			disableParentScroll: true,
                        controller: function ($scope, $mdDialog) {

                            $scope.hide = function () {
                                $mdDialog.hide();

                            };

                            $scope.cancel = function () {

                                $mdDialog.cancel();

                            };

                            $scope.answer = function (answer) {
                                //console.log($mdDialog.hide('answer'));
                                $mdDialog.hide(answer);

                            };
                        }

                    })
                };
                
                scope.cancel = function () {
                    $mdDialog.cancel();
                }

                if (attribute.ngModel === "payment.routingNumber" || attribute.ngModel === "payment.accountNumber" || attribute.ngModel === "payment.cvv")
                {
                    element.bind('input', function () {
                        if (ngModel.$viewValue == "" || ngModel.$viewValue === undefined) {
                            if(attribute.ngModel === "payment.cvv"){
                                if(oldcardimg === "")
                                {
                                    oldcardimg = scope.$parent.cardimg;
                                    scope.$parent.cardimg =  "img/visaCardBack.png";
                                    
                                }
                               
                            }
                            scope.clear = false;
                            scope.information = true;
                            scope.$apply();
                        }
                        else {
                            scope.$parent.cardimg = oldcardimg;
                            oldcardimg="";
                            scope.information = false;
                            scope.clear = true;
                            scope.$apply();

                        }
                    })
                    element.bind('focus', function () {
                        if (ngModel.$viewValue == "" || ngModel.$viewValue === undefined) {

                            if(attribute.ngModel === "payment.cvv"){
                                if(oldcardimg === "")
                                {
                                    oldcardimg = scope.$parent.cardimg;
                                    scope.$parent.cardimg =  "img/visaCardBack.png";
                                    
                                }
                                
                            }

                            scope.information = true;
                            scope.clear = false;
                            scope.$apply();
                        }
                        else {
                            scope.clear = true;
                            scope.$parent.cardimg = oldcardimg;
                            oldcardimg="";
                            scope.information = false;
                            scope.$apply();

                        }
                    })
                    element.bind('blur', function () {
                        scope.$parent.cardimg = oldcardimg;
                        oldcardimg="";
                        scope.information = false;
                        scope.clear = false;
                        //scope.$apply();
                    });

                    scope.clearinput = function () {
                        scope.$parent.cardimg = oldcardimg;
                        oldcardimg="";
                        ngModel.$setViewValue("");
                        ngModel.$render();
                        scope.information = true;
                        scope.clear = false;
                        //scope.$apply();
                    }


                }
                else {
                    element.bind('input', function () {
                        if (ngModel.$viewValue == "" || ngModel.$viewValue === undefined) {
                            scope.clear = false;
                            scope.$apply();
                        }
                        else {
                            scope.clear = true;
                            scope.$apply();

                        }
                    })
                    element.bind('focus', function () {
                        if (ngModel.$viewValue == "" || ngModel.$viewValue === undefined) {
                            scope.clear = false;                            
                            scope.$apply();
                          
                        }
                        else {
                            scope.clear = true;
                            scope.$apply();

                        }
                    })
                    element.bind('blur', function () {
                        // scope.$parent.cardimg = oldcardimg;
                        // oldcardimg="";
                        scope.clear = false;
                        scope.$apply();
                    });

                    scope.clearinput = function () {
                        scope.$parent.cardimg = oldcardimg;
                        oldcardimg="";
                        ngModel.$setViewValue("");
                        ngModel.$render();
                        scope.clear = false;
                        //scope.$apply();
                    }

                }
            }

        }
    }]);

   

    app.directive('numbervaluesonly', function () {
        return {
            restrict: 'A',
            link: function (scope, element, attrs, ctrls) {
                element.on('keydown', function (event) {
                    if (event.which == 64 || event.which == 16) {
                        return false;
                    } else if (event.which >= 48 && event.which <= 57) {
                        return true;
                    } else if (event.which >= 96 && event.which <= 105) {
                        return true;
                    } else if ([8, 9, 13, 27, 37, 38, 39, 40].indexOf(event.which) > -1) {
                        return true;
                    } else {
                        event.preventDefault();
                        return false;
                    }

                });

            }
        }
    });

    app.directive('cardvalidn', ['$parse', function ($parse) {
        return {
            require: '?ngModel',
            link: function (scope, elem, attrs, ctrl) {
                
                var model = ctrl;
                scope.max_length = "18";
                elem.on('input', function () {
                    if (attrs.id) {
                        if (attrs.id == "cardNum") {
                            fromUser(elem.val());
                        }
                    }
                });
                elem.on('focus', function () {
                    if (attrs.id) {
                        if (attrs.id == "cardNum") {
                            fromUser(elem.val());
                        }
                    }
                });
                function fromUser(text) {
                    
                        if (text.length != 0) {
                            if (attrs.id == "cardNum") {
                                scope.modelVal = model.$viewValue;
                                var maxLimit = scope.modelVal.length;
                                scope.modelVal = (scope.modelVal).substr(0, 1);
                                if (scope.modelVal == "3") {
                                    scope.max_length = "17";
                                    if (maxLimit == "17") {
                                        scope.checkModals = "1";
                                        scope.cardimg = "img/americanExpressCard.png";
                                    }
                                }
                                else if (scope.modelVal == "5" ) {
                                    scope.max_length = "18";
                                    if (maxLimit == "18") {
                                        scope.checkModals = "2";
                                        scope.cardimg = "img/masterCard.png";
                                                                            
                                    }                                   
                                }
                                else if (scope.modelVal == "4"  || scope.modelVal == "6") {
                                    scope.max_length = "18";
                                    if (maxLimit == "18") {
                                        scope.checkModals = "2";
                                        scope.cardimg = "img/visaCard.png";
                                    }
                                }
                            }
                        }
                        else {
                            console.log("text length"+text.length);
                            
                            scope.cardimg = "img/plainCard.png";
                            scope.checkModals = "0";
                            scope.$apply();
                        }
                    
                }
                //model.$parsers.push(fromUser);
            }
        };
    }]);
 
    app.controller('addCardctrl', ['$scope', '$mdDialog', function ($scope, $mdDialog) {

        $scope.showbilling=false;
        $scope.billing = true;
        $scope.exclmn = false;
        $scope.cardimg = "img/plainCard.png";
        //oldcardimg = $scope.cardimg;
        $scope.checkModals = "0";
        $scope.selected = "";
        $scope.states = ['Alabama', 'Alaska', 'Arizona', 'Arkansas',
        'California', 'Colorado', 'Connecticut', 'Delaware', 'Florida',
        'Georgia', 'Hawaii', 'Idaho', 'Illinois', 'Indiana', 'Iowa',
        'Kansas', 'Kentucky', 'Louisiana', 'Maine', 'Maryland',
        'Massachusetts', 'Michigan', 'Minnesota', 'Mississippi',
        'Missouri', 'Montana', 'Nebraska', 'Nevada', 'New Hampshire',
        'New Jersey', 'New Mexico', 'New York', 'North Dakota',
        'North Carolina', 'Ohio', 'Oklahoma', 'Oregon', 'Pennsylvania',
        'Rhode Island', 'South Carolina', 'South Dakota', 'Tennessee',
        'Texas', 'Utah', 'Vermont', 'Virginia', 'Washington',
        'West Virginia', 'Wisconsin', 'Wyoming'];

        $scope.accountName = "";
        

        $scope.customFullscreen = false;

        $scope.openDialog = function() {
            $mdDialog.show({
                 parent: angular.element(document.body),
                 clickOutsideToClose:true,
                 templateUrl:'modal4.html'                  ,
                locals: {                  
                },
                controller: DialogController
              });
                  
              function DialogController($scope, $mdDialog) {
                $scope.closeDialog = function() {
                
                };
                $scope.hide = function () {
                    $mdDialog.hide();

                };

                $scope.cancel = function () {

                    $mdDialog.cancel();

                };
              };
          }
          $scope.openDialog1 = function() {
            $mdDialog.show({
                 parent: angular.element(document.body),
                 clickOutsideToClose:true,
                 templateUrl:'modalCTA1.html'                  ,
                locals: {                  
                },
                controller: DialogController
              });
                  
              function DialogController($scope, $mdDialog) {
                $scope.closeDialog = function() {
                  
                };
                $scope.hide = function () {
                    $mdDialog.hide();

                };

                $scope.cancel = function () {

                    $mdDialog.cancel();

                };
              };
          }
        $scope.showModal = function (ev, modalId) {
            if (modalId == "0") {
                $mdDialog.show({
                    controller: DialogController,
                    templateUrl: 'modal3.html',
                    parent: angular.element(document.body),
                    targetEvent: ev,
                    clickOutsideToClose: true,
                    fullscreen: $scope.customFullscreen
                })
            }
            else if (modalId == "1") {
                $mdDialog.show({
                    controller: DialogController,
                    templateUrl: 'modal2.html',
                    parent: angular.element(document.body),
                    targetEvent: ev,
                    clickOutsideToClose: true,
                    fullscreen: $scope.customFullscreen
                })
            }
            else if (modalId == "2") {
                $mdDialog.show({
                    controller: DialogController,
                    templateUrl: 'modal1.html',
                    parent: angular.element(document.body),
                    targetEvent: ev,
                    clickOutsideToClose: true,
                    fullscreen: $scope.customFullscreen
                })
            }
            
        }
        function DialogController($scope, $mdDialog) {
            $scope.hide = function () {
                $mdDialog.hide();
            };

            $scope.cancel = function () {
                $mdDialog.cancel();
            };

            $scope.answer = function (answer) {
                $mdDialog.hide(answer);
            };
        }
        $scope.chkbxFunc = function () {
            $scope.billing = !$scope.billing;
        }

        $scope.resetcard = function () {
            $scope.cardimg = "img/plainCard.png";
            $scope.crossCvv = false;
            $scope.exclmnCvv = false;
        }


        $scope.changeIconImg = function (modelVal) {

            $scope.cardimg = "img/plainCard.png";
            if (modelVal == "" || modelVal === undefined) {
                $scope.exclmnCvv = true;
                $scope.crossCvv = false;
            }
            else {
                $scope.exclmnCvv = false;
                $scope.crossCvv = true;
            }

        }
        $scope.resetModal = function () {
            $scope.cardimg = "img/plainCard.png";
            $scope.checkModals = "0";
        }
        


    }])
	


	.controller('custommodalcontroller', ['$scope', function($scope) {
          
		  var modalID = document.getElementById('custom-myModal');
		  
		  $scope.closeCustomModal = function($scope)
		  {
			  modalID.style.display = "none";
		  }
            
	}])


 .controller('checkAutopay', ['$scope', function($scope) {
          
		  var autopayoff = 1;
		  $scope.imgfile="img/on.jpg";
          $scope.changeautopay = function() {
			if(autopayoff == 1)
			{
			$scope.imgfile="img/off.jpg";
			autopayoff = 0;
			}
			else
			{
			$scope.imgfile="img/on.jpg";
			autopayoff = 1;
			}
		};
            
    }])
	 .controller('checkAutopayoff', ['$scope', function($scope) {
		  var autopayoff = 0;
		  $scope.imgfile="img/off.jpg";
          $scope.changeautopay = function() {
			if(autopayoff == 1)
			{
			$scope.imgfile="img/off.jpg";
			autopayoff = 0;
			}
			else
			{
			$scope.imgfile="img/on.jpg";
			autopayoff = 1;
			}
		};
            
    }])
  .controller('DropdownController', function ($scope) {
			
        $scope.value=true;
        $scope.value2=false;
			
        $scope.DropDownValue=function(val){
            $scope.selectedVal=val;
            $scope.value=false;
            $scope.value2=true;
        }
  })
        .controller('CarouselDemoCtrl', ['$scope', function ($scope) {
            $scope.myInterval = 5000;
            $scope.noWrapSlides = false;
            $scope.active = 0;
            var slides = $scope.slides = [];
            var currIndex = 0;
            $scope.addSlide = function () {
                slides.push({
                    image: 'img/device.png',
                    //text: ['Nice image', 'Awesome photograph', 'That is so cool'][slides.length % 3],
                    id: currIndex++
                });
            };

            for (var i = 0; i < 4; i++) {
                $scope.addSlide();
            }

            $scope.next = function () {
               // alert("next");
            }

        }])
  .controller('ModalDemoCtrl1', ['$scope', function($scope)  {
      $scope.displayModal = false;
     $scope.openModal = function() {
     $scope.displayModal = true;
      $scope.ModalBlack = {
           "background":"rgba(0,0,0,0.9)"
          }

        };

              $scope.closeModal = function() {
                   $scope.displayModal = false;
                   $scope.ModalBlack = {
                   "background": "rgba(255,255,255)"

                  }

        };

 

    }])
.controller('radioController', ['$scope', function ($scope) {
    $scope.isSelected1 = false;
    $scope.isSelected2 = false;
    $scope.radioVal1 = false;
    $scope.radioVal2 = false;
    $scope.radioisSelected = true;

    $scope.checked = function (checkOption) {
        if (checkOption == "option1") {
            $scope.isSelected1 = true;
        }
        else {
            $scope.isSelected1 = false;
        }


        if (checkOption == "option2") {
            $scope.isSelected2 = true;
        }
        else {
            $scope.isSelected2 = false;
        }

    }
    $scope.setValue = function (event) {
        $scope.radioisSelected = false;
        if (event.target.value == "option1") {
            $scope.isSelected1 = true;
        }
        else {
            $scope.isSelected1 = false;
        }
        if (event.target.value == "option2") {
            $scope.isSelected2 = true;
        }
        else {
            $scope.isSelected2 = false;
        }
    }

    $scope.showContent = function (id) {
        if (id == "div1") {
            $scope.radioVal1 = !$scope.radioVal1;
            if ($scope.radioVal1 == true) {
                $scope.expand1 = true;
                $scope.expand2 = false;
            }
            else {
                $scope.expand1 = false;
            }       
        }
        else {
            $scope.radioVal1 = false;
            $scope.expand1 = false;
        }
        if (id == "div2") {
            $scope.radioVal2 = !$scope.radioVal2;
            if ($scope.radioVal2 == true) {
                $scope.expand2 = true;
                $scope.expand1 = false;
            }
            else {
                $scope.expand2 = false;
            }
        }
        else {
            $scope.radioVal2 = false;
            $scope.expand2 = false;
        }
    }

}])


        .controller('colorPickerController', ['$scope', function ($scope) {
            $scope.isSelected1 = false;
            $scope.isSelected2 = false;
            $scope.isSelected3 = false;
            $scope.isSelected4 = false;
            $scope.isSelected5 = false;
            $scope.isSelected6 = false;
            $scope.colorisSelected = true;
            $scope.setValue = function (event) {
                $scope.colorisSelected = false;
                if (event.target.value == "space_gray") {
                    $scope.isSelected1 = true;
                }
                else {
                    $scope.isSelected1 = false;
                }
                if (event.target.value == "light_gray") {
                    $scope.isSelected2 = true;
                }
                else {
                    $scope.isSelected2 = false;
                }
                if (event.target.value == "gold") {
                    $scope.isSelected3 = true;
                }
                else {
                    $scope.isSelected3 = false;
                }
                if (event.target.value == "rose_gold") {
                    $scope.isSelected4 = true;
                }
                else {
                    $scope.isSelected4 = false;
                }
                if (event.target.value == "rose_gold2") {
                    $scope.isSelected5 = true;
                }
                else {
                    $scope.isSelected5 = false;
                }
                if (event.target.value == "rose_gold3") {
                    $scope.isSelected6 = true;
                }
                else {
                    $scope.isSelected6 = false;
                }
            }
            $scope.color1=function(){
                $scope.isSelected1 = true;
            }

        }])


	  .controller('ModalDemoCtrl1', ['$scope', function($scope)  {
	    $scope.displayModal = false;
		$scope.openModal = function() {
		    $scope.displayModal = true;
		    $scope.ModalBlack = {
		        "background-color":"black"
		    }
        };
		$scope.closeModal = function() {
		    $scope.displayModal = false;
		    $scope.ModalBlack = {
		        "background-color": "white"
		    }
        };

    }])

    .controller('ModalInstanceCtrl', ['$scope', '$uibModalInstance', function($scope, $uibModalInstance) {

        $scope.ok = function() {
            $uibModalInstance.close();
        };

        $scope.cancel = function() {
            $uibModalInstance.dismiss('cancel');
        };
    }])
	
	.controller('styleCtrl', ['$scope', function($scope) {

	    $scope.increment=105;
		
		$scope.initialWidth=0;
		$scope.initialMarginLeft=-239;
		
		$scope.newWidth=0+$scope.increment;
		$scope.newMarginLeft=(-239+$scope.increment);
		
		
		$scope.mywidth= {
        "width":$scope.newWidth+"px "
		}
		
		$scope.customMarginLeft= {
        "margin-left":$scope.newMarginLeft+"px"
		}
		
		
    }])
      

          .controller('clearCntrl', ['$scope', '$mdDialog', function ($scope, $mdDialog, $element) {
              $scope.customFullscreen = false;
              $scope.accountName = "";
              
              $scope.openDialog1 = function() {
                $mdDialog.show({
                     parent: angular.element(document.body),
                     clickOutsideToClose:true,
                     templateUrl:'modalCTA1.html'                  ,
                    locals: {                  
                    },
                    controller: DialogController
                  });
                      
                  function DialogController($scope, $mdDialog) {
                    $scope.closeDialog = function() {
                      
                    };
                    $scope.hide = function () {
                        $mdDialog.hide();
    
                    };
    
                    $scope.cancel = function () {
    
                        $mdDialog.cancel();
    
                    };
                  };
              }            
            
          }]) 



    .controller('scrollCtrl', ['$scope', '$element', function($scope, $element) {

        $scope.visibleElement = true;

        angular.element(window).bind("scroll", function() {
             //console.log("scrolling screen"+window.innerHeight);
             
             element = document.getElementById('stickyfooter');

            if(element.getBoundingClientRect().top+40<=window.innerHeight)
            {
                $scope.visibleElement = false;
                 //console.log("scrolling Ele "+$scope.visibleElement );  
                $scope.$apply();
            }
            else
            {
                $scope.visibleElement = true;
                 //console.log("scrolling Ele "+$scope.visibleElement );  
                $scope.$apply();
            }

            
        });

        
    }])

})();
