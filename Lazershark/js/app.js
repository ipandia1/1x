element = "";

(function () {
    'use strict';
    var app = angular.module('app', ['ngAnimate', 'ngAria', 'ngCookies', 'ngMessages', 'ngSanitize', 'ngTouch', 'ui.bootstrap', 'ngMaterial']);

    angular.module("uib/template/modal/window.html", []).run(["$templateCache", function ($templateCache) {
        $templateCache.put("uib/template/modal/window.html",
            "  <button type=\"button\" class=\"close pull-right uib-close\" ng-click=\"close($event)\"><i aria-hidden=\"true\" class=\"ico-closeIcon uib-close\" ng-click=\"close($event)\"></i></button><div class=\"modal-dialog {{size ? 'modal-' + size : ''}}\"><div class=\"modal-content\" uib-modal-transclude></div></div>\n" +
            "");
    }]);

    app.directive('erroricon', ['$parse', '$compile', '$window', function ($parse, $compile, $window) {
        return {
            restrict: 'A',
            require: ['ngModel'],
            link: function (scope, element, attrs, ctrls) {
                var model = ctrls[0];
                scope.showThis = false;
                var template = $compile('<i class="fa fa-exclamation-circle mobile" ng-show="showThis" style="position: absolute; top: 0px; right: -8px; margin: 8px 0px;outline:0px;color:red;left:95%"></i><span ng-show="showThis" style="position:absolute;top:40px;width:100%;height:100%;right:0;z-index:10;font-size:11px;color: #ff0000;line-height:12px;padding-top:5px;">Invalid code. Invalid code. Invalid code. Invalid code. </span>')(scope);
                element.after(template);


                var reg = /^[^`~!@#$%\^&*()_+={}|[\]\\:';"<>?,./]*$/;
                function fromUser(text) {
                    scope.showThis = false;
                    scope.el = element[0].classList;
                    var reg = /^[^`~!@#$%\^&*()_+={}|[\]\\:';"<>?,./]*$/;
                    if (!text.match(reg)) {
                        scope.showThis = true;
                    }
                    else {
                        scope.showThis = false;
                    }
                    scope.changeClr = true;
                    if (text == "") {
                        scope.changeClr = false;
                    }

                }
                model.$parsers.push(fromUser);
            }

        }
    }]);



    app.controller('generalController', ['$scope', function ($scope) {

    }])


        .controller('custommodalcontroller', ['$scope', function ($scope) {

            var modalID = document.getElementById('custom-myModal');

            $scope.closeCustomModal = function ($scope) {
                modalID.style.display = "none";
            }

        }])

        .controller('checkAutopay', ['$scope', function ($scope) {

            var autopayoff = 1;
            $scope.imgfile = "img/on.jpg";
            $scope.changeautopay = function () {
                if (autopayoff == 1) {
                    $scope.imgfile = "img/off.jpg";
                    autopayoff = 0;
                }
                else {
                    $scope.imgfile = "img/on.jpg";
                    autopayoff = 1;
                }
            };

        }])
        .controller('checkAutopayoff', ['$scope', function ($scope) {
            var autopayoff = 0;
            $scope.imgfile = "img/off.jpg";
            $scope.changeautopay = function () {
                if (autopayoff == 1) {
                    $scope.imgfile = "img/off.jpg";
                    autopayoff = 0;
                }
                else {
                    $scope.imgfile = "img/on.jpg";
                    autopayoff = 1;
                }
            };

        }])
        .controller('DropdownController', function ($scope) {

            $scope.value = true;
            $scope.value2 = false;

            $scope.DropDownValue = function (val) {
                $scope.selectedVal = val;
                $scope.value = false;
                $scope.value2 = true;
            }
        })
        .controller('CarouselDemoCtrl', ['$scope', function ($scope) {
            $scope.myInterval = 5000;
            $scope.noWrapSlides = false;
            $scope.active = 0;
            var slides = $scope.slides = [];
            var currIndex = 0;
            $scope.addSlide = function () {
                slides.push({
                    image: 'img/device.png',
                    //text: ['Nice image', 'Awesome photograph', 'That is so cool'][slides.length % 3],
                    id: currIndex++
                });
            };

            for (var i = 0; i < 4; i++) {
                $scope.addSlide();
            }

            $scope.next = function () {
                alert("next");
            }

        }])
        .controller('ModalDemoCtrl1', ['$scope', function ($scope) {
            $scope.displayModal = false;
            $scope.openModal = function () {
                $scope.displayModal = true;
                $scope.ModalBlack = {
                    "background": "rgba(0,0,0,0.9)"
                }

            };

            $scope.closeModal = function () {
                $scope.displayModal = false;
                $scope.ModalBlack = {
                    "background": "rgba(255,255,255)"

                }

            };



        }])
        .controller('radioController', ['$scope', function ($scope) {
            $scope.isSelected1 = false;
            $scope.isSelected2 = false;
            $scope.radioVal1 = false;
            $scope.radioVal2 = false;
            $scope.radioisSelected = true;

            $scope.checked = function (checkOption) {
                if (checkOption == "option1") {
                    $scope.isSelected1 = true;
                }
                else {
                    $scope.isSelected1 = false;
                }


                if (checkOption == "option2") {
                    $scope.isSelected2 = true;
                }
                else {
                    $scope.isSelected2 = false;
                }

            }
            $scope.setValue = function (event) {
                $scope.radioisSelected = false;
                if (event.target.value == "option1") {
                    $scope.isSelected1 = true;
                }
                else {
                    $scope.isSelected1 = false;
                }
                if (event.target.value == "option2") {
                    $scope.isSelected2 = true;
                }
                else {
                    $scope.isSelected2 = false;
                }
            }

            $scope.showContent = function (id) {
                if (id == "div1") {
                    $scope.radioVal1 = !$scope.radioVal1;
                    if ($scope.radioVal1 == true) {
                        $scope.expand1 = true;
                        $scope.expand2 = false;
                    }
                    else {
                        $scope.expand1 = false;
                    }
                }
                else {
                    $scope.radioVal1 = false;
                    $scope.expand1 = false;
                }
                if (id == "div2") {
                    $scope.radioVal2 = !$scope.radioVal2;
                    if ($scope.radioVal2 == true) {
                        $scope.expand2 = true;
                        $scope.expand1 = false;
                    }
                    else {
                        $scope.expand2 = false;
                    }
                }
                else {
                    $scope.radioVal2 = false;
                    $scope.expand2 = false;
                }
            }

        }])


        .controller('colorPickerController', ['$scope', function ($scope) {
            $scope.isSelected1 = false;
            $scope.isSelected2 = false;
            $scope.isSelected3 = false;
            $scope.isSelected4 = false;
            $scope.isSelected5 = false;
            $scope.isSelected6 = false;
            $scope.colorisSelected = true;
            $scope.setValue = function (event) {
                $scope.colorisSelected = false;
                if (event.target.value == "space_gray") {
                    $scope.isSelected1 = true;
                }
                else {
                    $scope.isSelected1 = false;
                }
                if (event.target.value == "light_gray") {
                    $scope.isSelected2 = true;
                }
                else {
                    $scope.isSelected2 = false;
                }
                if (event.target.value == "gold") {
                    $scope.isSelected3 = true;
                }
                else {
                    $scope.isSelected3 = false;
                }
                if (event.target.value == "rose_gold") {
                    $scope.isSelected4 = true;
                }
                else {
                    $scope.isSelected4 = false;
                }
                if (event.target.value == "rose_gold2") {
                    $scope.isSelected5 = true;
                }
                else {
                    $scope.isSelected5 = false;
                }
                if (event.target.value == "rose_gold3") {
                    $scope.isSelected6 = true;
                }
                else {
                    $scope.isSelected6 = false;
                }
            }
            $scope.color1 = function () {
                $scope.isSelected1 = true;
            }

        }])


        .controller('ModalDemoCtrl1', ['$scope', function ($scope) {
            $scope.displayModal = false;
            $scope.openModal = function () {
                $scope.displayModal = true;
                $scope.ModalBlack = {
                    "background-color": "black"
                }
            };
            $scope.closeModal = function () {
                $scope.displayModal = false;
                $scope.ModalBlack = {
                    "background-color": "white"
                }
            };

        }])

        .controller('ModalInstanceCtrl', ['$scope', '$uibModalInstance', function ($scope, $uibModalInstance) {

            $scope.ok = function () {
                $uibModalInstance.close();
            };

            $scope.cancel = function () {
                $uibModalInstance.dismiss('cancel');
            };
        }])

        .controller('styleCtrl', ['$scope', function ($scope) {

            $scope.increment = 105;

            $scope.initialWidth = 0;
            $scope.initialMarginLeft = -239;

            $scope.newWidth = 0 + $scope.increment;
            $scope.newMarginLeft = (-239 + $scope.increment);


            $scope.mywidth = {
                "width": $scope.newWidth + "px "
            }

            $scope.customMarginLeft = {
                "margin-left": $scope.newMarginLeft + "px"
            }


        }])

        .controller('scrollCtrl', ['$scope', '$element', function ($scope, $element) {

            $scope.visibleElement = true;

            angular.element(window).bind("scroll", function () {

                element = document.getElementById('stickyfooter');

                if (element.getBoundingClientRect().top + 40 <= window.innerHeight) {
                    $scope.visibleElement = false;
                    $scope.$apply();
                }
                else {
                    $scope.visibleElement = true;
                    $scope.$apply();
                }
            });
        }])

    app.controller('accessoriesCtrl', ['$scope', '$http', function ($scope, $http) {
        $http.get("./json/accessories.json").then(function (response) {
            $scope.accessories = response.data.accessories;
        });

        $scope.strikeText = function (strikeText) {
            return $scope.strikeAccessoryemi = "<del>" + strikeText + "</del>"
        };
        $scope.footerHide = false;
        $scope.checkboxClick = function (checked) {

            $scope.footerHide = true;

        };
    }])

    app.controller('cartTabsCtrl', ['$scope', '$window', function ($scope, $window) {
        $scope.slashed = true;
        $scope.selectedTabIndex = 0;
        $scope.orderDetailViewed = false;
        $scope.shippingPageViewed = false;
        $scope.paymentPageViewed = false;
        $scope.newAddress = false;
        $scope.newE911Address = false;
        $scope.newPrimaryAddress = false;
        $scope.existingAddress = true;
        $scope.existingE911Address = true;
        $scope.existingPrimaryAddress = true;
        $scope.existingPromoCode = true;
        $scope.hide = false;
        $scope.checked = true;
        $scope.checked1 = true;
        $scope.methodE911Mobile = true;
        $scope.methodPrimaryMobile = true;
        $scope.titleShipping = "Shipping address";
        $scope.titleE911Shipping = "E911 address";
        $scope.titlePrimaryShipping = "Primary place of use address";
        $scope.mobileTabs = true;
        $scope.mobileE911Tabs = true;
        $scope.mobilePrimaryTabs = true;
        $scope.buttonsMobile = false;
        $scope.buttonsE911Mobile = false;
        $scope.buttonsPrimaryMobile = false;
        $scope.newAddressMobile = false;
        $scope.newE911AddressMobile = false;
        $scope.newPrimaryAddressMobile = false;
        $scope.mobileTabs1 = true;

        /*   $scope.payment = {
            address1: '1234 Main St.',
            address2: 'Suite 001',
            city: 'Rancho Santa Margarita',
            state:'CA',
            zip:'10001'
            };
            $scope.paymentE911 = {
                address1: '2345 Main St.',
                address2: 'Suite 001',
                city: 'Rancho Santa Margarita',
                state:'CA',
                zip:'10001'
                };        
                $scope.paymentPrimary = {
                    address1: '3456 Main St.',
                    address2: 'Suite 001',
                    city: 'Rancho Santa Margarita',
                    state:'CA',
                    zip:'10001'
                    };       */
     

        $scope.checkboxClick = function () {

            $scope.hide = !$scope.hide;
            $scope.checked = !$scope.checked;
            if ($window.innerWidth >= 768) {

                var theHeight = angular.element(document.querySelector('.paymentHtCalc'));
                var theHeight1 = theHeight[0].offsetHeight;
                if (theHeight[0].offsetHeight <= 493) {
                    theHeight1 = 849;
                } else {
                    theHeight1 = 423;
                }

                var theHeightReCalc = document.getElementsByClassName("heightReCalc");
                var height1 = $window.innerHeight;
                if (height1 < (theHeight1 + 184)) {
                    var recal = theHeight1 - 222 + 35 + "px";
                    theHeightReCalc[0].style.minHeight = recal;
                } else {
                    var recal2 = height1 - 406 + 35 + "px";
                    theHeightReCalc[0].style.minHeight = recal2;
                }
            }
        };

        $scope.add = function () {
            $scope.newAddress = true;
            $scope.existingAddress = false;
            $scope.buttonsMobile = false;
            $scope.shippingDynamicHt('Add');
        }

        $scope.addE911 = function () {
            $scope.newE911Address = true;
            $scope.existingE911Address = false;
            $scope.buttonsMobile = false;
            $scope.shippingDynamicHt('Add');
        }

        $scope.addPrimary = function () {
            $scope.newPrimaryAddress = true;
            $scope.existingPrimaryAddress = false;
            $scope.buttonsMobile = false;
            $scope.shippingDynamicHt('Add');
        }

        $scope.shippingDynamicHt = function (val) {
            if ($window.innerWidth >= 768) {
                var shippingAddrHeight = angular.element(document.querySelector('.shippingHtCalc'))[0].offsetHeight;
                if (val == 'Add') {
                    shippingAddrHeight = shippingAddrHeight + 342;
                } else if (val == 'Cancel') {
                    shippingAddrHeight = shippingAddrHeight - 342 + 28;
                }
                var shippingMethodHeight = document.getElementsByClassName("shipHtReCalc");
                var windowHeight = $window.innerHeight;
                if (windowHeight < (shippingAddrHeight)) {
                    var recal = shippingAddrHeight + 30 + "px";
                    shippingMethodHeight[0].style.minHeight = recal;
                } else {
                    var recal = shippingAddrHeight - 30 + "px";
                    shippingMethodHeight[0].style.minHeight = recal;
                }
            }
            else {
                element.css('min-height', 473 + 'px')
            }
        }
        $scope.cancelShippingAddr = function () {
            $scope.newAddress = false;
            $scope.existingAddress = true;
            $scope.shippingDynamicHt('Cancel');
        }
        $scope.cancelE911Addr = function () {
            $scope.newE911Address = false;
            $scope.existingE911Address = true;
            $scope.shippingDynamicHt('Cancel');
        }
        $scope.cancelPrimaryAddr = function () {
            $scope.newPrimaryAddress = false;
            $scope.existingPrimaryAddress = true;
            $scope.shippingDynamicHt('Cancel');
        }
        $scope.cancelPaymentAddr = function () {
            $scope.hide = !$scope.hide;
        }

        $scope.addPaymentMobile = function () {
            $scope.newAddressMobile = true;
            $scope.existingAddress = false;
            $scope.mobileTabs = false;
            $scope.buttonsMobile = true;
            if ($window.innerWidth < 768) {
                $scope.titleShipping = "Payment address";
            }
            /*    $scope.payment = {
                   address1: '1234 Main St.',
                   address2: 'Suite 001',
                   city: 'Rancho Santa Margarita',
                   state:'CA',
                   zip:'10001'
                   }; */
        }
        $scope.addMobile = function () {
            $scope.newAddressMobile = true;
            $scope.mobileTabs = false;
            $scope.buttonsMobile = true;
            if ($window.innerWidth < 768) {
                $scope.titleShipping = "Shipping address";
            }
            /*  $scope.payment = {
                 address1: '1234 Main St.',
                 address2: 'Suite 001',
                 city: 'Rancho Santa Margarita',
                 state:'CA',
                 zip:'10001'
                 }; */
        }
        $scope.addE911Mobile = function () {
            $scope.newAddressMobile = true;
            $scope.mobileTabs = false;
            $scope.buttonsMobile = true;
            if ($window.innerWidth < 768) {
                $scope.titleShipping = "E911 address";
            }
            /*   $scope.payment = {
                  address1: '2345 Main St.',
                  address2: 'Suite 001',
                  city: 'Rancho Santa Margarita',
                  state:'CA',
                  zip:'10001'
                  }; */
        }
        $scope.addPrimaryMobile = function () {
            $scope.newAddressMobile = true;
            $scope.mobileTabs = false;
            $scope.buttonsMobile = true;
            if ($window.innerWidth < 768) {
                $scope.titleShipping = "Primary place of use address";
            }
            /*     $scope.payment = {
                    address1: '3456 Main St.',
                    address2: 'Suite 001',
                    city: 'Rancho Santa Margarita',
                    state:'CA',
                    zip:'10001'
                    }; */
        }
        $scope.addressUpdate = function () {
            $scope.newAddress = false;
            $scope.existingAddress = true;
            $scope.buttonsMobile = false;
            $scope.mobileTabs = true;
            $scope.titleShipping = "Shipping address";
            $scope.newAddressMobile = false;
        }
        $scope.addressE911Update = function () {
            $scope.newE911Address = false;
            $scope.existingE911Address = true;
            $scope.buttonsMobile = false;
            $scope.mobileTabs = true;
            $scope.titleShipping = "E911 address";
            $scope.newAddressMobile = false;
        }
        $scope.addressPrimaryUpdate = function () {
            $scope.newPrimaryAddress = false;
            $scope.existingPrimaryAddress = true;
            $scope.buttonsMobile = false;
            $scope.mobileTabs = true;
            $scope.titleShipping = "Primary place of use address";
            $scope.newAddressMobile = false;
        }
        $scope.primaryAddressModal = function () {
            document.body.classList.add("noscroll");
            $scope.visibleLCModal = true;
            $scope.visibleAnimation = true;
        }
        $scope.E911Modal = function () {
            //document.body.classList.add("noscroll");
            $scope.visibleLCModal = true;
            $scope.visibleAnimation = true;
        }

        $scope.closeModal = function () {
            document.body.classList.remove("noscroll");
            $scope.visibleLCModal = false;
            $scope.visibleAnimation = false;
        }
        $scope.addPromoCode = function () {
            $scope.promoCode = true;
            $scope.existingPromoCode = false;
        }
        $scope.applyCode = function () {
            $scope.successText = true;
            $scope.promoCode = true;
            $scope.changeClr = false;
            $scope.successText1 = true;
            $scope.successText2 = true;
            $scope.successText3 = true;
            this.payment = null;
        }
        $scope.remove1 = function () {
            $scope.successText1 = false;
        }
        $scope.remove2 = function () {
            $scope.successText2 = false;
        }

        $scope.change1 = function (val) {
            $scope.changeClr = true;
            if (val == "" || val.length < 4) {
                $scope.changeClr = false;
            }
        }

        $scope.page1 = false;
        $scope.page2 = false;
        $scope.page3 = false;

        $scope.accordianData = [{
            "heading": "1. Order detail",
            "check": false,
            "id": "0"
        }, {
            "heading": "2. Shipping",
            "check": false,
            "id": "1"
        }, {
            "heading": "3. Payment",
            "check": false,
            "id": "2"
        }];

        $scope.onLoadFun = function (data) {
            for (var i in $scope.accordianData) {
                if ($scope.accordianData[0] != data) {
                    $scope.accordianData[0].expanded = true;
                    $scope.page1 = true;
                }
            }
        }

        $scope.collapseAll = function (data) {

            for (var i in $scope.accordianData) {
                if ($scope.accordianData[i] != data) {
                    $scope.accordianData[i].expanded = false;
                }
            }

            if ($scope.accordianData[0].heading == data.heading) {
                $scope.accordianData[0].expanded = true;
                $scope.page1 = true;
                $scope.page2 = false;
                $scope.page3 = false;
            }
            else if ($scope.accordianData[1].heading == data.heading) {
                $scope.accordianData[1].expanded = true;

                $scope.page1 = false;
                $scope.page2 = true;
                $scope.page3 = false;
            }
            else if ($scope.accordianData[2].heading == data.heading) {
                $scope.accordianData[2].expanded = true;
                $scope.page1 = false;
                $scope.page2 = false;
                $scope.page3 = true;
            }
        };

        $scope.moveToShipping = function () {
            if ($window.innerWidth < 768) { /*Accordian View Logic for Mobile*/
                $scope.accordianData[0].expanded = false;
                $scope.accordianData[1].expanded = true;
                $scope.page1 = false;
                $scope.page2 = true;
                $scope.page3 = false;
                $scope.accordianData[0].check = true;
                $scope.accordianData[0].heading = "Order detail";
            } else { /*Tab View Logic for Tablets, desktops*/
                $scope.orderDetailViewed = true;
                $scope.selectedTabIndex = 1;
            }

        }

        $scope.moveToPayment = function () {
            if ($window.innerWidth < 768) { /*Accordian View for Mobile*/
                $scope.accordianData[1].expanded = false;
                $scope.accordianData[2].expanded = true;
                $scope.page1 = false;
                $scope.page2 = false;
                $scope.page3 = true;
                $scope.accordianData[1].check = true;
                $scope.accordianData[1].heading = "Shipping";
            } else {  /*Tab View Logic for Tablets, desktops*/
                $scope.shippingPageViewed = true;
                $scope.selectedTabIndex = 2;
            }
        }

        $scope.checkMark = function (labelText) {
            return "<i class='fa fa-check color-green'></i> " + labelText;
        };



    }])

    app.controller('accessoriesCarouselCtrl', ['$scope', '$http', function ($scope, $http) {
        $scope.offers = "";
        $scope.offerDataIndex = 0;
        $scope.offerMaxIndex = 0;
        $http.get("./json/offers.json").then(function (response) {
            $scope.offers = response.data.offers;
            $scope.offerDataDesc = $scope.offers[$scope.offerDataIndex].desc;
            $scope.offerDataUrl = $scope.offers[$scope.offerDataIndex].url;
            $scope.offerMaxIndex = $scope.offers.length - 1;
        });

        $scope.moveToNext = function () {
            if ($scope.offerDataIndex <= $scope.offerMaxIndex) {
                $scope.offerDataIndex++;
                $scope.offerDataDesc = $scope.offers[$scope.offerDataIndex].desc;
                $scope.offerDataUrl = $scope.offers[$scope.offerDataIndex].url;
            }
        };
        $scope.moveToPrevious = function () {
            if ($scope.offerDataIndex > 0) {
                $scope.offerDataIndex--;
                $scope.offerDataDesc = $scope.offers[$scope.offerDataIndex].desc;
                $scope.offerDataUrl = $scope.offers[$scope.offerDataIndex].url;
            }
        };
    }])

    app.controller('accessoriesFooterCtrl', ['$scope', function ($scope) {
        $scope.footerHide = false;
        $scope.checkboxAccessories = function () {
            $scope.footerHide = !$scope.footerHide;
        };

        $scope.payMonthly = true;
        $scope.payFull = false;
        $scope.payMonthlyFunction = function () {
            $scope.payMonthly = true;
            $scope.payFull = false;
        }
        $scope.payFullFunction = function () {
            $scope.payMonthly = false;
            $scope.payFull = true;
        }
    }])
    app.directive('setHeightPayment', function ($window) {
        return {
            link: function (scope, element, attrs) {
                scope.winHeight = function () {
                    if ($window.innerWidth >= 768) {
                        var height1 = $window.innerHeight;
                        element.css('min-height', (height1 - 406) + 'px'); //406 is height of all the elements which are above the specified element.

                    }
                    else {
                        element.css('min-height', (0) + 'px')
                    }
                }
                scope.winHeight();

                angular.element(window).bind('resize', function () {
                    scope.winHeight();
                    scope.$digest();
                });
            }
        }
    })
    app.directive('setHeightShipping', function ($window) {
        return {
            link: function (scope, element, attrs) {
                scope.winHeight = function () {
                    if ($window.innerWidth >= 768) {

                        var height1 = $window.innerHeight;
                        element.css('min-height', (height1 - 184) + 'px');
                    }
                    else {
                        element.css('min-height', (0) + 'px')
                    }
                }
                scope.winHeight();

                angular.element(window).bind('resize', function () {
                    scope.winHeight();
                    scope.$digest();
                });
            }
        }
    })
    app.directive('setHeightOrder', function ($window) {
        return {
            link: function (scope, element, attrs) {
                scope.winHeight = function () {
                    if ($window.innerWidth >= 768) {

                        var theHeightR = angular.element(document.querySelector('.rightHeight'));
                        var theHeightR1 = theHeightR[0].offsetHeight;

                        if (($window.innerHeight - 184) > (theHeightR1 - 90)) {
                            element.css('min-height', ($window.innerHeight - 184) + 'px')
                        } else {
                            element.css('min-height', (theHeightR1) + 'px')
                        }
                    }
                    else {
                        var OffsetHeight1 = document.body.offsetHeight;
                        element.css('min-height', (0) + 'px');
                    }
                }
                scope.winHeight();

                angular.element(window).bind('resize', function () {
                    scope.winHeight();
                    scope.$digest();
                });
            }
        }
    })

    app.directive('setHeightScroll', function ($window) {
        return {
            link: function (scope, element, attrs) {
                scope.winHeight = function () {
                    if ($window.innerWidth >= 768) {
                        var OffsetHeight1 = $window.innerHeight;
                        var theHeightR = angular.element(document.querySelector('.rightHeight'));
                        var theHeightR1 = theHeightR[0].offsetHeight;
                        var leftHeight = angular.element(document.querySelector('.scrollClass'));
                        var leftHeight1 = leftHeight[0].offsetHeight;
                        if ((OffsetHeight1 - 184) < theHeightR1) {
                            //element.css('max-height', (theHeightR1) + 'px');
                            element.css('max-height', (theHeightR1 - 90) + 'px');

                        }
                        else {
                            if ((OffsetHeight1 - 184) >= leftHeight1) {

                                element.css('min-height', (OffsetHeight1 - 234) + 'px');
                            } else {
                                //element.css('max-height', (theHeightR1) + 'px');
                                element.css('max-height', (OffsetHeight1 - 234) + 'px');
                            }

                            //element.css('max-height', (OffsetHeight1 - 184) + 'px');

                        }
                    }

                    else {
                        var OffsetHeight1 = document.body.offsetHeight;
                        element.css('min-height', (0) + 'px');
                    }
                }
                scope.winHeight();

                angular.element(window).bind('resize', function () {
                    scope.winHeight();
                    scope.$digest();
                });
            }
        }
    })

    app.directive('numbervaluesonly', function () {
        return {
            restrict: 'A',
            link: function (scope, element, attrs, ctrls) {
                element.on('keydown', function (event) {
                    if (event.which == 64 || event.which == 16) {
                        return false;
                    } else if (event.which >= 48 && event.which <= 57) {
                        return true;
                    } else if (event.which >= 96 && event.which <= 105) {
                        return true;
                    } else if ([8, 9, 13, 27, 37, 38, 39, 40].indexOf(event.which) > -1) {
                        return true;
                    } else {
                        event.preventDefault();
                        return false;
                    }

                });

            }
        }
    });

    app.directive('format', ['$filter', function ($filter) {
        return {
            require: '?ngModel',
            link: function (scope, elem, attrs, ctrl) {

                if (!ctrl) return;
                ctrl.$parsers.unshift(function (viewValue) {
                    var plainNumber = viewValue.replace(/[^\d|\-+|\.+]/g, '');
                    plainNumber = plainNumber.replace(/(\d{4})/g, '$1 ').replace(/(^\s+|\s+$)/, '');
                    elem.val(plainNumber);
                    return plainNumber;
                });
            }
        };
    }]);

    app.directive('expiryformat', ['$filter', function ($filter) {
        return {
            require: '?ngModel',
            link: function (scope, elem, attrs, ctrl) {
                if (!ctrl) return;
                ctrl.$parsers.unshift(function (plainNumber) {
                    if (plainNumber.length == 2) {
                        plainNumber = plainNumber + "/";

                    }
                    elem.val(plainNumber);
                    return plainNumber;
                });
            }
        };
    }])
    app.directive('starRating', function () {
        return {
            restrict: 'A',
            template: '<ul class="rating">' +
                '<li ng-repeat="star in stars" class="fa fa-star-o" ng-class="star" ng-click="toggle($index)" aria-label="star {{$index +1}}" tabindex="1">' +

                '</li>' +
                '</ul>',
            scope: {
                ratingValue: '=',
                max: '=',
                onRatingSelected: '&'
            },

            link: function (scope, elem, attrs) {

                var updateStars = function () {
                    scope.stars = [];

                    scope.fillstar = false;
                    for (var i = 0; i < scope.max; i++) {
                        scope.stars.push({
                            filled: i < scope.ratingValue,
                            fastaro: i < scope.ratingValue
                        });
                        scope.fillstar = true;
                    }
                };

                scope.toggle = function (index) {
                    scope.ratingValue = index + 1;
                    scope.onRatingSelected({
                        rating: index + 1
                    });
                    var nextElement = document.getElementById('tMobileBusiness');
                 nextElement.focus();         
                };

                scope.$watch('ratingValue', function (oldVal, newVal) {
                    if (newVal) {
                        updateStars();
                    }
                });
            }
        }
    })
        .controller('orderCtrl', ['$scope', '$http', '$window', '$location', '$timeout', function ($scope, $http, $window, $location, $timeout) {
            $scope.phonevariations = 0;   //change the value to 0/1 to see phone varients

            $scope.simvarientheading = [
                'SIM Starter Kit',
                'SIM Starter Kit',
                'SIM Starter Kit',
                "T-Mobile® 3-in-1 SIM Starter Kit",
                "T-Mobile® 3-in-1 SIM Starter Kit",
                "SIM Starter Kit"];
            $scope.simvarienttype = 5; //change the value to 0/1/2/3/4/5 to see SIM varients
        }])
        .controller('fraudCtrl', ['$scope', '$http', function ($scope, $http) {
            $scope.showSection = [true, false, false];
            $scope.sectionNo = 0;
            $scope.visible = false;
            $scope.continue = function () {
                $scope.showSection[$scope.sectionNo] = false;
                $scope.sectionNo++;
                $scope.showSection[$scope.sectionNo] = true;
            }

            $scope.questionNo = 0;
            $scope.reviewQuestions = [{
                "Question": "In which of the following cities have you ever lived or owned property?",
                "Options": ['seattle-washington', 'san-clemente-califo', 'brooklyn-new-york', 'austin-texas', 'dallas-texas', 'none-of-the-above']

            }, {
                "Question": "In which of the following cities have you ever lived or owned property?",
                "Options": ['seattle-washington', 'san-clemente-califo', 'brooklyn-new-york', 'austin-texas', 'dallas-texas', 'none-of-the-above']
            }];
            $scope.submit = function () {
                var questionsCount = this.reviewQuestions.length;
                if ($scope.questionNo < questionsCount - 1) {
                    $scope.questionNo++;
                }
                else {
                    this.continue();
                }
            };
            $scope.backtoshopping = function () {
                $scope.showSection[$scope.sectionNo] = false;
                $scope.sectionNo = 0;
                $scope.questionNo = 0;
                $scope.showSection[$scope.sectionNo] = true;
            };
            $scope.gotopreviouspage = function () {
                if ($scope.sectionNo == 1 && $scope.questionNo != 0) {
                    $scope.questionNo--;
                } else if ($scope.sectionNo) {
                    $scope.showSection[$scope.sectionNo] = false;
                    $scope.sectionNo--;
                    $scope.showSection[$scope.sectionNo] = true;
                }
            }
            $scope.close = function () {
                $scope.visible = false;
            }
            $scope.openModal = function () {
                $scope.visible = !$scope.visible;
            }
        }])
        .controller('formCtrl', function ($scope, $http, $location, $window,$timeout) {
            $scope.onLoad = function () {
                $location.path('form1');
                this.progressBar(this.pageArray, this.page);
            }
				var appWindow = angular.element($window);

            //variables to change the display
            $scope.theme = false;//(for theme)
            $scope.leftTemp = true;// (image alignment)
            $scope.showImage = true;//(image display)
            $scope.pageArray = ['form1', 'form2', 'form3', 'form4'];
            $scope.stepHeading = ["Let’s get started.", "Tell us a bit about yourself.", "Where can we ship your device?", "You're almost done."]
            $scope.stepTitle = ["Current experience", "Your info", "Shipping address", "Confirmation"];
            $scope.index = '';
            $scope.count = '';
            $scope.isSelected = true;
						$scope.isSelectedForm1=true;
            $scope.showBox = false;
            $scope.showForm1Box = false;
            $scope.showbuttons = false;
            $scope.showSubmit = true;
            $scope.button = 'Continue';
            $scope.resentDone =false;// To see the resent-link page            
            $scope.next = function () {
               
                $timeout(function () { 
                    window.scroll(0,0);
                $scope.direction = true;
                $scope.path = $location.url().split('/')[1];
                for (var i in $scope.pageArray) {
                    if ($scope.path == $scope.pageArray[i]) {
                        $scope.count = ++i;
                        $location.path($scope.pageArray[$scope.count]);
                        $scope.page = $scope.pageArray[$scope.count];
                        break;
                    }
                }
                if ($scope.page == 'form3') {
                    $scope.showbuttons = false;
                    $scope.showSubmit = true;
                    $scope.button = 'Submit';
                }
                else if ($scope.page == 'form4') {
                    $scope.showbuttons = false;
                    $scope.showSubmit = false;
                }
                else {
                    $scope.showbuttons = true;
                    $scope.showSubmit = false;
                }
                $scope.progressBar($scope.pageArray, $scope.page);
            }, 1);
            };

            $scope.back = function () {
            
                $timeout(function () { 
                    window.scroll(0,0); 
                $scope.direction = false;
                $scope.path = $location.url().split('/')[1];
                for (var i in $scope.pageArray) {
                    if ($scope.path == $scope.pageArray[i]) {
                        $scope.count = --i;
                        $location.path($scope.pageArray[$scope.count]);
                        $scope.page = $scope.pageArray[$scope.count];
                        break;
                    }
                }
                if ($scope.page == 'form1') {
                    $scope.button = 'Continue';
                    $scope.showbuttons = false;
                    $scope.showSubmit = true;
                }
                $scope.progressBar($scope.pageArray, $scope.page);
            }, 1);
        };
            $scope.onSelect = function (val) {
                if (val == 'yes') {
                    $scope.showBox = true;
                    $scope.isSelected = false;
                } else {
								/*	if($window.innerWidth<768)	{
										document.querySelector('.form-container').style.setProperty('height','444px'); 
									}
									else{
										document.querySelector('.form-container').style.setProperty('height','557px');
									} */
                    $scope.showBox = false;
                    $scope.isSelected = true;
                }
            }
            $scope.onSelectForm1 = function (val) {
                if (val == 'yes') {
                    $scope.showForm1Box = true;
                    $scope.isSelectedForm1 = false;
                } else {
                    $scope.showForm1Box = false;
                    $scope.isSelectedForm1 = true;
                }
            }
            $scope.wirelessProviderList = [{ wirelessProvider: 'AT&T', disabled: true }, { wirelessProvider: 'T-mobile', disabled: false }, { wirelessProvider: 'Verizon', disabled: false }, { wirelessProvider: 'Lyca', disabled: false }, { wirelessProvider: 'Sprint', disabled: false }, { wirelessProvider: 'metroPCS', disabled: false }];
            $scope.wpSelectedVal = "Select from the list";
            $scope.selectedWP = function (value) {
                $scope.wpSelectedVal = value;
            }

            $scope.listValues = [{ state: 'AL', disabled: true }, { state: 'AK', disabled: false }, { state: 'AZ', disabled: false }, { state: 'AR', disabled: false }, { state: 'CA', disabled: false }, { state: 'CO', disabled: false }, { state: 'CT', disabled: false }, { state: 'DE', disabled: false }, { state: 'EL', disabled: false }, { state: 'GA', disabled: false }, { state: 'HI', disabled: false }, { state: 'ID', disabled: false }, { state: 'VI', disabled: false }, { state: 'WA', disabled: false }, { state: 'WV', disabled: false }, { state: 'WI', disabled: true }];
            $scope.selectedValWA = "Select from the list";
            $scope.selectWA = function (value) {
                $scope.selectedValWA = value;
            }
            $scope.rating = 0;
            $scope.ratings = {
                current: 2,
                max: 5
            };
            $scope.progressBar = function (steps, currentStep) {

                if (angular.isUndefined($scope.page)) {
                    $scope.index = 1;
                }
                else if ($scope.index < steps.length) {
                    $scope.index = steps.indexOf(currentStep) + 1;
                }
                var elem = document.getElementById("myBar");
                var stepsCount = steps.length;
                var width = $scope.index / stepsCount * 100;
                if (width <= 100) {
                    elem.style.width = width + '%';
                }
                if ($scope.index == steps.length) {
                    elem.style.backgroundColor = "#e20074";

                }
            }
            $scope.almostDone = true;// all done page
            $scope.doneFunction=function(){
                $scope.almostDone = false;
            }
        })
   
        app.directive('match', function($parse) {
            return {
              require: 'ngModel',
              link: function(scope, elem, attrs, ctrl) {
                scope.$watch(function() {        
                  return $parse(attrs.match)(scope) === ctrl.$modelValue;
                }, function(currentValue) {
                  ctrl.$setValidity('mismatch', currentValue);
                });
              }
            };
          });
})();

