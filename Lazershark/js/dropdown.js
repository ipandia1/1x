angular.module('app')
    .directive('dropdown', function () {
        return {
            link: function (scope, element, attrs) {
                element.bind('click', function (event, target) {
                    const selecteddropdown = element[0].querySelectorAll('.dropdown-menu');
                    element[0].focus();
                    const dropdownvalue = element[0].querySelectorAll('.dropdown-toggle');
                    const bodyrect = selecteddropdown[0].getBoundingClientRect();
                    const activeelement = element[0].querySelectorAll('.active');
                    if (selecteddropdown[0].classList.toggle('show')) {
                        selecteddropdown[0].style.marginTop = -(element[0].clientHeight) + 'px';
                    } else {
                        selecteddropdown[0].style.marginTop = 0 + 'px';
                    }
                    if (activeelement.length !== 0) {
                        selecteddropdown[0].scrollTop = activeelement[0].offsetTop;
                    }
                })
                element.bind('blur', function (event) {
                    var Alldropdowns = element[0].querySelectorAll('.dropdown-menu');
                    const hoverelement = element[0].querySelectorAll('.dropdownclass');
                    if (hoverelement.length !== 0) {
                        hoverelement[0].classList.remove('dropdownclass');
                    }
                    for (var count = 0; count < Alldropdowns.length; count++) {
                        var dropdowncontrol = Alldropdowns[count];
                        if (dropdowncontrol.classList.contains('show')) {
                            dropdowncontrol.classList.remove('show');
                        }
                    }
                })
                element.bind('keydown', function (event) {
                    const kcode = event.keyCode;
                    if (kcode === 40 || kcode === 38 || kcode === 13) {
                        const dropdownvalue = element[0].querySelectorAll('.dropdown-toggle');
                        const selecteddropdown = element[0].querySelectorAll('.dropdown-menu');
                        let hoverelement = element[0].querySelectorAll('.dropdownclass');
                        if (hoverelement.length === 0) {
                            hoverelement = element[0].querySelectorAll('.active');
                        }
                        if (hoverelement.length === 0) {
                            hoverelement = element[0].querySelectorAll('.dropdown-option');
                        }
                        hoverelement[0].classList.remove('dropdownclass');
                        let menuitemtop;
                        if (kcode === 40) {
                            if (hoverelement[0].nextElementSibling === null) {
                                menuitemtop = hoverelement[0].getBoundingClientRect();
                                hoverelement[0].classList.add('dropdownclass');
                            } else {
                                hoverelement[0].nextElementSibling.classList.add('dropdownclass');
                                const activeelement = element[0].querySelectorAll('.dropdownclass');
                                menuitemtop = hoverelement[0].nextElementSibling.getBoundingClientRect();
                                selecteddropdown[0].scrollTop = activeelement[0].offsetTop;
                            }
                            if (menuitemtop.top + 25 >= window.innerHeight) {
                            } else {
                                event.preventDefault();
                                event.stopPropagation();
                            }

                        } else if (kcode === 38) {
                            if (hoverelement[0].previousElementSibling === null || hoverelement[0].previousElementSibling.children.length === 0) {
                                hoverelement[0].classList.add('dropdownclass');
                            } else {
                                hoverelement[0].previousElementSibling.classList.add('dropdownclass');
                                const activeelement = element[0].querySelectorAll('.dropdownclass');
                                selecteddropdown[0].scrollTop = activeelement[0].offsetTop;
                            }
                            event.preventDefault();
                            event.stopPropagation();
                        } else if (kcode === 13) {
                            const dropdownvalue1 = hoverelement[0];
                            if (selecteddropdown[0].classList.contains('show')) {
                                dropdownvalue1.classList.add('dropdown-option-active');
                                dropdownvalue1.children[0].classList.add('dropdown-option-active-a');
                            }
                            dropdownvalue1.children[0].click();
                            event.preventDefault();
                            event.stopPropagation();
                        }
                    }
                })
                element.bind('keyup', function (event) {
                    const kcode = event.keyCode;
                    if (kcode === 13) {
                        // const dropdownvalue = element[0].querySelectorAll('.dropdown-toggle');
                        const selecteddropdown = element[0].querySelectorAll('.dropdown-menu');
                        let hoverelement = element[0].querySelectorAll('.dropdownclass');
                        if (hoverelement.length === 0) {
                            hoverelement = element[0].querySelectorAll('.active');
                        }
                        if (hoverelement.length === 0) {
                            hoverelement = element[0].querySelectorAll('.dropdown-option');
                        }
                        const dropdownvalue1 = hoverelement[0];
                        dropdownvalue1.classList.remove('dropdown-option-active');
                        dropdownvalue1.children[0].classList.remove('dropdown-option-active-a');
                    }
                    event.preventDefault();
                    event.stopPropagation();
                })
            }
        };
    });



