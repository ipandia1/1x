(function () {
    'use strict';
    var app = angular.module('app', ['ngAnimate', 'ngAria', 'ngAria', 'ngCookies', 'ngMessages', 'ngSanitize', 'ngTouch', 'ui.bootstrap', 'ngRoute']);

    app.config(function ($routeProvider) {
        $routeProvider.
            when("/", {
                templateUrl: "partials/device-non-nyc.html",
                controller: "deviceprotectionCtrl"
            }).
            when("/non-nyc-plans", {
                templateUrl: "partials/device-non-nyc-plans.html",
                controller: "deviceprotectionCtrl"
            }).
            when("/nyc-migration", {
                templateUrl: "partials/device-nyc-migration.html",
                controller: "deviceprotectionCtrl"
            }).
            when("/non-nyc-migration", {
                templateUrl: "partials/device-non-nyc-migration.html",
                controller: "deviceprotectionCtrl"
            }).
            when("/non_nyc_others", {
                templateUrl: "partials/device-protection-others.html",
                controller: "deviceprotectionCtrl"
            })

    });

    angular.module("uib/template/modal/window.html", []).run(["$templateCache", function ($templateCache) {
        $templateCache.put("uib/template/modal/window.html",
            "  <button type=\"button\" class=\"close pull-right uib-close\" ng-click=\"close($event)\"><i aria-hidden=\"true\" class=\"ico-closeIcon uib-close\" ng-click=\"close($event)\"></i></button><div class=\"modal-dialog {{size ? 'modal-' + size : ''}}\"><div class=\"modal-content\" uib-modal-transclude></div></div>\n" +
            "");
    }]);

    app.controller('generalController', ['$scope', function ($scope) {

    }])

        .controller('AccordionDemoCtrl', ['$scope', function ($scope) {
            $scope.oneAtATime = true;

            $scope.groups = [{
                title: 'Dynamic Group Header - 1',
                content: 'Dynamic Group Body - 1'
            }, {
                title: 'Dynamic Group Header - 2',
                content: 'Dynamic Group Body - 2'
            }];

            $scope.items = ['Item 1', 'Item 2', 'Item 3'];

            $scope.addItem = function () {
                var newItemNo = $scope.items.length + 1;
                $scope.items.push('Item ' + newItemNo);
            };

            $scope.status = {
                isCustomHeaderOpen: false,
                isFirstOpen: true,
                isFirstDisabled: false
            };
        }])

        .controller('AlertDemoCtrl', ['$scope', function ($scope) {
            $scope.alerts = [{
                type: 'danger',
                msg: 'Oh snap! Change a few things up and try submitting again.'
            },
            {
                type: 'success',
                msg: 'Well done! You successfully read this important alert message.'
            }
            ];

            $scope.addAlert = function () {
                $scope.alerts.push({
                    msg: 'Another alert!'
                });
            };

            $scope.closeAlert = function (index) {
                $scope.alerts.splice(index, 1);
            };
        }])

        .controller('ButtonsCtrl', ['$scope', function ($scope) {
            $scope.singleModel = 1;

            $scope.radioModel = 'Middle';

            $scope.checkModel = {
                left: false,
                middle: true,
                right: false
            };

            $scope.checkResults = [];

            $scope.$watchCollection('checkModel', function () {
                $scope.checkResults = [];
                angular.forEach($scope.checkModel, function (value, key) {
                    if (value) {
                        $scope.checkResults.push(key);
                    }
                });
            });
        }])

        .controller('CarouselDemoCtrl', ['$scope', function ($scope) {
            $scope.myInterval = 5000;
            $scope.noWrapSlides = false;
            $scope.active = 0;
            var slides = $scope.slides = [];
            var currIndex = 0;

            $scope.addSlide = function () {
                slides.push({
                    image: 'http://lorempixel.com/580/300',
                    text: ['Nice image', 'Awesome photograph', 'That is so cool', 'I love that'][slides.length % 4],
                    id: currIndex++
                });
            };

            for (var i = 0; i < 4; i++) {
                $scope.addSlide();
            }

            $scope.next = function () {
                alert("next");
            }

        }])


        .controller('DropdownCtrl', ['$scope', '$log', function ($scope, $log) {
            $scope.status = {
                isopen: false
            };

            $scope.toggled = function (open) {
                $log.log('Dropdown is now: ', open);
            };

            $scope.toggleDropdown = function ($event) {
                $event.preventDefault();
                $event.stopPropagation();
                $scope.status.isopen = !$scope.status.isopen;
            };

            $scope.appendToEl = angular.element(document.querySelector('#dropdown-long-content'));
        }])


        .controller('ModalDemoCtrl', ['$scope', '$uibModal', '$log', function ($scope, $uibModal, $log) {


            $scope.close = function () {
                $scope.visible = false;
            }
            $scope.removeGlue = function () {
                let body = document.getElementsByTagName('body')[0];
                body.classList.remove("glueModal");
            };

            $scope.addGlue = function () {
                let body = document.getElementsByTagName('body')[0];
                body.classList.add("glueModal");
            };


            $scope.openOneButton = function () {

                var modalInstance = $uibModal.open({
                    //animation: $scope.animationsEnabled,
                    templateUrl: 'myOneModalContent.html',
                    controller: 'ModalInstanceCtrl',
                    resolve: {
                        items: function () {
                            return $scope.items;
                        }
                    }
                });

                modalInstance.result.then(function (selectedItem) {
                    $scope.selected = selectedItem;
                }, function () {
                    $scope.removeGlue();
                    $log.info('Modal dismissed at: ' + new Date());
                });
                $scope.addGlue();
            };

            $scope.openTwoButton = function () {

                var modalInstance = $uibModal.open({
                    animation: $scope.animationsEnabled,
                    templateUrl: 'myTwoModalContent.html',
                    controller: 'ModalInstanceCtrl',
                    resolve: {
                        items: function () {
                            return $scope.items;
                        }
                    }
                });

                modalInstance.result.then(function (selectedItem) {
                    $scope.selected = selectedItem;
                }, function () {
                    $log.info('Modal dismissed at: ' + new Date());
                });
            };


        }])

        .controller('ModalInstanceCtrl', ['$scope', '$uibModalInstance', function ($scope, $uibModalInstance) {

            $scope.ok = function () {
                $uibModalInstance.close();
            };

            $scope.cancel = function () {
                $uibModalInstance.dismiss('cancel');
            };
        }])

        .controller('addressCtrl', ['$scope', function ($scope) {

        }])
        .controller('cardInformation', ['$scope', function ($scope) {
            $scope.nameoncard = 'Jane Doe';
            $scope.month = "01";
            $scope.year = "01";
            $scope.cardNumber = "1111-1111-1111-1111";
            $scope.cvv = "111";
            $scope.zip = "1111";
            $scope.nickname = "Roomate Card";
        }])
        .controller('bankinformation', ['$scope', function ($scope) {
            $scope.nameonaccount = 'Jane Doe';
            $scope.routingnumber = '11111111';
            $scope.accountnumber = '11111111111111111';
            $scope.nickname = 'Wife’s Account';
        }])
        .controller('tradeInCtrl', ['$scope', '$http', '$window', '$location', '$timeout', function ($scope, $http, $window, $location, $timeout) {
            $scope.tradeIn = false;
            $scope.lineSelector = false;
            $scope.lineSelectorDetails = true;
            var jsonPath = "json/tradeIn.json";
            $scope.showDetailsMobile = true;
            $scope.lineSelectorDetailsDesktop = true;
            $scope.tradeTitle = 'How trade-in works';
            $scope.tradeArray = [{
                'valid': true,
                'content': 'Curabitur blandit tempus porttitor. Morbi leo risus, porta ac consectetur ac, vestibulum at eros. Vivamus sagittis lacus vel augue laoreet rutrum faucibus dolor auctor. Aenean lacinia bibendum nulla sed consectetur.'
            },
            {
                'valid': true,
                'content': 'Etiam porta sem malesuada magna mollis euismod. Cras mattis consectetur purus sit amet fermentum. Aenean lacinia bibendum nulla sed consectetur. Cras mattis consectetur purus sit amet fermentum.'
            }
            ];

            $scope.tradeInUrls = [
                'partials/trade-in-template-bullet.html',
                'partials/trade-in-template-JUMP-promo-eligible.html',
                'partials/trade-in-template-JUMP.html',
                'partials/trade-in-template.html'
            ];

            $scope.tradeInUrlVariable = 3; // change the value 0/1/2/3 to view diffrent template 

            $scope.viewChange = function () {
                if ($window.innerWidth >= 768) {
                    $scope.isDesktop = true;
                } else {
                    $scope.isDesktop = false;
                }
            }
            $window.onload = function () {
                $window.scrollTo(0, 0);
                $scope.viewChange();
                if ($location.$$url == '/line-selector-trade-in') {
                    $window.location.href = '#/';
                }
            }
            angular.element($window).bind('resize', function () {
                if ($window.innerWidth >= 768) {
                    $scope.$apply(function () {
                        $scope.isDesktop = true;
                    });
                } else {
                    $scope.$apply(function () {
                        $scope.isDesktop = false;
                    });
                }
            });
            $scope.viewChange();
            $http.get(jsonPath).then(function (response) {
                $scope.title = response.data.title;
                $scope.details = response.data.details;
            });
            $scope.tradeModal = function () {
                document.body.classList.add("noscroll");
                $scope.visible = true;
                $scope.visibleAnimation = true;
            }
            $scope.close = function () {
                document.body.classList.remove("noscroll");
                $scope.visible = false;
                $scope.visibleAnimation = false;
            }
            $scope.hidden = true;

            // Ankit Jain 0628 - Making changes to animate fade in intead of right to left.
            $scope.animate = function (index) {
                $scope.hidden = false;
                $timeout(function () {
                    $window.scrollTo(0, 0);
                    $location.path('/line-selector-trade-in');
                    $scope.viewChange();
                    $scope.lineSelectorDetails = false;
                    $scope.lineSelectorDetailsDesktop = true;
                    for (var i = 0; i < $scope.details.length; i++) {
                        if (('object-' + i) != ('object-' + index)) {

                            if ($scope.lineSelectorDetailsDesktop) {
                                document.getElementById('object-' + i).style.display = 'none';
                                document.getElementById('lineSelector-title').style.display = 'none';
                            } else {
                                document.getElementById('object-' + i).style.display = 'block';
                            }
                        } else {
                            if ($window.innerWidth >= 768) {
                                document.getElementById('object-' + i).style.animation = 'mymove_dektop 500ms';
                                document.getElementById('lineSelectorDetails-text').style.display = 'block';
                                document.getElementById('lineSelectorDetails-text').style.animation = 'mymove_dektop 500ms';
                            } else {
                                document.getElementById('object-' + i).style.animation = 'mymove_mobile 500ms';
                                document.getElementById('lineSelectorDetails-text').style.display = 'block';
                                document.getElementById('lineSelectorDetails-text').style.animation = 'mymove_mobile 500ms';
                            }
                            document.getElementById('object-' + i).style.transitionTimingFunction = 'linear';
                        }
                    }
                }, 500);

            }
            // Ankit Jain 0628 - Making changes to animate fade in intead of right to left.
            $scope.repeatAnimationToGoBack = function () {
                $location.path('/line-selector-trade-in');
                $scope.viewChange();
                $scope.lineSelectorDetails = true;
                $scope.lineSelectorDetailsDesktop = false;
                for (var i = 0; i < $scope.details.length; i++) {
                    document.getElementById('object-' + i).style.display = 'block';
                    document.getElementById('lineSelector-title').style.display = 'block';
                    document.getElementById('lineSelectorDetails-text').style.display = 'none';
                    document.getElementById('object-' + i).style.animation = 'repeatMyMove_desktop 500ms';
                    document.getElementById('lineSelector-title').style.animation = 'repeatMyMove_desktop 500ms';
                }
            }
            $scope.goBack = function () {
                $window.scrollTo(0, 0);
                $scope.hidden = true;
                if ($location.$$url == "/line-selector-trade-in") {
                    $scope.repeatAnimationToGoBack();
                    $location.path('');
                }
            }
        }])
        .controller('tradeInConditionCtrl', ['$scope', '$http', '$window', '$location', '$timeout', function ($scope, $http, $window, $location, $timeout) {
            $scope.tradeCondtn = false;
            $scope.tradeValue = true;
            $scope.hidden = true;
            $scope.phonecondtion_fullImage = "./img/phone-condition-desktop.png";
            $scope.phonecondtion1_fullImage = "./img/phone-condition1-desktop.png";
            var jsonPathforTradeIn = "json/tradeIn2.json";
            $scope.goToTradeVal = function () {
                $timeout(function () {
                    // $window.scrollTo(0, 0);
                    $scope.tradeCondtn = true;
                    $scope.tradeValue = false;
                    $location.path('/trade-value');
                    // Ankit jain - 0628 - Fade in effect to move from one screen to another. 
                    document.getElementById('tradeValue').style.animation = 'fadeInEffect 2s';
                    document.getElementById('tradeValue').style.display = 'block';
                    document.getElementById('tradeCondition').style.display = 'none';
                }, 300);
            }

            $scope.phoneModal = function () {
                document.body.classList.add("noscroll");
                $scope.visible = true;
                $scope.visibleAnimation = true;
            }
            $scope.close = function () {
                document.body.classList.remove("noscroll");
                //                $scope.visible = false;
                $scope.visibleAnimation = false;
            }
            $scope.phoneTitle = 'Phone condition';
            $scope.phoneArray = [{
                'quesn': 'Curabitur blandit tempus porttitor. Morbi leo risus, porta ac consectetur ac?',
                'answer': 'Vestibulum at eros. Vivamus sagittis lacus vel augue laoreet rutrum faucibus dolor auctor. Aenean lacinia bibendum nulla sed consectetur.'
            },
            {
                'quesn': 'Etiam porta sem malesuada magna mollis euismod? ',
                'answer': 'Cras mattis consectetur purus sit amet fermentum. Aenean lacinia bibendum nulla sed consectetur. Cras mattis consectetur purus sit amet fermentum.'
            },
            {
                'quesn': 'Maecenas faucibus mollis interdum?',
                'answer': 'Fusce dapibus, tellus ac cursus commodo, tortor mauris condimentum nibh, ut fermentum massa justo sit amet risus. Nulla vitae elit libero, a pharetra augue. Integer posuere erat a ante venenatis dapibus posuere velit aliquet.'
            }
            ];

            $scope.phoneConditionURLs = [
                'partials/phone-condition-template-bad-condition.html',
                'partials/phone-condition-template-bullet.html',
                'partials/phone-condition-template.html'
            ];

            $scope.phoneConditionVariable = 0; // change the value 0/1/2 to view diffrent template 

            $http.get(jsonPathforTradeIn).then(function (response) {
                $scope.tradeinDetails = response.data.details;

            });
            $scope.goBack = function () {
                $window.scrollTo(0, 0);
                $scope.hidden = true;
                if ($location.$$url == "/trade-value") {
                    $location.path('');
                    $scope.tradeValue = true;
                    $scope.tradeCondtn = false;
                    // Ankit jain - 0628 - Fade in effect to move from one screen to another. 
                    document.getElementById('tradeCondition').style.display = 'block';
                    document.getElementById('tradeCondition').style.animation = 'fadeInEffect 2s';
                    document.getElementById('tradeValue').style.animation = 'fadeInEffect 2s';
                    document.getElementById('tradeValue').style.display = 'none';
                }
            }
        }])

        .controller('deviceprotectionCtrl', ['$scope', '$http', '$window', '$location', '$timeout', function ($scope, $http, $window, $location, $timeout) {


            $scope.pdp_nonnyc_others_lists = [{
                'protection_bestValue': 'BEST VALUE!',
                'protection_id': 'Protection 360',
                'protection_price': '$14.00/mo',
                'protection_description': [
                    "Replace phone when lost or stolen", "Extended warranty",
                    "Free upgrade after 50% balance paid", "Apple Care, ID protection, McAfee Security and Premium tech support "
                ]
            },
            {
                'protection_bestValue': '',
                'protection_id': 'Basic',
                'protection_price': '$13.00/mo',
                'protection_description': [
                    "Replace phone when lost or stolen", "Extended warranty"

                ]
            },
            {
                'protection_bestValue': '',
                'protection_id': "I don't need protection for my phone",

            }

            ];
            $scope.phonecondtion_fullImage = "./img/phone-condition-desktop.png";

            $scope.pdp_nyc_plans_lists = [{
                'protection_bestValue': 'BEST VALUE!',
                'protection_id': 'Protection 360',
                'protection_price': '$14.00/mo',
                'protection_description': [
                    "Replace phone when lost or stolen",
                    "Extended warranty",
                    "Free upgrade after 50% balance paid", "Apple Care, ID protection, McAfee Security and Premium tech support "
                ]
            },
            {
                'protection_bestValue': '',
                'protection_id': 'Extended warranty',
                'protection_price': '$6.75/mo.',
                'protection_description': [
                    "Maecenas faucibus mollis interdum. Curabitur blandit tempus porttitor."
                ]
            },

            {
                'protection_bestValue': '',
                'protection_id': 'Insurance only',
                'protection_price': '$4.00/mo.',
                'protection_description': [
                    "Maecenas faucibus mollis interdum. Curabitur blandit tempus porttitor."
                ]
            },
            {
                'protection_bestValue': '',
                'protection_id': 'Extended warranty and insurance',
                'protection_price': '$8.50/mo.',
                'protection_description': [
                    "Maecenas faucibus mollis interdum. Curabitur blandit tempus porttitor."
                ]
            },
            {
                'protection_bestValue': '',
                'protection_id': "I don't need protection for my phone",

            }

            ];

            $scope.deviceProtectionURLs = [
                '#non-nyc-plans',
                '#non-nyc-migration',
                '#nyc-migration',
                '#non_nyc_others',
            ];
            $scope.deviceProtectionUrlVariable = 2; // change the value 0/1/2/3 to view diffrent template 


            $scope.navigateProtectionplans = function () {
                return $scope.deviceProtectionURLs[$scope.deviceProtectionUrlVariable];
            }

            $scope.goBack = function () {
                $window.scrollTo(0, 0);
                $scope.hidden = true;
                $location.path('');

            }
            $scope.manymoreModal = function () {
                document.body.classList.add("noscroll");
                $scope.visible = true;
                $scope.visibleAnimation = true;
            }
            $scope.close = function () {
                document.body.classList.remove("noscroll");
                $scope.visible = false;
                $scope.visibleAnimation = false;
            }


            $scope.safeTitle = 'Protection 360';
            $scope.safeArray = [{
                'contentTitle': '',
                'content': 'Vestibulum at eros. Vivamus sagittis lacus vel augue laoreet rutrum faucibus dolor auctor. Aenean lacinia bibendum nulla sed consectetur.'
            },
            {
                'contentTitle': 'Etiam porta sem malesuada magna mollis euismod',
                'content': 'Cras mattis consectetur purus sit amet fermentum. Aenean lacinia bibendum nulla sed consectetur. Cras mattis consectetur purus sit amet fermentum.'
            },
            {
                'contentTitle': 'Maecenas faucibus mollis interdum',
                'content': 'Fusce dapibus, tellus ac cursus commodo, tortor mauris condimentum nibh, ut fermentum massa justo sit amet risus. Nulla vitae elit libero, a pharetra augue. Integer posuere erat a ante venenatis dapibus posuere velit aliquet.'
            },
            {
                'contentTitle': 'Maecenas faucibus mollis interdum',
                'content': 'Fusce dapibus, tellus ac cursus commodo, tortor mauris condimentum nibh, ut fermentum massa justo sit amet risus. Nulla vitae elit libero, a pharetra augue. Integer posuere erat a ante'
            }
            ];


        }])

        .controller('deviceIntentCtrl', ['$scope', '$window', function ($scope, $window) {
            window.scroll(0, 0);
            $scope.visible = true;
            var alertHeight = document.getElementById("notificationsAlert").offsetHeight;
            var header = document.getElementById("notificationsAlert");
            document.getElementById("mainContentBody").style.paddingTop = alertHeight + "px";
            $scope.close = function () {
                $scope.visible = false;
                $scope.myFunction();
                document.getElementById("mainContentBody").style.marginTop = (-alertHeight) + "px";
                return false;
            }

            $scope.myFunction = function () {
                if ($scope.visible) {
                    var topHeaderHt = document.getElementById("topHeader").offsetHeight;
                    if (window.pageYOffset <= topHeaderHt) {
                        document.getElementById("notificationsAlert").style.position = "relative";
                       

                    } else {
                        document.getElementById("notificationsAlert").style.position = "fixed";
                        document.getElementById("notificationsAlert").style.top = "0px";
                    }

                }
                else {
                    document.getElementById("notificationsAlert").style.position = "fixed";
                    document.getElementById("notificationsAlert").style.top = "0px";
                }

            }
            angular.element($window).bind('resize', function () {
                alertHeight = document.getElementById("notificationsAlert").offsetHeight;

                if ($scope.visible == false) {
                    document.getElementById("mainContentBody").style.paddingTop = (-alertHeight) + "px";
                }
                else {
                    document.getElementById("mainContentBody").style.paddingTop = alertHeight + "px";
                }
                $scope.$digest();
            });
            window.onscroll = function () {
                $scope.myFunction()
            };
            $scope.deviceIntent = [{
                'imgUrl': './img/new phone@2x.png',
                'title': 'Buy a new phone',
                'desc':''
            },
            {
                'imgUrl': './img/BYOD@2x.png',
                'title': 'Use my own phone',
                'desc':'Some phones may not be compatible.'
            },
            {
                'imgUrl': './img/other devices@2x.png',
                'title': 'Watches, tablets & more',
                'desc':''
            }
            ];
            
            $scope.hidden = true;
			
			$scope.manymoreModal = function () {
                document.body.classList.add("noscroll");
                $scope.visibleLCModal = true;
                $scope.visibleAnimation = true;
            }
            $scope.closeModal = function () {
                document.body.classList.remove("noscroll");
                $scope.visibleLCModal = false;
                $scope.visibleAnimation = false;
            }
			
			$scope.monthlyList = [{'monthly': 'DUE MONTHLY', 'totalAmtMonthly': '20', 'content1': 'Additional voice line on your','planName':' T-Mobile ONE plan.', 'content2': 'While using AutoPay.', 'content3': 'Taxes and fees included.', 'planDetails': ''}];
            $scope.plan = 'Here’s the cost to add a new line:';
            $scope.condition = 'Price doesn’t include phone.';
            $scope.terms = 'See full plan terms';
            
            /*Line cost*/
			
            // comment/uncomment variables for various pages
            
					// for LineCost_dep and MSG_dep use below variable
					/* $scope.todayList = [{'today': 'DUE TODAY', 'totalAmtToday': '75', 'deposit': '$50.00 deposit', 'kitAmt': '$25.00', 'kit': 'SIM starter kit ', 'details': 'Details'}]; */
					
					// for LineCost_dep+promo and MSG_dep+promo use below variable
					 /*$scope.todayList = [{'today': 'DUE TODAY', 'totalAmtToday': '75', 'deposit': '$50.00 deposit', 'kitAmt': '$25.00', 'kit': 'SIM starter kit ', 'discount': '$25.00 off code IMTEST', 'details': 'Details'}]; */
					
					// for LineCost_Reg and MSG_Reg use below variable
					/* $scope.todayList = [{'today': 'DUE TODAY', 'totalAmtToday': '25', 'kit': 'SIM starter kit'}]; */
					
					// for LineCost_dep+InstDisc MSG_dep+InstDisc use below variable
					  /*$scope.todayList = [{'today': 'DUE TODAY', 'totalAmtToday': '50', 'deposit': '$50.00 deposit', 'strikeAmt': '$25.00', 'kitAmt': '$0.00', 'kit': 'SIM starter kit ', 'discount': '$25.00 instant discount', 'details': 'Details'}]; */
					
					// for LineCost+InstDisc and MSG_Inst Disc use below variable
                    /*$scope.todayList = [{'today': 'DUE TODAY', 'totalAmtToday': '0', 'strikeTodayDue': '$25.00', 'kit': 'SIM starter kit ', 'discount': '$25.00 instant discount'}];*/

                    // for LineCost+InstDisc and MSG_IMTEST Disc use below variable
					$scope.todayList = [{'today': 'DUE TODAY', 'totalAmtToday': '25', 'strikeTodayDue': '', 'kit': 'SIM starter kit ', 'discount': '$25.00 off code IMTEST'}];
                    
            
             // comment/uncomment variables for various modals
					
					// for ModalBYOD_DEP+SIM use following variable
					/*$scope.modalArray = [{
						'modalTitle': 'Deposit',
						'contentTitle': 'Why does my order require a deposit?',
						'content': 'Based on your credit information, we require a deposit to activate your account with a monthly plan. Unless otherwise required by law, you may request a refund of a deposit after 12 months (with simple interest at the rate required by law) if your account has remained in good standing by calling Customer Care at 1-877-453-1305.'
					},
					{
						'modalTitle': '3-in-1 SIM starter kit',
						'contentTitle': '',
						'content': 'The 3-in-1 SIM Starter Kit includes a Nano SIM card with Micro and Standard SIM adapters. For use with unlocked, compatible GSM phones. No SIM card activation required. Once you receive your SIM card in the mail, simply slip it into your phone and you’re ready to go.'
					}
					]; */
					
					// for ModalBYOD_SIM use following variable
					/*$scope.modalArray = [{
						'modalTitle': '3-in-1 SIM starter kit',
						'contentTitle': '',
						'content': 'The 3-in-1 SIM Starter Kit includes a Nano SIM card with Micro and Standard SIM adapters. For use with unlocked, compatible GSM phones. No SIM card activation required. Once you receive your SIM card in the mail, simply slip it into your phone and you’re ready to go.'
					}
					]; */
					
					// for Modal_SIM use following variable
					/*$scope.modalArray = [{
						'modalTitle': 'SIM starter kit',
						'contentTitle': '',
						'content': 'SIM starter kit automatically added with your device purchase'
					}
					]; */
					
					// for Modal_dep+SIM use following variable
					$scope.modalArray = [{
						'modalTitle': 'Deposit',
						'contentTitle': 'Why does my order require a deposit?',
						'content': 'Based on your credit information, we require a deposit to activate your account with a monthly plan. Unless otherwise required by law, you may request a refund of a deposit after 12 months (with simple interest at the rate required by law) if your account has remained in good standing by calling Customer Care at 1-877-453-1305.'
					},
					{
						'modalTitle': 'SIM starter kit',
						'contentTitle': '',
						'content': 'SIM starter kit automatically added with your device purchase'
					}
					];
        }])
        .controller('modalCtrl', ['$scope', '$window', function ($scope, $window){
            $scope.hidden = true;
			
			$scope.manymoreModal = function () {
                document.body.classList.add("noscroll");
                $scope.visibleLCModal = true;
                $scope.visibleAnimation = true;
            }
            $scope.closeModal = function () {
                document.body.classList.remove("noscroll");
                $scope.visibleLCModal = false;
                $scope.visibleAnimation = false;
            }
            $scope.checkArray=[
                'Add $69 in accessories',
                'Choose monthly pricing',
                'Complete checkout'
            ]
        }])
})();