(function() {
    'use strict';
    var app = angular.module('app', ['ngAnimate', 'ngAria', 'ngAria', 'ngCookies', 'ngMessages', 'ngSanitize', 'ngTouch', 'ui.bootstrap']);
	
	angular.module("uib/template/modal/window.html", []).run(["$templateCache", function($templateCache) {
	  $templateCache.put("uib/template/modal/window.html",
		"  <button type=\"button\" class=\"close pull-right uib-close\" ng-click=\"close($event)\"><i aria-hidden=\"true\" class=\"ico-closeIcon uib-close\" ng-click=\"close($event)\"></i></button><div class=\"modal-dialog {{size ? 'modal-' + size : ''}}\"><div class=\"modal-content\" uib-modal-transclude></div></div>\n" +
		"");
	}]);
	
    app.controller('generalController', ['$scope', function($scope) {

    }])
    
	
	
    .controller('orderStatusCtrl', ['$scope', function($scope){

        
        $scope.hideOrderSummary = false;
        $scope.showOrderSummary=true;
        $scope.hideOrderWithTradeinSummary = false;
        $scope.showOrderWithTradeinSummary=true;
        $scope.hideTradeindeivceSummary = false;
        $scope.showTradeindeivceSummary=true;
            
        $scope.showOrderDetails = function () {
            $scope.hideOrderSummary = true;
            $scope.showOrderSummary=false;
        }
        $scope.hideOrderDetails = function () {
            $scope.hideOrderSummary = false;
            $scope.showOrderSummary=true;       
         }
         $scope.showOrderWithTradeinDetails = function () {
            $scope.hideOrderWithTradeinSummary = true;
            $scope.showOrderWithTradeinSummary=false;
        }
        $scope.hideOrderWithTradeinDetails = function () {
            $scope.hideOrderWithTradeinSummary = false;
            $scope.showOrderWithTradeinSummary=true;       
         }
         $scope.showTradeindeivceDetails = function () {
            $scope.hideTradeindeivceSummary = true;
            $scope.showTradeindeivceSummary=false;
        }
        $scope.hideTradeindeivceDetails = function () {
            $scope.hideTradeindeivceSummary = false;
            $scope.showTradeindeivceSummary=true;       
         }

    }])
})();
