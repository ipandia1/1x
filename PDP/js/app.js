(function() {
    'use strict';
    var app = angular.module('app', ['ngAnimate', 'ngAria', 'ngAria', 'ngCookies', 'ngMessages', 'ngSanitize', 'ngTouch', 'ui.bootstrap']);
	


    app.controller('generalController', ['$scope', function($scope) {

    }])

    .controller('AccordionDemoCtrl', ['$scope', function($scope) {
        $scope.oneAtATime = true;

        $scope.groups = [{
            title: 'Dynamic Group Header - 1',
            content: 'Dynamic Group Body - 1'
        }, {
            title: 'Dynamic Group Header - 2',
            content: 'Dynamic Group Body - 2'
        }];

        $scope.items = ['Item 1', 'Item 2', 'Item 3'];

        $scope.addItem = function() {
            var newItemNo = $scope.items.length + 1;
            $scope.items.push('Item ' + newItemNo);
        };

        $scope.status = {
            isCustomHeaderOpen: false,
            isFirstOpen: true,
            isFirstDisabled: false
        };
    }])


    .controller('AlertDemoCtrl', ['$scope', function($scope) {
        $scope.alerts = [
            { type: 'danger', msg: 'Oh snap! Change a few things up and try submitting again.' },
            { type: 'success', msg: 'Well done! You successfully read this important alert message.' }
        ];

        $scope.addAlert = function() {
            $scope.alerts.push({ msg: 'Another alert!' });
        };

        $scope.closeAlert = function(index) {
            $scope.alerts.splice(index, 1);
        };
    }])

    .controller('ButtonsCtrl', ['$scope', function($scope) {
        $scope.singleModel = 1;

        $scope.radioModel = 'Middle';

        $scope.checkModel = {
            left: false,
            middle: true,
            right: false
        };

        $scope.checkResults = [];

        $scope.$watchCollection('checkModel', function() {
            $scope.checkResults = [];
            angular.forEach($scope.checkModel, function(value, key) {
                if (value) {
                    $scope.checkResults.push(key);
                }
            });
        });
    }])

    .controller('CarouselDemoCtrl', ['$scope', function ($scope) {
        //$scope.myInterval = 5000;
        $scope.noWrapSlides = false;
        $scope.active = 0;
        var slides = $scope.slides = [];
        var currIndex = 0;
        $scope.addSlide = function () {
            slides.push({
                image: 'http://placehold.it/140x227',
                //text: ['Nice image', 'Awesome photograph', 'That is so cool'][slides.length % 3],
                id: currIndex++
            });
        };

        for (var i = 0; i < 3; i++) {
            $scope.addSlide();
        }

        $scope.next = function () {
            alert("next");
        }

    }])

       .controller('CarouselDemoCtrl1', ['$scope', function ($scope) {
        //$scope.myInterval = 5000;
        $scope.noWrapSlides = false;
        $scope.active = 0;
        var slides = $scope.slides = [];
        var currIndex = 0;
        $scope.addSlide = function () {
            slides.push({
                image: 'http://placehold.it/226x121',
                //text: ['Nice image', 'Awesome photograph', 'That is so cool'][slides.length % 3],
                id: currIndex++
            });
        };

        for (var i = 0; i < 3; i++) {
            $scope.addSlide();
        }

        $scope.next = function () {
            alert("next");
        }

    }])


    .controller('DropdownCtrl', ['$scope', '$log', function ($scope, $log) {
        $scope.status = {
            isopen: false
        };

        $scope.toggled = function(open) {
            $log.log('Dropdown is now: ', open);
        };

        $scope.toggleDropdown = function($event) {
            $event.preventDefault();
            $event.stopPropagation();
            $scope.status.isopen = !$scope.status.isopen;
        };

        $scope.appendToEl = angular.element(document.querySelector('#dropdown-long-content'));
    }])


    .controller('ModalDemoCtrl', ['$scope', '$uibModal', '$log', function($scope, $uibModal, $log) {

        $scope.openOneButton = function() {

            var modalInstance = $uibModal.open({
                animation: $scope.animationsEnabled,
                templateUrl: 'myOneModalContent.html',
                controller: 'ModalInstanceCtrl',
                resolve: {
                    items: function() {
                        return $scope.items;
                    }
                }
            });

            modalInstance.result.then(function(selectedItem) {
                $scope.selected = selectedItem;
            }, function() {
                $log.info('Modal dismissed at: ' + new Date());
            });
        };

        $scope.openTwoButton = function() {

            var modalInstance = $uibModal.open({
                animation: $scope.animationsEnabled,
                templateUrl: 'myTwoModalContent.html',
                controller: 'ModalInstanceCtrl',
                resolve: {
                    items: function() {
                        return $scope.items;
                    }
                }
            });

            modalInstance.result.then(function(selectedItem) {
                $scope.selected = selectedItem;
            }, function() {
                $log.info('Modal dismissed at: ' + new Date());
            });
        };


    }])

    .controller('ModalInstanceCtrl', ['$scope', '$uibModalInstance', function($scope, $uibModalInstance) {

        $scope.ok = function() {
            $uibModalInstance.close();
        };

        $scope.cancel = function() {
            $uibModalInstance.dismiss('cancel');
        };
    }])

    /*.controller('ProgressDemoCtrl', ['$scope', function($scope) {
        $scope.max = 200;

        $scope.random = function() {
            var value = Math.floor(Math.random() * 100 + 1);
            var type;

            if (value < 25) {
                type = 'success';
            } else if (value < 50) {
                type = 'info';
            } else if (value < 75) {
                type = 'warning';
            } else {
                type = 'danger';
            }

            $scope.showWarning = type === 'danger' || type === 'warning';

            $scope.dynamic = value;
            $scope.type = type;
        };

        $scope.random();

        $scope.randomStacked = function() {
            $scope.stacked = [];
            var types = ['success', 'info', 'warning', 'danger'];

            for (var i = 0, n = Math.floor(Math.random() * 4 + 1); i < n; i++) {
                var index = Math.floor(Math.random() * 4);
                $scope.stacked.push({
                    value: Math.floor(Math.random() * 30 + 1),
                    type: types[index]
                });
            }
        };

        $scope.randomStacked();
    }])*/

/*    .controller('TabsDemoCtrl', ['$scope', '$window', function($scope, $window) {
        $scope.tabs = [
            { title: 'Dynamic Title 1', content: 'Dynamic content 1' },
            { title: 'Dynamic Title 2', content: 'Dynamic content 2', disabled: true }
        ];

        $scope.model = {
            name: 'Tabs'
        };
    }])*/

/*    .controller('TooltipDemoCtrl', ['$scope', '$sce', function($scope, $sce) {
        $scope.dynamicTooltip = 'Hello, World!';
        $scope.dynamicTooltipText = 'dynamic';
        $scope.htmlTooltip = $sce.trustAsHtml('I\'ve been made <b>bold</b>!');
        $scope.placement = {
            options: [
                'top',
                'top-left',
                'top-right',
                'bottom',
                'bottom-left',
                'bottom-right',
                'left',
                'left-top',
                'left-bottom',
                'right',
                'right-top',
                'right-bottom'
            ],
            selected: 'top'
        };
    }])*/
	
	.controller('TabsDemoCtrl', ['$scope', '$window', function($scope, $window) {
        $scope.tabs = [
            { title: 'Dynamic Title 1', content: 'Dynamic content 1' },
            { title: 'Dynamic Title 2', content: 'Dynamic content 2', disabled: true }
        ];

        $scope.model = {
            name: 'Tabs'
        };
		/*Hyd SEZ*/
		//$scope.tab = 1;
        $scope.isSet1 = true;
        $scope.rating1 = {
            "color": "#e20074"
        }
        $scope.setTab = function(newTab){
            if (newTab == 1)
            {
                $scope.isSet1 = true;
                $scope.isSet2 = false;
                $scope.isSet3 = false;
                $scope.isSet4 = false;
                $scope.isSet5 = false;
                $scope.rating1 = {
                    "color": "#e20074"
                }
                $scope.rating2 = {
                    "color": "#bebebe"
                }
                $scope.rating3 = {
                    "color": "#bebebe"
                }
                $scope.rating4 = {
                    "color": "#bebebe"
                }
                $scope.rating5 = {
                    "color": "#bebebe"
                }

            }
            else if (newTab == 2) {
                $scope.isSet2 = true;
                $scope.isSet1 = false;
                $scope.isSet3 = false;
                $scope.isSet4 = false;
                $scope.isSet5 = false;
                $scope.rating2 = {
                    "color": "#e20074"
                }
                $scope.rating1 = {
                    "color": "#bebebe"
                }
                $scope.rating3 = {
                    "color": "#bebebe"
                }
                $scope.rating4 = {
                    "color": "#bebebe"
                }
                $scope.rating5 = {
                    "color": "#bebebe"
                }
            }
            else if (newTab == 3) {
                $scope.isSet3 = true;
                $scope.isSet2 = false;
                $scope.isSet1 = false;
                $scope.isSet4 = false;
                $scope.isSet5 = false;
                $scope.rating3 = {
                    "color": "#e20074"
                }
                $scope.rating2 = {
                    "color": "#bebebe"
                }
                $scope.rating1 = {
                    "color": "#bebebe"
                }
                $scope.rating4 = {
                    "color": "#bebebe"
                }
                $scope.rating5 = {
                    "color": "#bebebe"
                }
            }
            else if (newTab == 4) {
                $scope.isSet4 = true;
                $scope.isSet2 = false;
                $scope.isSet3 = false;
                $scope.isSet1 = false;
                $scope.isSet5 = false;
                $scope.rating4 = {
                    "color": "#e20074"
                }
                $scope.rating2 = {
                    "color": "#bebebe"
                }
                $scope.rating3 = {
                    "color": "#bebebe"
                }
                $scope.rating1 = {
                    "color": "#bebebe"
                }
                $scope.rating5 = {
                    "color": "#bebebe"
                }
            }
            else if (newTab == 5) {
                $scope.isSet5 = true;
                $scope.isSet2 = false;
                $scope.isSet3 = false;
                $scope.isSet4 = false;
                $scope.isSet1 = false;
                $scope.rating5 = {
                    "color": "#e20074"
                }
                $scope.rating2 = {
                    "color": "#bebebe"
                }
                $scope.rating3 = {
                    "color": "#bebebe"
                }
                $scope.rating4 = {
                    "color": "#bebebe"
                }
                $scope.rating1 = {
                    "color": "#bebebe"
                }
            }
        };

       
       
      //$scope.isSet = function(tabNum){
      //return $scope.tab === tabNum;
      //};

       $scope.maxSize = 5;
       $scope.bigTotalItems = 175;
	   /*Hyd SEZ ends here*/
	   
    }])
	
	/*copyed by koushik starts*/
	.controller('SeemoreCtrl1', ['$scope', function ($scope) {
		$scope.description="With 3D Touch, Live Photos, 7000 series aluminum, A9 chip, advanced cameras, 4.7-inch Retina HD display and so much more, you'll see how - with IPhone 6s - the only thing that's changed is everything.With 3D Touch, Live Photos, 7000 series aluminum, A9 chip, advanced cameras, 4.7-inch Retina HD display and so much more, you'll see how - with IPhone 6s - the only thing that's changed is everything";
	
		/*$scope.legal="With 3D Touch, Live Photos, 7000 series aluminum, A9 chip, advanced cameras, 4.7-inch Retina HD display and so much more, you'll see how - with IPhone 6s - the only thing that's changed is everything.With 3D Touch, Live Photos, 7000 series aluminum, A9 chip, advanced cameras, 4.7-inch Retina HD display and so much more, you'll see how - with IPhone 6s - the only thing that's changed is everything"*/
        
        $scope.slashed = true; // toggle between true and false for slashed and unslashed pages
	}]) 
	/*copyed by koushik ends*/
	/* Offshore Team */
	.controller('CheckListProsCtrl', function($scope) {
		$scope.pros = [
			'Great for texting', 
			'Great web browsing', 
			'Long Battery life', 
			'Easy of use',
			'Great camera', 
			'Useful apps', 
			'Fun games', 
			'WiFi',
			'GPS', 
			'4G LTE', 
			'Durable', 
			'Large screen',
			'Touch screen', 
			'Music', 
			'Keyboard', 
			'Speakerphone',
			'Volume level', 
			'Processor', 
			'Memory'
		];
		$scope.maxProTextBox = 10;
		$scope.proTextBoxArray = []; //to store data of textbox
		var ZERO = 0;
		//create new textbox(ng-repeat logic) when clicked on last text box and is empty
		$scope.insertProTextBox = function(index) {
			var noOfTextBoxes = $scope.proTextBoxArray.length;
			if(noOfTextBoxes <= $scope.maxProTextBox) {
				if (noOfTextBoxes == ZERO && $scope.proTextBoxArray[ZERO].length > ZERO) {
					$scope.proTextBoxArray.push('');
				}
				else if (index == (noOfTextBoxes) && $scope.proTextBoxArray[noOfTextBoxes].length > ZERO) {
					$scope.proTextBoxArray.push('');
				}
				
			}
		}
	})
	
	.controller('CheckListConsCtrl', function($scope) {
		$scope.cons = [
			'Expensive', 
			'Hard to use', 
			'Web browsing', 
			'Battery',
			'Camera', 
			'Screen', 
			'Touchscreen', 
			'Keyboard',
			'Heavy', 
			'Bulky', 
			'Speakerphone', 
			'Volume level',
			'Processor', 
			'Memory',  
			'Buggy'
		];
		$scope.maxConTextBox = 10;
		$scope.conTextBoxArray = [];//to store data of textbox
		var ZERO = 0;
		//create new textbox(ng-repeat logic) when clicked on last text box and is empty
		$scope.insertConTextBox = function(index) {
			var noOfTextBoxes = $scope.conTextBoxArray.length;
			if(noOfTextBoxes <= $scope.maxConTextBox) {
				if (noOfTextBoxes == ZERO && $scope.conTextBoxArray[ZERO].length > ZERO) {
					$scope.conTextBoxArray.push('');
				}
				else if (index == (noOfTextBoxes) && $scope.conTextBoxArray[noOfTextBoxes].length > ZERO) {
					$scope.conTextBoxArray.push('');
				}
			}
		}
	})
	
	.controller('myInfoReview', function($scope) {
	})
	 .controller('starCtrl', ['$scope',function ($scope) {
    $scope.rate = 0;
	$scope.EaseofUserate= 0;
	$scope.Batteryliferate=0;
	$scope.Featuresrate=0;
	$scope.CallQualityrate=0;
  $scope.max = 5;
  $scope.isReadonly = false;
  
  var ratings=[{
    "name": "poor",
    "id" : 1
    },
    {
    "name": "Fair",
    "id": 2
    },
    {
    "name": "Average",
    "id": 3
    }, {
    "name": "Good",
    "id": 4
    },
	{
    "name": "Excellent",
    "id": 5
    }];
    $scope.hoveringOver = function(value) {
    $scope.overStar = ratings[value-1].name;
    };
	$scope.setvalue=function(index)
	{
	$scope.rate=index;
	$scope.test= ratings[$scope.rate-1].name;
	}
	
	
	$scope.onEaseofUsehover = function(value) {
    $scope.EaseofUsemouseovervalue = ratings[value-1].name;
	};
	
	$scope.setEaseofUsevalue=function(index)
	{
	$scope.EaseofUserate=index;
	$scope.EaseofUseratedvalue= ratings[$scope.EaseofUserate-1].name;
	}
	$scope.onEaseofUsemouseout=function()
	{
	$scope.EaseofUsemouseovervalue='';
	 $scope.EaseofUseratedvalue= ratings[$scope.EaseofUserate-1 ].name;
	 
	};
	
	$scope.onBatterylifehover = function(value) {
    $scope.Batterylifemouseovervalue = ratings[value-1].name;
	};
	
	$scope.setBatterylifevalue=function(index)
	{
	$scope.Batteryliferate=index;
	$scope.Batteryliferatedvalue= ratings[$scope.Batteryliferate-1].name;
	}
	$scope.onBatterylifemouseout=function()
	{
	$scope.Batterylifemouseovervalue='';
	 $scope.Batteryliferatedvalue= ratings[$scope.Batteryliferate-1 ].name;
	 
	};
	
	$scope.onFeatureshover = function(value) {
    $scope.Featuresmouseovervalue = ratings[value-1].name;
	};
	
	$scope.setFeaturesvalue=function(index)
	{
	$scope.Featuresrate=index;
	$scope.Featuresratedvalue= ratings[$scope.Featuresrate-1].name;
	}
	$scope.onFeaturesmouseout=function()
	{
	$scope.Featuresmouseovervalue='';
	 $scope.Featuresratedvalue= ratings[$scope.Featuresrate-1 ].name;
	 
	};
	
	$scope.onCallQualityhover = function(value) {
    $scope.CallQualitymouseovervalue = ratings[value-1].name;
	};
	
	$scope.setCallQualityvalue=function(index)
	{
	$scope.CallQualityrate=index;
	$scope.CallQualityratedvalue= ratings[$scope.CallQualityrate-1].name;
	}
	$scope.onCallQualitymouseout=function()
	{
	$scope.CallQualitymouseovervalue='';
	 $scope.CallQualityratedvalue= ratings[$scope.CallQualityrate-1 ].name;
	 
	};
	

  $scope.ratingStates = [
    {stateOn: 'glyphicon-ok-sign', stateOff: 'glyphicon-ok-circle'},
    {stateOn: 'fa fa-star p-l-5-md p-l-5-lg fa-2x', stateOff: 'fa fa-star-o p-l-5-md p-l-5-lg fa-2x'},
    
    
  ];
	
    
     }])
	 .controller('progressiveCtrl', ['$scope',function ($scope) {
    $scope.isReadonly = false;
	$scope.changeOnHover = true;
    $scope.maxValue = 5 ; 
    $scope.star=2;
	$scope.ratingValue = 1;
	$scope.currentindex=1;
    
     }])
    angular.module('ui.bootstrap.carousel', [])

        .controller('UibCarouselController', ['$scope', '$element', '$interval', '$timeout', '$animate', function($scope, $element, $interval, $timeout, $animate) {
            var self = this,
                slides = self.slides = $scope.slides = [],
                SLIDE_DIRECTION = 'uib-slideDirection',
                currentIndex = $scope.active,
                currentInterval, isPlaying;

            var destroyed = false;
            $element.addClass('carousel');

            self.addSlide = function(slide, element) {
                slides.push({
                    slide: slide,
                    element: element
                });
                slides.sort(function(a, b) {
                    return +a.slide.index - +b.slide.index;
                });
                //if this is the first slide or the slide is set to active, select it
                if (slide.index === $scope.active || slides.length === 1 && !angular.isNumber($scope.active)) {
                    if ($scope.$currentTransition) {
                        $scope.$currentTransition = null;
                    }

                    currentIndex = slide.index;
                    $scope.active = slide.index;
                    setActive(currentIndex);
                    self.select(slides[findSlideIndex(slide)]);
                    if (slides.length === 1) {
                        $scope.play();
                    }
                }
            };

            self.getCurrentIndex = function() {
                for (var i = 0; i < slides.length; i++) {
                    if (slides[i].slide.index === currentIndex) {
                        return i;
                    }
                }
            };

            self.next = $scope.next = function() {
                var newIndex = (self.getCurrentIndex() + 1) % slides.length;

                if (newIndex === 0 && $scope.noWrap()) {
                    $scope.pause();
                    return;
                }

                return self.select(slides[newIndex], 'next');
            };

            self.prev = $scope.prev = function() {
                var newIndex = self.getCurrentIndex() - 1 < 0 ? slides.length - 1 : self.getCurrentIndex() - 1;

                if ($scope.noWrap() && newIndex === slides.length - 1) {
                    $scope.pause();
                    return;
                }

                return self.select(slides[newIndex], 'prev');
            };

            self.removeSlide = function(slide) {
                var index = findSlideIndex(slide);

                //get the index of the slide inside the carousel
                slides.splice(index, 1);
                if (slides.length > 0 && currentIndex === index) {
                    if (index >= slides.length) {
                        currentIndex = slides.length - 1;
                        $scope.active = currentIndex;
                        setActive(currentIndex);
                        self.select(slides[slides.length - 1]);
                    } else {
                        currentIndex = index;
                        $scope.active = currentIndex;
                        setActive(currentIndex);
                        self.select(slides[index]);
                    }
                } else if (currentIndex > index) {
                    currentIndex--;
                    $scope.active = currentIndex;
                }

                //clean the active value when no more slide
                if (slides.length === 0) {
                    currentIndex = null;
                    $scope.active = null;
                }
            };

            /* direction: "prev" or "next" */
            self.select = $scope.select = function(nextSlide, direction) {
                var nextIndex = findSlideIndex(nextSlide.slide);
                //Decide direction if it's not given
                if (direction === undefined) {
                    direction = nextIndex > self.getCurrentIndex() ? 'next' : 'prev';
                }
                //Prevent this user-triggered transition from occurring if there is already one in progress
                if (nextSlide.slide.index !== currentIndex &&
                    !$scope.$currentTransition) {
                    goNext(nextSlide.slide, nextIndex, direction);
                }
            };

            /* Allow outside people to call indexOf on slides array */
            $scope.indexOfSlide = function(slide) {
                return +slide.slide.index;
            };

            $scope.isActive = function(slide) {
                return $scope.active === slide.slide.index;
            };

            $scope.isPrevDisabled = function() {
                return $scope.active === 0 && $scope.noWrap();
            };

            $scope.isNextDisabled = function() {
                return $scope.active === slides.length - 1 && $scope.noWrap();
            };

            $scope.pause = function() {
                if (!$scope.noPause) {
                    isPlaying = false;
                    resetTimer();
                }
            };

            $scope.play = function() {
                if (!isPlaying) {
                    isPlaying = true;
                    restartTimer();
                }
            };

            $element.on('mouseenter', $scope.pause);
            $element.on('mouseleave', $scope.play);

            $scope.$on('$destroy', function() {
                destroyed = true;
                resetTimer();
            });

            $scope.$watch('noTransition', function(noTransition) {
                $animate.enabled($element, !noTransition);
            });

            $scope.$watch('interval', restartTimer);

            $scope.$watchCollection('slides', resetTransition);

            $scope.$watch('active', function(index) {
                if (angular.isNumber(index) && currentIndex !== index) {
                    for (var i = 0; i < slides.length; i++) {
                        if (slides[i].slide.index === index) {
                            index = i;
                            break;
                        }
                    }

                    var slide = slides[index];
                    if (slide) {
                        setActive(index);
                        self.select(slides[index]);
                        currentIndex = index;
                    }
                }
            });

            function getSlideByIndex(index) {
                for (var i = 0, l = slides.length; i < l; ++i) {
                    if (slides[i].index === index) {
                        return slides[i];
                    }
                }
            }

            function setActive(index) {
                for (var i = 0; i < slides.length; i++) {
                    slides[i].slide.active = i === index;
                }
            }

            function goNext(slide, index, direction) {
                if (destroyed) {
                    return;
                }

                angular.extend(slide, {direction: direction});
                angular.extend(slides[currentIndex].slide || {}, {direction: direction});
                if ($animate.enabled($element) && !$scope.$currentTransition &&
                    slides[index].element && self.slides.length > 1) {
                    slides[index].element.data(SLIDE_DIRECTION, slide.direction);
                    var currentIdx = self.getCurrentIndex();

                    if (angular.isNumber(currentIdx) && slides[currentIdx].element) {
                        slides[currentIdx].element.data(SLIDE_DIRECTION, slide.direction);
                    }

                    $scope.$currentTransition = true;
                    $animate.on('addClass', slides[index].element, function(element, phase) {
                        if (phase === 'close') {
                            $scope.$currentTransition = null;
                            $animate.off('addClass', element);
                        }
                    });
                }

                $scope.active = slide.index;
                currentIndex = slide.index;
                setActive(index);

                //every time you change slides, reset the timer
                restartTimer();
            }

            function findSlideIndex(slide) {
                for (var i = 0; i < slides.length; i++) {
                    if (slides[i].slide === slide) {
                        return i;
                    }
                }
            }

            function resetTimer() {
                if (currentInterval) {
                    $interval.cancel(currentInterval);
                    currentInterval = null;
                }
            }

            function resetTransition(slides) {
                if (!slides.length) {
                    $scope.$currentTransition = null;
                }
            }

            function restartTimer() {
                resetTimer();
                var interval = +$scope.interval;
                if (!isNaN(interval) && interval > 0) {
                    currentInterval = $interval(timerFn, interval);
                }
            }

            function timerFn() {
                var interval = +$scope.interval;
                if (isPlaying && !isNaN(interval) && interval > 0 && slides.length) {
                    $scope.next();
                } else {
                    $scope.pause();
                }
            }
        }])

        .directive('uibCarousel', function() {
            return {
                transclude: true,
                controller: 'UibCarouselController',
                controllerAs: 'carousel',
                restrict: 'A',
                templateUrl: function(element, attrs) {
                    return attrs.templateUrl || 'uib/template/carousel/carousel.html';
                },
                scope: {
                    active: '=',
                    interval: '=',
                    noTransition: '=',
                    noPause: '=',
                    noWrap: '&'
                }
            };
        })

        .directive('uibSlide', ['$animate', function($animate) {
            return {
                require: '^uibCarousel',
                restrict: 'A',
                transclude: true,
                templateUrl: function(element, attrs) {
                    return attrs.templateUrl || 'uib/template/carousel/slide.html';
                },
                scope: {
                    actual: '=?',
                    index: '=?'
                },
                link: function (scope, element, attrs, carouselCtrl) {
                    element.addClass('item');
                    carouselCtrl.addSlide(scope, element);
                    //when the scope is destroyed then remove the slide from the current slides array
                    scope.$on('$destroy', function() {
                        carouselCtrl.removeSlide(scope);
                    });

                    scope.$watch('active', function(active) {
                        $animate[active ? 'addClass' : 'removeClass'](element, 'active');
                    });
                }
            };
        }])

        .animation('.item', ['$animateCss',
            function($animateCss) {
                var SLIDE_DIRECTION = 'uib-slideDirection';

                function removeClass(element, className, callback) {
                    element.removeClass(className);
                    if (callback) {
                        callback();
                    }
                }

                return {
                    beforeAddClass: function(element, className, done) {
                        if (className === 'active') {
                            var stopped = false;
                            var direction = element.data(SLIDE_DIRECTION);
                            var directionClass = direction === 'next' ? 'left' : 'right';
                            var removeClassFn = removeClass.bind(this, element,
                                directionClass + ' ' + direction, done);
                            element.addClass(direction);

                            $animateCss(element, {addClass: directionClass})
                                .start()
                                .done(removeClassFn);

                            return function() {
                                stopped = true;
                            };
                        }
                        done();
                    },
                    beforeRemoveClass: function (element, className, done) {
                        if (className === 'active') {
                            var stopped = false;
                            var direction = element.data(SLIDE_DIRECTION);
                            var directionClass = direction === 'next' ? 'left' : 'right';
                            var removeClassFn = removeClass.bind(this, element, directionClass, done);

                            $animateCss(element, {addClass: directionClass})
                                .start()
                                .done(removeClassFn);

                            return function() {
                                stopped = true;
                            };
                        }
                        done();
                    }
                };
            }]);
	/*Offshore Team */
})();
