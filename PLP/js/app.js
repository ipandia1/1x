(function() {
    'use strict';
    var app = angular.module('app', ['ngAnimate', 'ngAria', 'ngAria', 'ngCookies', 'ngMessages', 'ngSanitize', 'ngTouch', 'ui.bootstrap']);

	angular.module("uib/template/modal/window.html", []).run(["$templateCache", function($templateCache) {
	  $templateCache.put("uib/template/modal/window.html",
		"  <button type=\"button\" class=\"close pull-right uib-close\" ng-click=\"close($event)\"><i aria-hidden=\"true\" class=\"ico-closeIcon uib-close\" ng-click=\"close($event)\"></i></button><div class=\"modal-dialog {{size ? 'modal-' + size : ''}}\"><div class=\"modal-content\" uib-modal-transclude></div></div>\n" +
		"");
	}]);
	
    app.controller('generalController', ['$scope', function($scope) {
                $scope.sortValues="Sort By";
                $scope.sortBy=function(values){
                if(values=="Featured")
                {
                $scope.sortValues="Featured";
                }
                else if(values=="Price low to high")
                {
                $scope.sortValues="Price low to high";
                }
                else if(values=="Price high to low")
                {
                $scope.sortValues="Price high to low";
                }
                else if(values=="Best seller")
                {
                $scope.sortValues="Best seller";
                }
                else if(values=="Highest rating")
                {
                $scope.sortValues="Highest rating";
                }
                $scope.isCollapsed=false;
     }
    }])


    .controller('AccordionDemoCtrl', ['$scope', function($scope) {
        $scope.oneAtATime = true;

        $scope.groups = [{
            title: 'Dynamic Group Header - 1',
            content: 'Dynamic Group Body - 1'
        }, {
            title: 'Dynamic Group Header - 2',
            content: 'Dynamic Group Body - 2'
        }];

        $scope.items = ['Item 1', 'Item 2', 'Item 3'];

        $scope.addItem = function() {
            var newItemNo = $scope.items.length + 1;
            $scope.items.push('Item ' + newItemNo);
        };

        $scope.status = {
            isCustomHeaderOpen: false,
            isFirstOpen: true,
            isFirstDisabled: false
        };
            $scope.colorArray = [
                {
                    id: 0,
                    colors: ["#909097", "#deb4ad", "#d6bda5", "#cdcecf", "#efaf71",
                        "#72ab8b", "#aa8c74", "#000000", "#dddddd", "#ee2c29"]
                },
                {
                    id: 1,
                    colors: ["#909097", "#deb4ad", "#d6bda5", "#cdcecf"]
                },
                {
                    id: 2,
                    colors: [ "#efaf71",  "#000000", "#dddddd"]
                },
                {
                    id: 3,
                    colors: [ "#d6bda5", "#cdcecf"]
                },
                {
                    id: 4,
                    colors: ["#909097", "#deb4ad", "#d6bda5", "#cdcecf", "#efaf71",
                        "#72ab8b"]
                },
                {
                    id: 5,
                    colors: [ "#d6bda5"]
                },
                {
                    id: 6,
                    colors: ["#909097", "#deb4ad", "#d6bda5", "#cdcecf", "#efaf71"]
                },
                {
                    id: 7,
                    colors: ["#909097", "#deb4ad", "#d6bda5", "#cdcecf", "#efaf71",
                        "#72ab8b", "#aa8c74", "#000000", "#dddddd", "#ee2c29"]
                },
                {
                    id: 8,
                    colors: ["#909097", "#deb4ad", "#72ab8b", "#aa8c74", "#000000", "#dddddd", "#ee2c29"]
                },
                {
                    id: 9,
                    colors: ["#909097", "#deb4ad", "#d6bda5", "#cdcecf", "#efaf71",
                        "#72ab8b", "#aa8c74", "#000000", "#dddddd", "#ee2c29"]
                },
                {
                    id: 10,
                    colors: ["#909097", "#deb4ad", "#d6bda5", "#cdcecf", "#efaf71",
                        "#72ab8b", "#aa8c74", "#000000", "#dddddd", "#ee2c29"]
                },
                {
                    id: 11,
                    colors: ["#909097", "#deb4ad", "#d6bda5", "#cdcecf", "#efaf71",
                        "#72ab8b", "#aa8c74", "#000000", "#dddddd", "#ee2c29"]
                },
                {
                    id: 12,
                    colors: ["#909097", "#deb4ad", "#d6bda5", "#cdcecf", "#efaf71",
                        "#72ab8b", "#aa8c74", "#000000", "#dddddd", "#ee2c29"]
                },
                {
                    id: 13,
                    colors: ["#909097", "#deb4ad", "#d6bda5", "#cdcecf", "#efaf71",
                        "#72ab8b", "#aa8c74", "#000000", "#dddddd", "#ee2c29"]
                },

            ];

            $scope.getColor = function (id, val) {
                if ($scope.colorArray[id].colors.length <= 4) {
                    $scope.center = true;
                } else {
                    $scope.center = false;
                }
                return $scope.colorArray[id].colors[val];
            };
    }])

    .controller('AlertDemoCtrl', ['$scope', function($scope) {
        $scope.alerts = [
            { type: 'danger', msg: 'Oh snap! Change a few things up and try submitting again.' },
            { type: 'success', msg: 'Well done! You successfully read this important alert message.' }
        ];

        $scope.addAlert = function() {
            $scope.alerts.push({ msg: 'Another alert!' });
        };

        $scope.closeAlert = function(index) {
            $scope.alerts.splice(index, 1);
        };
    }])

    .controller('ButtonsCtrl', ['$scope', function($scope) {
        $scope.singleModel = 1;

        $scope.radioModel = 'Middle';

        $scope.checkModel = {
            left: false,
            middle: true,
            right: false
        };

        $scope.checkResults = [];

        $scope.$watchCollection('checkModel', function() {
            $scope.checkResults = [];
            angular.forEach($scope.checkModel, function(value, key) {
                if (value) {
                    $scope.checkResults.push(key);
                }
            });
        });
    }])

    .controller('CarouselDemoCtrl', ['$scope', function($scope) {
        $scope.myInterval = 5000;
        $scope.noWrapSlides = false;
        $scope.active = 0;
        var slides = $scope.slides = [];
        var currIndex = 0;

        $scope.addSlide = function() {
            slides.push({
                image: 'http://lorempixel.com/85/100',
                text: ['Nice image', 'Awesome photograph', 'That is so cool'][slides.length % 3],
                id: currIndex++
            });
        };

        for (var i = 0; i < 3; i++) {
            $scope.addSlide();
        }

        $scope.next = function(){
            alert("next");
        }

    }])


    .controller('DropdownCtrl', ['$scope', '$log', function ($scope, $log) {
        $scope.status = {
            isopen: false
        };

        $scope.toggled = function(open) {
            $log.log('Dropdown is now: ', open);
        };

        $scope.toggleDropdown = function($event) {
            $event.preventDefault();
            $event.stopPropagation();
            $scope.status.isopen = !$scope.status.isopen;
        };

        $scope.appendToEl = angular.element(document.querySelector('#dropdown-long-content'));
    }])


    .controller('ModalDemoCtrl', ['$scope', '$uibModal', '$log', function($scope, $uibModal, $log) {

        $scope.openOneButton = function() {

            var modalInstance = $uibModal.open({
                animation: $scope.animationsEnabled,
                templateUrl: 'myOneModalContent.html',
                controller: 'ModalInstanceCtrl',
                resolve: {
                    items: function() {
                        return $scope.items;
                    }
                }
            });

            modalInstance.result.then(function(selectedItem) {
                $scope.selected = selectedItem;
            }, function() {
                $log.info('Modal dismissed at: ' + new Date());
            });
        };

        $scope.openTwoButton = function() {

            var modalInstance = $uibModal.open({
                animation: $scope.animationsEnabled,
                templateUrl: 'myTwoModalContent.html',
                controller: 'ModalInstanceCtrl',
                resolve: {
                    items: function() {
                        return $scope.items;
                    }
                }
            });

            modalInstance.result.then(function(selectedItem) {
                $scope.selected = selectedItem;
            }, function() {
                $log.info('Modal dismissed at: ' + new Date());
            });
        };


    }])

    .controller('ModalInstanceCtrl', ['$scope', '$uibModalInstance', function($scope, $uibModalInstance) {

        $scope.ok = function() {
            $uibModalInstance.close();
        };

        $scope.cancel = function() {
            $uibModalInstance.dismiss('cancel');
        };
    }])

    /*.controller('ProgressDemoCtrl', ['$scope', function($scope) {
        $scope.max = 200;

        $scope.random = function() {
            var value = Math.floor(Math.random() * 100 + 1);
            var type;

            if (value < 25) {
                type = 'success';
            } else if (value < 50) {
                type = 'info';
            } else if (value < 75) {
                type = 'warning';
            } else {
                type = 'danger';
            }

            $scope.showWarning = type === 'danger' || type === 'warning';

            $scope.dynamic = value;
            $scope.type = type;
        };

        $scope.random();

        $scope.randomStacked = function() {
            $scope.stacked = [];
            var types = ['success', 'info', 'warning', 'danger'];

            for (var i = 0, n = Math.floor(Math.random() * 4 + 1); i < n; i++) {
                var index = Math.floor(Math.random() * 4);
                $scope.stacked.push({
                    value: Math.floor(Math.random() * 30 + 1),
                    type: types[index]
                });
            }
        };

        $scope.randomStacked();
    }])*/

    .controller('TabsDemoCtrl', ['$scope', '$window', function($scope, $window) {
        $scope.tabs = [
            { title: 'Dynamic Title 1', content: 'Dynamic content 1' },
            { title: 'Dynamic Title 2', content: 'Dynamic content 2', disabled: true }
        ];

        $scope.model = {
            name: 'Tabs'
        };
		
		/*Hyd SEZ*/
		
       $scope.maxSize = 5;
       $scope.bigTotalItems = 175;
	   /*Hyd SEZ ends here*/
	   
    }])

/*    .controller('TooltipDemoCtrl', ['$scope', '$sce', function($scope, $sce) {
        $scope.dynamicTooltip = 'Hello, World!';
        $scope.dynamicTooltipText = 'dynamic';
        $scope.htmlTooltip = $sce.trustAsHtml('I\'ve been made <b>bold</b>!');
        $scope.placement = {
            options: [
                'top',
                'top-left',
                'top-right',
                'bottom',
                'bottom-left',
                'bottom-right',
                'left',
                'left-top',
                'left-bottom',
                'right',
                'right-top',
                'right-bottom'
            ],
            selected: 'top'
        };
    }])*/

})();
