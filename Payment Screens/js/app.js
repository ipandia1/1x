(function () {
    'use strict';
    var app = angular.module('app', ['ngAnimate', 'ngAria', 'ngAria', 'ngCookies', 'ngMessages', 'ngSanitize', 'ngTouch', 'ui.bootstrap']);

    angular.module("uib/template/modal/window.html", []).run(["$templateCache", function ($templateCache) {
        $templateCache.put("uib/template/modal/window.html",
            "  <button type=\"button\" class=\"close pull-right uib-close\" ng-click=\"close($event)\"><i aria-hidden=\"true\" class=\"ico-closeIcon uib-close\" ng-click=\"close($event)\"></i></button><div class=\"modal-dialog {{size ? 'modal-' + size : ''}}\"><div class=\"modal-content\" uib-modal-transclude></div></div>\n" +
            "");
    }]);
    app.directive('number', function () {
        return {
            require: 'ngModel',
            restrict: 'A',
            link: function (scope, element, attrs, ctrl) {
                ctrl.$parsers.push(function (input) {
                    if (input == undefined) return ''
                    var inputNumber = input.toString().replace(/[^0-9]/g, '');
                    if (inputNumber != input) {
                        ctrl.$setViewValue(inputNumber);
                        ctrl.$render();
                    }
                    return inputNumber;
                });
            }
        };
    });

    app.controller('generalController', ['$scope', function ($scope) {

    }])

        .controller('AccordionDemoCtrl', ['$scope', function ($scope) {
            $scope.oneAtATime = true;

            $scope.groups = [{
                title: 'Dynamic Group Header - 1',
                content: 'Dynamic Group Body - 1'
            }, {
                title: 'Dynamic Group Header - 2',
                content: 'Dynamic Group Body - 2'
            }];

            $scope.items = ['Item 1', 'Item 2', 'Item 3'];

            $scope.addItem = function () {
                var newItemNo = $scope.items.length + 1;
                $scope.items.push('Item ' + newItemNo);
            };

            $scope.status = {
                isCustomHeaderOpen: false,
                isFirstOpen: true,
                isFirstDisabled: false
            };
        }])

        .controller('AlertDemoCtrl', ['$scope', function ($scope) {
            $scope.alerts = [
                { type: 'danger', msg: 'Oh snap! Change a few things up and try submitting again.' },
                { type: 'success', msg: 'Well done! You successfully read this important alert message.' }
            ];

            $scope.addAlert = function () {
                $scope.alerts.push({ msg: 'Another alert!' });
            };

            $scope.closeAlert = function (index) {
                $scope.alerts.splice(index, 1);
            };
        }])

        .controller('ButtonsCtrl', ['$scope', function ($scope) {
            $scope.singleModel = 1;

            $scope.radioModel = 'Middle';

            $scope.checkModel = {
                left: false,
                middle: true,
                right: false
            };

            $scope.checkResults = [];

            $scope.$watchCollection('checkModel', function () {
                $scope.checkResults = [];
                angular.forEach($scope.checkModel, function (value, key) {
                    if (value) {
                        $scope.checkResults.push(key);
                    }
                });
            });
        }])

        .controller('CarouselDemoCtrl', ['$scope', function ($scope) {
            $scope.myInterval = 5000;
            $scope.noWrapSlides = false;
            $scope.active = 0;
            var slides = $scope.slides = [];
            var currIndex = 0;

            $scope.addSlide = function () {
                slides.push({
                    image: 'http://lorempixel.com/580/300',
                    text: ['Nice image', 'Awesome photograph', 'That is so cool', 'I love that'][slides.length % 4],
                    id: currIndex++
                });
            };

            for (var i = 0; i < 4; i++) {
                $scope.addSlide();
            }

            $scope.next = function () {
                alert("next");
            }

        }])


        .controller('DropdownCtrl', ['$scope', '$log', function ($scope, $log) {
            $scope.status = {
                isopen: false
            };

            $scope.toggled = function (open) {
                $log.log('Dropdown is now: ', open);
            };

            $scope.toggleDropdown = function ($event) {
                $event.preventDefault();
                $event.stopPropagation();
                $scope.status.isopen = !$scope.status.isopen;
            };

            $scope.appendToEl = angular.element(document.querySelector('#dropdown-long-content'));
        }])


        .controller('ModalDemoCtrl', ['$scope', '$uibModal', '$log', function ($scope, $uibModal, $log) {


            $scope.close = function () {
                $scope.visible = false;
            }
            $scope.removeGlue = function () {
                let body = document.getElementsByTagName('body')[0];
                body.classList.remove("glueModal");
            };

            $scope.addGlue = function () {
                let body = document.getElementsByTagName('body')[0];
                body.classList.add("glueModal");
            };


            $scope.openOneButton = function () {

                var modalInstance = $uibModal.open({
                    //animation: $scope.animationsEnabled,
                    templateUrl: 'myOneModalContent.html',
                    controller: 'ModalInstanceCtrl',
                    resolve: {
                        items: function () {
                            return $scope.items;
                        }
                    }
                });

                modalInstance.result.then(function (selectedItem) {
                    $scope.selected = selectedItem;
                }, function () {
                    $scope.removeGlue();
                    $log.info('Modal dismissed at: ' + new Date());
                });
                $scope.addGlue();
            };

            $scope.openTwoButton = function () {

                var modalInstance = $uibModal.open({
                    animation: $scope.animationsEnabled,
                    templateUrl: 'myTwoModalContent.html',
                    controller: 'ModalInstanceCtrl',
                    resolve: {
                        items: function () {
                            return $scope.items;
                        }
                    }
                });

                modalInstance.result.then(function (selectedItem) {
                    $scope.selected = selectedItem;
                }, function () {
                    $log.info('Modal dismissed at: ' + new Date());
                });
            };


        }])

        .controller('ModalInstanceCtrl', ['$scope', '$uibModalInstance', function ($scope, $uibModalInstance) {

            $scope.ok = function () {
                $uibModalInstance.close();
            };

            $scope.cancel = function () {
                $uibModalInstance.dismiss('cancel');
            };
        }])

        .controller('addressCtrl', ['$scope', function ($scope) {

        }])
        .controller('confrmnCtrl', ['$scope', function ($scope) {
            $scope.disabledCard = true;
            $scope.bank = true;
            $scope.amex = true;
            $scope.discover = true;
            $scope.master = true;
            $scope.bankDiv = true;
            $scope.amexDiv = true;
            $scope.discoverDiv = true;
            $scope.masterDiv = true;
            $scope.disabledCardDIV = true;
            var timeoutValue = 0;
            $scope.delete = function (id) {

                if ((id === 0 && $scope.bank == true) || (id === 1 && $scope.amex == true) || (id === 2 && $scope.discover == true) || (id === 3 && $scope.master == true) || (id === 4 && $scope.disabledCard == true)) {
                    timeoutValue = 0;
                }
                else {
                    timeoutValue = 700;
                }
                setTimeout(function () {
                    $scope.$apply(function () {

                        if (id === 0) {
                            $scope.bank = !$scope.bank;
                        }
                        if (id === 1) {
                            $scope.amex = !$scope.amex;
                        }
                        if (id === 2) {
                            $scope.discover = !$scope.discover;
                        }
                        if (id === 3) {
                            $scope.master = !$scope.master;
                        }
                        if (id === 4) {
                            $scope.disabledCard = !$scope.disabledCard;
                        }

                    });
                }, timeoutValue);

            }
            $scope.remove = function (id) {
                setTimeout(function () {
                    $scope.$apply(function () {

                        if (id === 0) {
                            $scope.bankDiv = false;
                        }
                        if (id === 1) {
                            $scope.amexDiv = false;
                        }
                        if (id === 2) {
                            $scope.discoverDiv = false;
                        }
                        if (id === 3) {
                            $scope.masterDiv = false;
                        }
                        if (id === 4) {
                            $scope.disabledCardDIV = false;
                        }

                    });
                }, 700);

            }



        }])
        .controller('spmbbpmCtrl', ['$scope', function ($scope) {
            $scope.visa = true;
            $scope.bank = true;
            $scope.visaDiv = true;
            $scope.bankDiv = true;
            var timeoutValue = 0;
            $scope.delete = function (id) {

                if ((id === 0 && $scope.visa == true) || (id === 1 && $scope.bank == true)) {
                    timeoutValue = 0;
                }
                else {
                    timeoutValue = 700;
                }
                setTimeout(function () {
                    $scope.$apply(function () {

                        if (id === 0) {
                            $scope.visa = !$scope.visa;
                        }
                        if (id === 1) {
                            $scope.bank = !$scope.bank;
                        }

                    });
                }, timeoutValue);

            }
            $scope.remove = function (id) {
                setTimeout(function () {
                    $scope.$apply(function () {

                        if (id === 0) {
                            $scope.visaDiv = false;
                        }
                        if (id === 1) {
                            $scope.bankDiv = false;
                        }

                    });
                }, 700);

            }

        }])     
        .controller('cardinfoCtrl', ['$scope', function ($scope) {
            $scope.newCardInfo = false;
            $scope.saveToWalletCheck = true;
            $scope.saveToWallet = true;
            $scope.setAsDefault = true;
            $scope.cardImageModalShow = false;
            $scope.deleteModalShow = false;
            $scope.accNum = '34567';
            $scope.cardIcon = 'palinCardImg';

            $scope.cardImageModalOpen = function () {
                $scope.cardImageModalShow = true;
                document.getElementsByTagName('body')[0].classList.add('noscroll');
                document.getElementById('cardImageModal').focus();
            }
            $scope.cardImageModalClose = function () {
                $scope.cardImageModalShow = false;
                document.getElementById('cardImageModalButton').focus();
                document.getElementsByTagName('body')[0].classList.remove('noscroll');
                
            }
            $scope.deleteModalOpen = function () {
                $scope.deleteModalShow = true;
                document.getElementsByTagName('body')[0].classList.add('noscroll');
                document.getElementById('deleteModal').focus();
            }
            $scope.deleteModalClose = function () {
                $scope.deleteModalShow = false;
                document.getElementsByTagName('body')[0].classList.remove('noscroll');
                document.getElementById('deleteModalButton').focus();
            }

            $scope.backTabEvent = function (event, mainId) {
                if (event.keyCode === 27) {
                    if($scope.cardImageModalShow) {
                        $scope.cardImageModalClose();
                    }
                    if($scope.deleteModalShow) {
                        $scope.deleteModalClose();
                    }
                }
                const allElements = document.getElementById(mainId).querySelectorAll
                    ('button, [href], input, select, textarea, [tabindex]:not([tabindex="-1"])');
                if (document.activeElement === allElements[0]) {
                    if (event.keyCode === 9) {
                        if (event.shiftKey) {
                            event.preventDefault();
                            setTimeout(function () {
                                allElements[allElements.length - 1].focus();
                            }, 0);
                        }
                    }
                }
                if (document.activeElement === document.getElementById(mainId)) {
                    if (event.keyCode === 9) {
                        if (event.shiftKey) {
                            event.preventDefault();
                            setTimeout(function () {
                                allElements[allElements.length - 1].focus();
                            }, 0);
                        }
                    }
                }
                if (document.activeElement === allElements[allElements.length - 1]) {
                    if (event.keyCode === 9) {
                        if (event.shiftKey) { }
                        else {
                            event.preventDefault();
                            setTimeout(function () {
                                document.getElementById(mainId).focus();
                            }, 0);
                        }

                    }
                }
            }
            $scope.formatCard = function (value) {
                if(value) {
                var v = value.replace(/\s+/g, '');
                var matches = v.match(/\d{4,16}/g);
                var match = matches && matches[0] || ''
                var parts = []
                for (var i = 0, len = match.length; i < len; i += 4) {
                    parts.push(match.substring(i, i + 4))
                }
                if (parts.length) {
                    $scope.card_number = parts.join(' ');


                } else {
                    $scope.card_number = value;

                }
            }
            }
            $scope.formatExpireDate = function (value) {
                if(value) {
                var v = value.replace(/\s+/g, '');
                var matches = v.match(/\d{4,16}/g);
                var match = matches && matches[0] || ''
                var parts = []
                for (var i = 0, len = match.length; i < len; i += 2) {
                    parts.push(match.substring(i, i + 2))
                }
                if (parts.length) {
                    $scope.expiryDate = parts.join('/');


                } else {
                    $scope.expiryDate = value;

                }
            }
            }

            $scope.cardChange = function (event) {
                var target = event.target || event.srcElement;
                var val = target.value.split('')[0];

                if (val === '4') {
                    $scope.cardIcon = 'visaImg';
                } else if (val === '5') {
                    $scope.cardIcon = 'masterImg';
                } else if (val === '1') {
                    $scope.cardIcon = 'expressImg';
                } else if (val === '2') {
                    $scope.cardIcon = 'discoverImg';
                } else {
                    $scope.cardIcon = 'palinCardImg';
                }
            }
        }])
        .controller('bankinfoCtrl', ['$scope', function ($scope) {
            $scope.accNum = '34567';

        }])
        .controller('newBankinfoCtrl', ['$scope', function ($scope) {
            $scope.accNum = '';
            $scope.newBankInfo = false; // set false to see edit  bank information page or else true for new bank info
            $scope.saveToWalletCheck = true; //
            $scope.saveToWallet = true;
            $scope.setAsDefault = true;    
            $scope.bankConfirmModalVisible = false;
            $scope.cardVisible = false;

            $scope.bankConfirmModalOpen = function () {
                $scope.bankConfirmModalVisible = true;
                document.getElementById('bankConfirmModal').focus();
                document.getElementsByTagName('body')[0].classList.add('noscroll');
            };
            $scope.bankConfirmModalClose = function () {
                $scope.bankConfirmModalVisible = false;
                document.getElementsByTagName('body')[0].classList.remove('noscroll');
                document.getElementById('bankConfirmModalButton').focus();

            }
            // card image open   
            $scope.cardImageModalOpen = function () {
                $scope.cardVisible = true;
                document.getElementById('cardImageModal').focus();
                document.getElementsByTagName('body')[0].classList.add('noscroll');
            };
            $scope.cardImageModalClose = function () {
                $scope.cardVisible = false;
                document.getElementsByTagName('body')[0].classList.remove('noscroll');
                document.getElementById('cardImageModalButton').focus();

            }
            $scope.backTabEvent = function (event, mainId) {                
                if (event.keyCode === 27) {
                    if($scope.cardVisible) {
                        $scope.cardImageModalClose();
                    }
                    if($scope.bankConfirmModalVisible) {
                        $scope.bankConfirmModalClose();
                    }
                }
                const allElements = document.getElementById(mainId).querySelectorAll
                    ('button, [href], input, select, textarea, [tabindex]:not([tabindex="-1"])');
                if (document.activeElement === allElements[0]) {
                    if (event.keyCode === 9) {
                        if (event.shiftKey) {
                            event.preventDefault();
                            setTimeout(function () {
                                allElements[allElements.length - 1].focus();
                            }, 0);
                        }
                    }
                }
                if (document.activeElement === document.getElementById(mainId)) {
                    if (event.keyCode === 9) {
                        if (event.shiftKey) {
                            event.preventDefault();
                            setTimeout(function () {
                                allElements[allElements.length - 1].focus();
                            }, 0);
                        }
                    }
                }
                if (document.activeElement === allElements[allElements.length - 1]) {
                    if (event.keyCode === 9) {
                        if (event.shiftKey) { }
                        else {
                            event.preventDefault();
                            setTimeout(function () {
                                document.getElementById(mainId).focus();
                            }, 0);
                        }

                    }
                }
            }

        }])
        .controller('modalCtrl', ['$scope', '$window', function ($scope, $window) {
            $scope.hidden = true;

            $scope.manymoreModal = function () {
                document.body.classList.add("noscroll");
                $scope.visibleLCModal = true;
                $scope.visibleAnimation = true;
            }
            $scope.closeModal = function () {
                document.body.classList.remove("noscroll");
                $scope.visibleLCModal = false;
                $scope.visibleAnimation = false;
            }

        }])
        .controller('TranscnCtrl', ['$scope', function ($scope) {


            $scope.hideSummary = false;
            $scope.ShowSummary = true;

            $scope.showTranscn = function () {

                $scope.hideSummary = true;
                $scope.ShowSummary = false;

            }

            $scope.hideTranscn = function () {

                $scope.hideSummary = false;
                $scope.ShowSummary = true;
            }

        }])

    app.controller('paymentProcessingCtrl', ['$scope', '$timeout', function ($scope, $timeout) {

        $scope.showLoaderV2 = false;
        $scope.noOverlayV2 = false;
        $scope.showButton = true;
        $scope.showContent = false;
        $scope.showBalanceUpdated = false;
        $scope.showBalanceNotUpdated = false;

        $scope.showsSpinner = function () {
            $scope.showButton = false;
            $scope.showLoaderV2 = true;
        }

        $scope.hidesSpinner = function () {
            $scope.showLoaderV2 = false;
        }

        $scope.delayedSpinner = function () {
            $scope.showsSpinner();
            $scope.delayedContent();

            $timeout(function () {
                $scope.showContent = true;
                $scope.hidesSpinner();
                $scope.showContent = false;
                $scope.showBalanceUpdated = true; // to show the balance update make it true otherwise false
                $scope.showBalanceNotUpdated = false; // to show the balance not update make it true otherwise false
            }, 5000);

        }

        $scope.delayedContent = function () {

            $timeout(function () {
                $scope.showContent = true;
            }, 2000);
        }

    }]);
    app.controller('selectPayment', ['$scope', '$http', function ($scope, $http) {
        var in_session_1 = "in-session-1";
        var in_session_2 = "in-session-2";
        var in_session_3 = "in-session-3";
        var in_session_4 = "in-session-4";
        var in_session_5 = "in-session-5";
        var in_session_6 = "in-session-6";
        $scope.maxCards = 10;
        $scope.selectButtonDisabled = false;
        $http.get("./json/in-session.json").then(function (response) {
            $scope.data = response.data;
            $scope.sessionDetails = $scope.data[in_session_5][0];
            $scope.cardDetails = $scope.sessionDetails.cardDetails;
            $scope.addPayDetail = $scope.sessionDetails.addPayDetail; /* Add card 
    details */
            if ($scope.cardDetails.length == 0) {
                console.log($scope.cardDetails.length);
                $scope.selectButtonDisabled = true;
            }
            angular.forEach($scope.cardDetails, function (value, key) {
                $scope.date = new Date();
                var currentMonth = $scope.date.getMonth() + 1;
                var currentYear = $scope.date.getFullYear().toString().slice(-2);
                console.log(currentYear + '/' + currentMonth);
                var expirydate = value.expiryDate;
                if (key == 0) {
                    $scope.cardDetails[key].checked = true;
                }
                var cardExpiryMonth = expirydate.substring(1, 2);
                var cardExpiryYear = expirydate.substring(3, 5);
                if (expirydate) {
                    if (cardExpiryMonth <= currentMonth && cardExpiryYear <= currentYear) {
                        value.warning = true;
                        value.disabled = true;
                        value.expiryDate = "Expired " + value.expiryDate;
                        value.icon = value.icon + '-disabled';
                        $scope.cardDetails[key].checked = false;
                        $scope.cardDetails[key + 1].checked = true;
                        $scope.selectButtonDisabled = false;
                    }
                    else if ((cardExpiryMonth - currentMonth <= 1 && cardExpiryYear == currentYear) || (currentMonth == 12 && cardExpiryYear == parseInt(currentYear) + 1)) {
                        value.warning = true;
                        value.expiryDate = "Expiring soon " + value.expiryDate;
                    }
                }
                if (value.action != "Edit") {
                    value.disabled = true;
                    value.icon = value.icon + '-disabled';
                    $scope.selectButtonDisabled = true;
                }

            });


        });
        $scope.checkBoxEnter = function (data) {
            if (!document.getElementById('radio' + data).disabled) {
                document.getElementById('radio' + data).checked = true;
            }
        }
        $scope.moreInfoVisible = false;
        $scope.deleteVisible = false;
        $scope.cardid = "0";

        $scope.deleteModalOpen = function (elementname, action) {
            $scope.cardid = elementname;
            if (action === "Confirm/delete" || action === "Delete") {
                $scope.deleteVisible = true;
                document.getElementById('deleteModal').focus();
                document.getElementsByTagName('body')[0].classList.add('noscroll');
            }
        }
        $scope.deleteModalClose = function () {
            $scope.deleteVisible = false;
            document.getElementsByTagName('body')[0].classList.remove('noscroll');
            document.getElementById($scope.cardid).focus();
        }
        $scope.moreInfoModalOpen = function () {
            $scope.moreInfoVisible = true;
            document.getElementById('moreInfoModal').focus();
            document.getElementsByTagName('body')[0].classList.add('noscroll');
        };
        $scope.moreInfoModalClose = function () {
            $scope.moreInfoVisible = false;
            document.getElementsByTagName('body')[0].classList.remove('noscroll');
            document.getElementById('moreInfoModalButton').focus();

        }
        $scope.backTabEvent = function (event, mainId) {
            const allElements = document.getElementById(mainId).querySelectorAll
                ('button, [href], input, select, textarea, [tabindex]:not([tabindex="-1"])');
            if (document.activeElement === allElements[0]) {
                if (event.keyCode === 9) {
                    if (event.shiftKey) {
                        event.preventDefault();
                        setTimeout(function () {
                            allElements[allElements.length - 1].focus();
                        }, 0);
                    }
                }
            }
            if (document.activeElement === document.getElementById(mainId)) {
                if (event.keyCode === 9) {
                    if (event.shiftKey) {
                        event.preventDefault();
                        setTimeout(function () {
                            allElements[allElements.length - 1].focus();
                        }, 0);
                    }
                }
            }
            if (document.activeElement === allElements[allElements.length - 1]) {
                if (event.keyCode === 9) {
                    if (event.shiftKey) { }
                    else {
                        event.preventDefault();
                        setTimeout(function () {
                            document.getElementById(mainId).focus();
                        }, 0);
                    }

                }
            }
        }
    }])
        .controller('claimFlow', ['$scope', function ($scope) {
            $scope.card = true;
            $scope.bank = false;
            $scope.cardValid = true;
            $scope.status = "";
            $scope.confirmModalVisbile = false;
            $scope.cardIcon = 'palinCardImg';
            $scope.confirmModalOpen = function () {
                $scope.confirmModalVisbile = true;
                document.getElementById('confirmModal').focus();
                document.getElementsByTagName('body')[0].classList.add('noscroll');
            };
            $scope.confirmModalClose = function () {
                $scope.confirmModalVisbile = false;
                document.getElementsByTagName('body')[0].classList.remove('noscroll');
                document.getElementById('confirmModalButton').focus();

            }
            $scope.cardNumberChange = function () {
                if ($scope.cardNumber != null && $scope.cardNumber != '') {
                    $scope.status = "dirty";
                    if ($scope.cardNumber.length == 19 && $scope.cardValid) {
                        $scope.status = "valid"
                    }
                    else if ($scope.cardNumber.length == 19 && !$scope.cardValid) {
                        $scope.status = "error"
                    }
                }
            }
            $scope.formatCard = function (value) {
                if(value) {
                    var v = value.replace(/\s+/g, '');
                    var matches = v.match(/\d{4,16}/g);
                    var match = matches && matches[0] || ''
                    var parts = []
                    for (var i = 0, len = match.length; i < len; i += 4) {
                        parts.push(match.substring(i, i + 4))
                    }
                    if (parts.length) {
                        $scope.card_number = parts.join(' ');


                    } else {
                        $scope.card_number = value;

                    }
                }
            }
            
            $scope.cardChange = function (event) {
                var target = event.target || event.srcElement;
                var val = target.value.split('')[0];

                if (val === '4') {
                    $scope.cardIcon = 'visaImg';
                } else if (val === '5') {
                    $scope.cardIcon = 'masterImg';
                } else if (val === '1') {
                    $scope.cardIcon = 'expressImg';
                } else if (val === '2') {
                    $scope.cardIcon = 'discoverImg';
                } else {
                    $scope.cardIcon = 'palinCardImg';
                }
            }
            $scope.backTabEvent = function (event, mainId) {
                const allElements = document.getElementById(mainId).querySelectorAll
                    ('button, [href], input, select, textarea, [tabindex]:not([tabindex="-1"])');
                if (document.activeElement === allElements[0]) {
                    if (event.keyCode === 9) {
                        if (event.shiftKey) {
                            event.preventDefault();
                            setTimeout(function () {
                                allElements[allElements.length - 1].focus();
                            }, 0);
                        }
                    }
                }
                if (document.activeElement === document.getElementById(mainId)) {
                    if (event.keyCode === 9) {
                        if (event.shiftKey) {
                            event.preventDefault();
                            setTimeout(function () {
                                allElements[allElements.length - 1].focus();
                            }, 0);
                        }
                    }
                }
                if (document.activeElement === allElements[allElements.length - 1]) {
                    if (event.keyCode === 9) {
                        if (event.shiftKey) { }
                        else {
                            event.preventDefault();
                            setTimeout(function () {
                                document.getElementById(mainId).focus();
                            }, 0);
                        }
                    }
                }
                if (event.keyCode === 27) {
                    $scope.confirmModalClose();
                }
            }
        }])
        app.controller('otpCtrl', ['$scope', function ($scope) {
        $scope.applePay=true;
        $scope.otherPay=false;
        }])
})();
