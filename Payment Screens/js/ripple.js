angular.module('app')
    .directive('ripple', function () {
      
        return {
			
            link: function (scope, element) {
             element.bind('click',function(event) {
			 element.css('overflow', 'hidden');
      element.css('transform', 'translateZ(0)');
	  element.css('-moz-transform','translateZ(0)');
	  
        if (element[0].querySelector('span') == null) {
      var ripple = document.createElement('span');
      var currentelementpositions = element[0].getBoundingClientRect();
      var radius;
      if (currentelementpositions.height > currentelementpositions.width) {
        radius = currentelementpositions.height;
      }
      else {
        radius = currentelementpositions.width;
      }

      var scrolltop = (document.documentElement && document.documentElement.scrollTop) || document.body.scrollTop;
      var scrollleft = (document.documentElement && document.documentElement.scrollLeft) || document.body.scrollLeft;
      var rippleleft = event.pageX - currentelementpositions.left - radius / 2 - scrollleft;
      var rippletop = event.pageY - currentelementpositions.top - radius / 2 - scrolltop;
      ripple.className = 'glueRippleAnimate';
      ripple.style.width = ripple.style.height = radius + 'px';
      ripple.style.left = rippleleft + 'px';
      ripple.style.top = rippletop + 'px';
      ripple.addEventListener('webkitAnimationEnd',function() {
        element[0].removeChild(ripple);
      });

     element[0].appendChild(ripple);
    }      })}
        };
    });
	
	
	
