(function () {
    'use strict';
    var app = angular.module('app', ['ngAnimate', 'ngAria', 'ngAria', 'ngCookies', 'ngMessages', 'ngSanitize', 'ngTouch', 'ui.bootstrap', 'ngRoute']);
    /*app.config(function ($routeProvider) {
        $routeProvider.
            when("/", {
                templateUrl: "partials/line-selector.html",
                controller: "tradeInCtrl"
            }).
            when("/lineSelector", {
                templateUrl: "partials/line-selector.html",
                controller: "tradeInCtrl"
            }).
            when("/trade-in-condition", {
                templateUrl: "partials/trade-in-condition.html",
                controller: "tradeInConditionCtrl"
            }).
            when("/trade-in-value", {
                templateUrl: "partials/trade-in-value.html",
                controller: "tradeInValueCtrl"
            });
    });*/

    angular.module("uib/template/modal/window.html", []).run(["$templateCache", function ($templateCache) {
        $templateCache.put("uib/template/modal/window.html",
            "  <button type=\"button\" class=\"close pull-right uib-close\" ng-click=\"close($event)\"><i aria-hidden=\"true\" class=\"ico-closeIcon uib-close\" ng-click=\"close($event)\"></i></button><div class=\"modal-dialog {{size ? 'modal-' + size : ''}}\"><div class=\"modal-content\" uib-modal-transclude></div></div>\n" +
            "");
    }]);

    app.controller('generalController', ['$scope', function ($scope) {

        }])

        .controller('AccordionDemoCtrl', ['$scope', function ($scope) {
            $scope.oneAtATime = true;

            $scope.groups = [{
                title: 'Dynamic Group Header - 1',
                content: 'Dynamic Group Body - 1'
            }, {
                title: 'Dynamic Group Header - 2',
                content: 'Dynamic Group Body - 2'
            }];

            $scope.items = ['Item 1', 'Item 2', 'Item 3'];

            $scope.addItem = function () {
                var newItemNo = $scope.items.length + 1;
                $scope.items.push('Item ' + newItemNo);
            };

            $scope.status = {
                isCustomHeaderOpen: false,
                isFirstOpen: true,
                isFirstDisabled: false
            };
        }])

        .controller('AlertDemoCtrl', ['$scope', function ($scope) {
            $scope.alerts = [{
                    type: 'danger',
                    msg: 'Oh snap! Change a few things up and try submitting again.'
                },
                {
                    type: 'success',
                    msg: 'Well done! You successfully read this important alert message.'
                }
            ];

            $scope.addAlert = function () {
                $scope.alerts.push({
                    msg: 'Another alert!'
                });
            };

            $scope.closeAlert = function (index) {
                $scope.alerts.splice(index, 1);
            };
        }])

        .controller('ButtonsCtrl', ['$scope', function ($scope) {
            $scope.singleModel = 1;

            $scope.radioModel = 'Middle';

            $scope.checkModel = {
                left: false,
                middle: true,
                right: false
            };

            $scope.checkResults = [];

            $scope.$watchCollection('checkModel', function () {
                $scope.checkResults = [];
                angular.forEach($scope.checkModel, function (value, key) {
                    if (value) {
                        $scope.checkResults.push(key);
                    }
                });
            });
        }])

        .controller('CarouselDemoCtrl', ['$scope', function ($scope) {
            $scope.myInterval = 5000;
            $scope.noWrapSlides = false;
            $scope.active = 0;
            var slides = $scope.slides = [];
            var currIndex = 0;

            $scope.addSlide = function () {
                slides.push({
                    image: 'http://lorempixel.com/580/300',
                    text: ['Nice image', 'Awesome photograph', 'That is so cool', 'I love that'][slides.length % 4],
                    id: currIndex++
                });
            };

            for (var i = 0; i < 4; i++) {
                $scope.addSlide();
            }

            $scope.next = function () {
                alert("next");
            }

        }])


        .controller('DropdownCtrl', ['$scope', '$log', function ($scope, $log) {
            $scope.status = {
                isopen: false
            };

            $scope.toggled = function (open) {
                $log.log('Dropdown is now: ', open);
            };

            $scope.toggleDropdown = function ($event) {
                $event.preventDefault();
                $event.stopPropagation();
                $scope.status.isopen = !$scope.status.isopen;
            };

            $scope.appendToEl = angular.element(document.querySelector('#dropdown-long-content'));
        }])


        .controller('ModalDemoCtrl', ['$scope', '$uibModal', '$log', function ($scope, $uibModal, $log) {


            $scope.close = function () {
                $scope.visible = false;
            }
            $scope.removeGlue = function () {
                let body = document.getElementsByTagName('body')[0];
                body.classList.remove("glueModal");
            };

            $scope.addGlue = function () {
                let body = document.getElementsByTagName('body')[0];
                body.classList.add("glueModal");
            };


            $scope.openOneButton = function () {

                var modalInstance = $uibModal.open({
                    //animation: $scope.animationsEnabled,
                    templateUrl: 'myOneModalContent.html',
                    controller: 'ModalInstanceCtrl',
                    resolve: {
                        items: function () {
                            return $scope.items;
                        }
                    }
                });

                modalInstance.result.then(function (selectedItem) {
                    $scope.selected = selectedItem;
                }, function () {
                    $scope.removeGlue();
                    $log.info('Modal dismissed at: ' + new Date());
                });
                $scope.addGlue();
            };

            $scope.openTwoButton = function () {

                var modalInstance = $uibModal.open({
                    animation: $scope.animationsEnabled,
                    templateUrl: 'myTwoModalContent.html',
                    controller: 'ModalInstanceCtrl',
                    resolve: {
                        items: function () {
                            return $scope.items;
                        }
                    }
                });

                modalInstance.result.then(function (selectedItem) {
                    $scope.selected = selectedItem;
                }, function () {
                    $log.info('Modal dismissed at: ' + new Date());
                });
            };


        }])

        .controller('ModalInstanceCtrl', ['$scope', '$uibModalInstance', function ($scope, $uibModalInstance) {

            $scope.ok = function () {
                $uibModalInstance.close();
            };

            $scope.cancel = function () {
                $uibModalInstance.dismiss('cancel');
            };
        }])

        .controller('addressCtrl', ['$scope', function ($scope) {

        }])
        .controller('cardInformation', ['$scope', function ($scope) {
            $scope.nameoncard = 'Jane Doe';
            $scope.month = "01";
            $scope.year = "01";
            $scope.cardNumber = "1111-1111-1111-1111";
            $scope.cvv = "111";
            $scope.zip = "1111";
            $scope.nickname = "Roomate Card";
        }])
        .controller('bankinformation', ['$scope', function ($scope) {
            $scope.nameonaccount = 'Jane Doe';
            $scope.routingnumber = '11111111';
            $scope.accountnumber = '11111111111111111';
            $scope.nickname = 'Wife’s Account';
        }])
        .controller('tradeInCtrl', ['$scope', '$http', '$window', '$location', '$timeout', function ($scope, $http, $window, $location, $timeout) {
            $scope.tradeIn = false;
            // $scope.tradeCondtn = true;
            // $scope.tradeValue = true;
            $scope.lineSelector = false;
            $scope.lineSelectorDetails = true;
            var jsonPath = "json/tradeIn.json";
            $scope.showDetailsMobile = true;
            $scope.lineSelectorDetailsDesktop = true;
            $scope.tradeTitle = 'How trade-in works';
            $scope.tradeArray = [{
                    'valid': true,
                    'content': 'Curabitur blandit tempus porttitor. Morbi leo risus, porta ac consectetur ac, vestibulum at eros. Vivamus sagittis lacus vel augue laoreet rutrum faucibus dolor auctor. Aenean lacinia bibendum nulla sed consectetur.'
                },
                {
                    'valid': true,
                    'content': 'Etiam porta sem malesuada magna mollis euismod. Cras mattis consectetur purus sit amet fermentum. Aenean lacinia bibendum nulla sed consectetur. Cras mattis consectetur purus sit amet fermentum.'
                }
            ];

            $scope.viewChange = function () {
                if ($window.innerWidth >= 768) {
                    $scope.isDesktop = true;
                } else {
                    $scope.isDesktop = false;
                }
            }
            $window.onload = function () {
                $window.scrollTo(0, 0);
                $scope.viewChange();
                if ($location.$$url == '/line-selector-trade-in') {
                    $window.location.href  =  '#/';
                }
            }
            angular.element($window).bind('resize', function () {
                if ($window.innerWidth >= 768) {
                    $scope.$apply(function  ()  {
                        $scope.isDesktop = true;
                    });
                } else {
                    $scope.$apply(function  ()  {
                        $scope.isDesktop = false;
                    });
                }
            });
            $scope.viewChange();
            $http.get(jsonPath).then(function (response) {
                $scope.title = response.data.title;
                $scope.details = response.data.details;
            });
            $scope.tradeModal = function () {
                document.body.classList.add("noscroll");
                $scope.visible = true;
                $scope.visibleAnimation = true;
            }
            $scope.close = function () {
                document.body.classList.remove("noscroll");
                $scope.visible = false;
                $scope.visibleAnimation = false;
            }
            $scope.hidden = true;
            $scope.animate = function (index) {
                $scope.hidden = false;
                $timeout(function () {
                    $window.scrollTo(0, 0);
                    $location.path('/line-selector-trade-in');
                    $scope.viewChange();
                    $scope.lineSelectorDetails = false;
                    $scope.lineSelectorDetailsDesktop = true;
                    for (var i = 0; i < $scope.details.length; i++) {
                        if (('object-' + i) != ('object-' + index)) {
                            
                            if ($scope.lineSelectorDetailsDesktop) {
                                document.getElementById('object-' + i).style.display = 'none';
                                document.getElementById('lineSelector-title').style.display = 'none';
                            } else {
                                document.getElementById('object-' + i).style.display = 'block';
                            }
                        } else {
                            if ($window.innerWidth >= 768) {
                            //    alert($window.innerWidth);
                            document.getElementById('object-' + i).style.animation  =  'mymove_dektop 1s'; }
                            else
                                {
                                     document.getElementById('object-' + i).style.animation  =  'mymove_mobile 1s'; 
                                }
                            document.getElementById('object-' + i).style.transitionTimingFunction  =  'linear';
                        }
                    }
                }, 300);

            }

            $scope.repeatAnimationToGoBack = function () {
                $location.path('/line-selector-trade-in');
                $scope.viewChange();
                $scope.lineSelectorDetails = true;
                $scope.lineSelectorDetailsDesktop = false;
                for (var i = 0; i < $scope.details.length; i++) {
                    document.getElementById('object-' + i).style.display = 'block';
                    document.getElementById('lineSelector-title').style.display = 'block';
                              
                     document.getElementById('object-' + i).style.animation  =  'repeatMyMove_desktop 1s';
                     document.getElementById('object-' + i).style.transitionTimingFunction  =  'linear';
                       document.getElementById('lineSelector-title').style.animation  =  'repeatMyMove_desktop 1s';                 
                  
                    document.getElementById('lineSelector-title').style.transitionTimingFunction  =  'linear';
                }
            }
            $scope.goBack = function () {
                $window.scrollTo(0, 0);
                $scope.hidden = true;
                if ($location.$$url == "/line-selector-trade-in") {
                    $scope.repeatAnimationToGoBack();
                    $location.path('');
                }
            }
        }])
        .controller('tradeInConditionCtrl', ['$scope', '$http', '$window', '$location', '$timeout', function ($scope, $http, $window, $location, $timeout) {
            $scope.tradeCondtn = false;
            $scope.tradeValue = true;
            $scope.hidden = true;
            $scope.phonecondtion_fullImage = "./img/phone-condition-desktop.png";
            var jsonPathforTradeIn = "json/tradeIn2.json";
            $scope.goToTradeVal = function () {
                $timeout(function () {
                    $window.scrollTo(0, 0);
                    $scope.tradeCondtn = true;
                    $scope.tradeValue = false;
                    $location.path('/trade-value');
                }, 300);
            }

            $scope.phoneModal = function () {
                document.body.classList.add("noscroll");
                $scope.visible = true;
                $scope.visibleAnimation = true;
            }
            $scope.close = function () {
                document.body.classList.remove("noscroll");
                //                $scope.visible = false;
                $scope.visibleAnimation = false;
            }
            $scope.phoneTitle = 'Phone condition';
            $scope.phoneArray = [{
                    'quesn': 'Curabitur blandit tempus porttitor. Morbi leo risus, porta ac consectetur ac?',
                    'answer': 'Vestibulum at eros. Vivamus sagittis lacus vel augue laoreet rutrum faucibus dolor auctor. Aenean lacinia bibendum nulla sed consectetur.'
                },
                {
                    'quesn': 'Etiam porta sem malesuada magna mollis euismod? ',
                    'answer': 'Cras mattis consectetur purus sit amet fermentum. Aenean lacinia bibendum nulla sed consectetur. Cras mattis consectetur purus sit amet fermentum.'
                },
                {
                    'quesn': 'Maecenas faucibus mollis interdum?',
                    'answer': 'Fusce dapibus, tellus ac cursus commodo, tortor mauris condimentum nibh, ut fermentum massa justo sit amet risus. Nulla vitae elit libero, a pharetra augue. Integer posuere erat a ante venenatis dapibus posuere velit aliquet.'
                }
            ];

            $http.get(jsonPathforTradeIn).then(function(response){
                $scope.tradeinDetails = response.data.details;

            });
            $scope.goBack = function () {
                $window.scrollTo(0, 0);
                $scope.hidden = true;
                if ($location.$$url == "/trade-value") {
                    $location.path('');
                    $scope.tradeValue = true;
                    $scope.tradeCondtn = false;

                }
            }
        }])
})();