(function () {
  "use strict";
  var app = angular.module("app", [
    "ngAnimate",
    "ngAria",
    "ngAria",
    "ngCookies",
    "ngMessages",
    "ngSanitize",
    "ngTouch",
    "ui.bootstrap",
    "ngRoute",
  ]);

  app.config(function ($routeProvider) {
    $routeProvider
      .when("/", {
        templateUrl: "partials/device-non-nyc.html",
        controller: "deviceprotectionCtrl",
      })
      .when("/non-nyc-plans", {
        templateUrl: "partials/device-non-nyc-plans.html",
        controller: "deviceprotectionCtrl",
      })
      .when("/nyc-migration", {
        templateUrl: "partials/device-nyc-migration.html",
        controller: "deviceprotectionCtrl",
      })
      .when("/non-nyc-migration", {
        templateUrl: "partials/device-non-nyc-migration.html",
        controller: "deviceprotectionCtrl",
      })
      .when("/non_nyc_others", {
        templateUrl: "partials/device-protection-others.html",
        controller: "deviceprotectionCtrl",
      });
  });

  angular.module("uib/template/modal/window.html", []).run([
    "$templateCache",
    function ($templateCache) {
      $templateCache.put(
        "uib/template/modal/window.html",
        '  <button type="button" class="close pull-right uib-close" ng-click="close($event)"><i aria-hidden="true" class="ico-closeIcon uib-close" ng-click="close($event)"></i></button><div class="modal-dialog {{size ? \'modal-\' + size : \'\'}}"><div class="modal-content" uib-modal-transclude></div></div>\n' +
          ""
      );
    },
  ]);

  app
    .controller("generalController", ["$scope", function ($scope) {}])

    .controller("AccordionDemoCtrl", [
      "$scope",
      function ($scope) {
        $scope.oneAtATime = true;

        $scope.groups = [
          {
            title: "Dynamic Group Header - 1",
            content: "Dynamic Group Body - 1",
          },
          {
            title: "Dynamic Group Header - 2",
            content: "Dynamic Group Body - 2",
          },
        ];

        $scope.items = ["Item 1", "Item 2", "Item 3"];

        $scope.addItem = function () {
          var newItemNo = $scope.items.length + 1;
          $scope.items.push("Item " + newItemNo);
        };

        $scope.status = {
          isCustomHeaderOpen: false,
          isFirstOpen: true,
          isFirstDisabled: false,
        };
      },
    ])

    .controller("AlertDemoCtrl", [
      "$scope",
      function ($scope) {
        $scope.alerts = [
          {
            type: "danger",
            msg: "Oh snap! Change a few things up and try submitting again.",
          },
          {
            type: "success",
            msg:
              "Well done! You successfully read this important alert message.",
          },
        ];

        $scope.addAlert = function () {
          $scope.alerts.push({
            msg: "Another alert!",
          });
        };

        $scope.closeAlert = function (index) {
          $scope.alerts.splice(index, 1);
        };
      },
    ])

    .controller("ButtonsCtrl", [
      "$scope",
      function ($scope) {
        $scope.singleModel = 1;

        $scope.radioModel = "Middle";

        $scope.checkModel = {
          left: false,
          middle: true,
          right: false,
        };

        $scope.checkResults = [];

        $scope.$watchCollection("checkModel", function () {
          $scope.checkResults = [];
          angular.forEach($scope.checkModel, function (value, key) {
            if (value) {
              $scope.checkResults.push(key);
            }
          });
        });
      },
    ])

    .controller("CarouselDemoCtrl", [
      "$scope",
      function ($scope) {
        $scope.myInterval = 5000;
        $scope.noWrapSlides = false;
        $scope.active = 0;
        var slides = ($scope.slides = []);
        var currIndex = 0;

        $scope.addSlide = function () {
          slides.push({
            image: "http://lorempixel.com/580/300",
            text: [
              "Nice image",
              "Awesome photograph",
              "That is so cool",
              "I love that",
            ][slides.length % 4],
            id: currIndex++,
          });
        };

        for (var i = 0; i < 4; i++) {
          $scope.addSlide();
        }

        $scope.next = function () {
          alert("next");
        };
      },
    ])

    .controller("DropdownCtrl", [
      "$scope",
      "$log",
      function ($scope, $log) {
        $scope.status = {
          isopen: false,
        };

        $scope.toggled = function (open) {
          $log.log("Dropdown is now: ", open);
        };

        $scope.toggleDropdown = function ($event) {
          $event.preventDefault();
          $event.stopPropagation();
          $scope.status.isopen = !$scope.status.isopen;
        };

        $scope.appendToEl = angular.element(
          document.querySelector("#dropdown-long-content")
        );
      },
    ])

    .controller("ModalDemoCtrl", [
      "$scope",
      "$uibModal",
      "$log",
      function ($scope, $uibModal, $log) {
        $scope.close = function () {
          $scope.visible = false;
        };
        $scope.removeGlue = function () {
          let body = document.getElementsByTagName("body")[0];
          body.classList.remove("glueModal");
        };

        $scope.addGlue = function () {
          let body = document.getElementsByTagName("body")[0];
          body.classList.add("glueModal");
        };

        $scope.openOneButton = function () {
          var modalInstance = $uibModal.open({
            //animation: $scope.animationsEnabled,
            templateUrl: "myOneModalContent.html",
            controller: "ModalInstanceCtrl",
            resolve: {
              items: function () {
                return $scope.items;
              },
            },
          });

          modalInstance.result.then(
            function (selectedItem) {
              $scope.selected = selectedItem;
            },
            function () {
              $scope.removeGlue();
              $log.info("Modal dismissed at: " + new Date());
            }
          );
          $scope.addGlue();
        };

        $scope.openTwoButton = function () {
          var modalInstance = $uibModal.open({
            animation: $scope.animationsEnabled,
            templateUrl: "myTwoModalContent.html",
            controller: "ModalInstanceCtrl",
            resolve: {
              items: function () {
                return $scope.items;
              },
            },
          });

          modalInstance.result.then(
            function (selectedItem) {
              $scope.selected = selectedItem;
            },
            function () {
              $log.info("Modal dismissed at: " + new Date());
            }
          );
        };
      },
    ])

    .controller("ModalInstanceCtrl", [
      "$scope",
      "$uibModalInstance",
      function ($scope, $uibModalInstance) {
        $scope.ok = function () {
          $uibModalInstance.close();
        };

        $scope.cancel = function () {
          $uibModalInstance.dismiss("cancel");
        };
      },
    ])

    .controller("addressCtrl", ["$scope", function ($scope) {}])
    .controller("cardInformation", [
      "$scope",
      function ($scope) {
        $scope.nameoncard = "Jane Doe";
        $scope.month = "01";
        $scope.year = "01";
        $scope.cardNumber = "1111-1111-1111-1111";
        $scope.cvv = "111";
        $scope.zip = "1111";
        $scope.nickname = "Roomate Card";
      },
    ])
    .controller("bankinformation", [
      "$scope",
      function ($scope) {
        $scope.nameonaccount = "Jane Doe";
        $scope.routingnumber = "11111111";
        $scope.accountnumber = "11111111111111111";
        $scope.nickname = "Wife’s Account";
      },
    ])
    .controller("tradeInCtrl", [
      "$scope",
      "$http",
      "$window",
      "$location",
      "$timeout",
      function ($scope, $http, $window, $location, $timeout) {
        $scope.tradeIn = false;
        $scope.lineSelector = false;
        $scope.lineSelectorDetails = true;
        var jsonPath = "json/tradeIn.json";
        $scope.showDetailsMobile = true;
        $scope.lineSelectorDetailsDesktop = true;
        $scope.tradeTitle = "How trade-in works";
        $scope.tradeArray = [
          {
            valid: true,
            content:
              "Curabitur blandit tempus porttitor. Morbi leo risus, porta ac consectetur ac, vestibulum at eros. Vivamus sagittis lacus vel augue laoreet rutrum faucibus dolor auctor. Aenean lacinia bibendum nulla sed consectetur.",
          },
          {
            valid: true,
            content:
              "Etiam porta sem malesuada magna mollis euismod. Cras mattis consectetur purus sit amet fermentum. Aenean lacinia bibendum nulla sed consectetur. Cras mattis consectetur purus sit amet fermentum.",
          },
        ];

        $scope.tradeInUrls = [
          "partials/trade-in-template-bullet.html",
          "partials/trade-in-template-JUMP-promo-eligible.html",
          "partials/trade-in-template-JUMP.html",
          "partials/trade-in-template.html",
        ];

        $scope.tradeInUrlVariable = 2; // change the value 0/1/2/3 to view diffrent template

        $scope.viewChange = function () {
          if ($window.innerWidth >= 768) {
            $scope.isDesktop = true;
          } else {
            $scope.isDesktop = false;
          }
        };
        $window.onload = function () {
          $window.scrollTo(0, 0);
          $scope.viewChange();
          if ($location.$$url == "/line-selector-trade-in") {
            $window.location.href = "#/";
          }
        };
        angular.element($window).bind("resize", function () {
          if ($window.innerWidth >= 768) {
            $scope.$apply(function () {
              $scope.isDesktop = true;
            });
          } else {
            $scope.$apply(function () {
              $scope.isDesktop = false;
            });
          }
        });
        $scope.viewChange();
        $http.get(jsonPath).then(function (response) {
          $scope.title = response.data.title;
          $scope.details = response.data.details;
        });
        $scope.tradeModal = function () {
          document.body.classList.add("noscroll");
          $scope.visible = true;
          $scope.visibleAnimation = true;
        };
        $scope.close = function () {
          document.body.classList.remove("noscroll");
          $scope.visible = false;
          $scope.visibleAnimation = false;
        };
        $scope.hidden = true;

        // Ankit Jain 0628 - Making changes to animate fade in intead of right to left.
        $scope.animate = function (index) {
          $scope.hidden = false;
          $timeout(function () {
            $window.scrollTo(0, 0);
            $location.path("/line-selector-trade-in");
            $scope.viewChange();
            $scope.lineSelectorDetails = false;
            $scope.lineSelectorDetailsDesktop = true;
            for (var i = 0; i < $scope.details.length; i++) {
              if ("object-" + i != "object-" + index) {
                if ($scope.lineSelectorDetailsDesktop) {
                  document.getElementById("object-" + i).style.display = "none";
                  document.getElementById("lineSelector-title").style.display =
                    "none";
                } else {
                  document.getElementById("object-" + i).style.display =
                    "block";
                }
              } else {
                if ($window.innerWidth >= 768) {
                  document.getElementById("object-" + i).style.animation =
                    "mymove_dektop 500ms";
                  document.getElementById(
                    "lineSelectorDetails-text"
                  ).style.display = "block";
                  document.getElementById(
                    "lineSelectorDetails-text"
                  ).style.animation = "mymove_dektop 500ms";
                } else {
                  document.getElementById("object-" + i).style.animation =
                    "mymove_mobile 500ms";
                  document.getElementById(
                    "lineSelectorDetails-text"
                  ).style.display = "block";
                  document.getElementById(
                    "lineSelectorDetails-text"
                  ).style.animation = "mymove_mobile 500ms";
                }
                document.getElementById(
                  "object-" + i
                ).style.transitionTimingFunction = "linear";
              }
            }
          }, 500);
        };
        // Ankit Jain 0628 - Making changes to animate fade in intead of right to left.
        $scope.repeatAnimationToGoBack = function () {
          $location.path("/line-selector-trade-in");
          $scope.viewChange();
          $scope.lineSelectorDetails = true;
          $scope.lineSelectorDetailsDesktop = false;
          for (var i = 0; i < $scope.details.length; i++) {
            document.getElementById("object-" + i).style.display = "block";
            document.getElementById("lineSelector-title").style.display =
              "block";
            document.getElementById("lineSelectorDetails-text").style.display =
              "none";
            document.getElementById("object-" + i).style.animation =
              "repeatMyMove_desktop 500ms";
            document.getElementById("lineSelector-title").style.animation =
              "repeatMyMove_desktop 500ms";
          }
        };
        $scope.goBack = function () {
          $window.scrollTo(0, 0);
          $scope.hidden = true;
          if ($location.$$url == "/line-selector-trade-in") {
            $scope.repeatAnimationToGoBack();
            $location.path("");
          }
        };
      },
    ])
    .controller("tradeInConditionCtrl", [
      "$scope",
      "$http",
      "$window",
      "$location",
      "$timeout",
      function ($scope, $http, $window, $location, $timeout) {
        $scope.tradeCondtn = false;
        $scope.tradeValue = true;
        $scope.hidden = true;
        $scope.phonecondtion_fullImage = "./img/phone-condition-desktop.png";
        $scope.phonecondtion1_fullImage = "./img/phone-condition1-desktop.png";
        var jsonPathforTradeIn = "json/tradeIn2.json";
        $scope.goToTradeVal = function () {
          $timeout(function () {
            // $window.scrollTo(0, 0);
            $scope.tradeCondtn = true;
            $scope.tradeValue = false;
            $location.path("/trade-value");
            // Ankit jain - 0628 - Fade in effect to move from one screen to another.
            document.getElementById("tradeValue").style.animation =
              "fadeInEffect 2s";
            document.getElementById("tradeValue").style.display = "block";
            document.getElementById("tradeCondition").style.display = "none";
          }, 300);
        };

        $scope.phoneModal = function () {
          document.body.classList.add("noscroll");
          $scope.visible = true;
          $scope.visibleAnimation = true;
        };
        $scope.close = function () {
          document.body.classList.remove("noscroll");
          //                $scope.visible = false;
          $scope.visibleAnimation = false;
        };
        $scope.phoneTitle = "Phone condition";
        $scope.phoneArray = [
          {
            quesn:
              "Curabitur blandit tempus porttitor. Morbi leo risus, porta ac consectetur ac?",
            answer:
              "Vestibulum at eros. Vivamus sagittis lacus vel augue laoreet rutrum faucibus dolor auctor. Aenean lacinia bibendum nulla sed consectetur.",
          },
          {
            quesn: "Etiam porta sem malesuada magna mollis euismod? ",
            answer:
              "Cras mattis consectetur purus sit amet fermentum. Aenean lacinia bibendum nulla sed consectetur. Cras mattis consectetur purus sit amet fermentum.",
          },
          {
            quesn: "Maecenas faucibus mollis interdum?",
            answer:
              "Fusce dapibus, tellus ac cursus commodo, tortor mauris condimentum nibh, ut fermentum massa justo sit amet risus. Nulla vitae elit libero, a pharetra augue. Integer posuere erat a ante venenatis dapibus posuere velit aliquet.",
          },
        ];

        $scope.phoneConditionURLs = [
          "partials/phone-condition-template-bad-condition.html",
          "partials/phone-condition-template-bullet.html",
          "partials/phone-condition-template.html",
        ];

        $scope.phoneConditionVariable = 1; // change the value 0/1/2 to view diffrent template

        $http.get(jsonPathforTradeIn).then(function (response) {
          $scope.tradeinDetails = response.data.details;
        });
        $scope.goBack = function () {
          $window.scrollTo(0, 0);
          $scope.hidden = true;
          if ($location.$$url == "/trade-value") {
            $location.path("");
            $scope.tradeValue = true;
            $scope.tradeCondtn = false;
            // Ankit jain - 0628 - Fade in effect to move from one screen to another.
            document.getElementById("tradeCondition").style.display = "block";
            document.getElementById("tradeCondition").style.animation =
              "fadeInEffect 2s";
            document.getElementById("tradeValue").style.animation =
              "fadeInEffect 2s";
            document.getElementById("tradeValue").style.display = "none";
          }
        };
      },
    ])

    .controller("deviceprotectionCtrl", [
      "$scope",
      "$http",
      "$window",
      "$location",
      "$timeout",
      function ($scope, $http, $window, $location, $timeout) {
        $scope.pdp_nonnyc_others_lists = [
          {
            protection_bestValue: "BEST VALUE!",
            protection_id: "Protection 360",
            protection_price: "$14.00/mo",
            protection_description: [
              "Replace phone when lost or stolen",
              "Extended warranty",
              "Free upgrade after 50% balance paid",
              "Apple Care, ID protection, McAfee Security and Premium tech support ",
            ],
          },
          {
            protection_bestValue: "",
            protection_id: "Basic",
            protection_price: "$13.00/mo",
            protection_description: [
              "Replace phone when lost or stolen",
              "Extended warranty",
            ],
          },
          {
            protection_bestValue: "",
            protection_id: "I don't need protection for my phone",
          },
        ];
        $scope.phonecondtion_fullImage = "./img/phone-condition-desktop.png";

        $scope.pdp_nyc_plans_lists = [
          {
            protection_bestValue: "BEST VALUE!",
            protection_id: "Protection 360",
            protection_price: "$14.00/mo",
            protection_description: [
              "Replace phone when lost or stolen",
              "Extended warranty",
              "Free upgrade after 50% balance paid",
              "Apple Care, ID protection, McAfee Security and Premium tech support ",
            ],
          },
          {
            protection_bestValue: "",
            protection_id: "Extended warranty",
            protection_price: "$6.75/mo.",
            protection_description: [
              "Maecenas faucibus mollis interdum. Curabitur blandit tempus porttitor.",
            ],
          },

          {
            protection_bestValue: "",
            protection_id: "Insurance only",
            protection_price: "$4.00/mo.",
            protection_description: [
              "Maecenas faucibus mollis interdum. Curabitur blandit tempus porttitor.",
            ],
          },
          {
            protection_bestValue: "",
            protection_id: "Extended warranty and insurance",
            protection_price: "$8.50/mo.",
            protection_description: [
              "Maecenas faucibus mollis interdum. Curabitur blandit tempus porttitor.",
            ],
          },
          {
            protection_bestValue: "",
            protection_id: "I don't need protection for my phone",
          },
        ];

        $scope.deviceProtectionURLs = [
          "#non-nyc-plans",
          "#non-nyc-migration",
          "#nyc-migration",
          "#non_nyc_others",
        ];
        $scope.deviceProtectionUrlVariable = 1; // change the value 0/1/2/3 to view diffrent template

        $scope.navigateProtectionplans = function () {
          return $scope.deviceProtectionURLs[
            $scope.deviceProtectionUrlVariable
          ];
        };

        $scope.goBack = function () {
          $window.scrollTo(0, 0);
          $scope.hidden = true;
          $location.path("");
        };
        $scope.manymoreModal = function () {
          document.body.classList.add("noscroll");
          $scope.visible = true;
          $scope.visibleAnimation = true;
        };
        $scope.close = function () {
          document.body.classList.remove("noscroll");
          $scope.visible = false;
          $scope.visibleAnimation = false;
        };

        $scope.safeTitle = "Protection 360";
        $scope.safeArray = [
          {
            contentTitle: "",
            content:
              "Vestibulum at eros. Vivamus sagittis lacus vel augue laoreet rutrum faucibus dolor auctor. Aenean lacinia bibendum nulla sed consectetur.",
          },
          {
            contentTitle: "Etiam porta sem malesuada magna mollis euismod",
            content:
              "Cras mattis consectetur purus sit amet fermentum. Aenean lacinia bibendum nulla sed consectetur. Cras mattis consectetur purus sit amet fermentum.",
          },
          {
            contentTitle: "Maecenas faucibus mollis interdum",
            content:
              "Fusce dapibus, tellus ac cursus commodo, tortor mauris condimentum nibh, ut fermentum massa justo sit amet risus. Nulla vitae elit libero, a pharetra augue. Integer posuere erat a ante venenatis dapibus posuere velit aliquet.",
          },
          {
            contentTitle: "Maecenas faucibus mollis interdum",
            content:
              "Fusce dapibus, tellus ac cursus commodo, tortor mauris condimentum nibh, ut fermentum massa justo sit amet risus. Nulla vitae elit libero, a pharetra augue. Integer posuere erat a ante",
          },
        ];
      },
    ])

    .controller("deviceIntentCtrl", [
      "$scope",
      "$window",
      function ($scope, $window) {
        window.scroll(0, 0);
        $scope.visible = true;
        $scope.hideDueToday = false;
        var alertHeight = document.getElementById("notificationsAlert")
          .offsetHeight;
        var header = document.getElementById("notificationsAlert");
        document.getElementById("mainContentBody").style.paddingTop =
          alertHeight + "px";
        $scope.close = function () {
          $scope.visible = false;
          $scope.myFunction();
          document.getElementById("mainContentBody").style.marginTop =
            -alertHeight + "px";
          return false;
        };

        $scope.myFunction = function () {
          if ($scope.visible) {
            var topHeaderHt = document.getElementById("topHeader").offsetHeight;
            if (window.pageYOffset <= topHeaderHt) {
              document.getElementById("notificationsAlert").style.position =
                "relative";
            } else {
              document.getElementById("notificationsAlert").style.position =
                "fixed";
              document.getElementById("notificationsAlert").style.top = "0px";
            }
          } else {
            document.getElementById("notificationsAlert").style.position =
              "fixed";
            document.getElementById("notificationsAlert").style.top = "0px";
          }
        };
        angular.element($window).bind("resize", function () {
          alertHeight = document.getElementById("notificationsAlert")
            .offsetHeight;

          if ($scope.visible == false) {
            document.getElementById("mainContentBody").style.paddingTop =
              -alertHeight + "px";
          } else {
            document.getElementById("mainContentBody").style.paddingTop =
              alertHeight + "px";
          }
          $scope.$digest();
        });
        window.onscroll = function () {
          $scope.myFunction();
        };
        $scope.deviceIntent = [
          {
            imgUrl: "./img/Phone@3x.png",
            title: "Buy a new phone",
            desc: "",
          },
          {
            imgUrl: "./img/BYOD@3x.png",
            title: "Use my own phone",
            desc: "Some phones may not be compatible.",
          },
          {
            imgUrl: "./img/watch@3x.png",
            title: "Watches, tablets & more",
            desc: "",
          },
        ];

        $scope.hidden = true;
        $scope.planDetails = "";

        $scope.manymoreModal = function (planDetails) {
          $scope.planDetails = "" + planDetails;
          $scope.monthlyPlanModal(planDetails);
          document.body.classList.add("noscroll");
          $scope.visibleLCModal = true;
          $scope.visibleAnimation = true;
        };

        $scope.closeModal = function () {
          document.body.classList.remove("noscroll");
          $scope.visibleLCModal = false;
          $scope.visibleAnimation = false;
        };

        $scope.monthlyList = [
          {
            monthly: "DUE MONTHLY",
            totalAmtMonthly: "20",
            content1: "Additional voice line on your",
            planName: " T-Mobile ONE plan.",
            content2: "While using AutoPay.",
            content3: "Taxes and fees included.",
            planDetails: "",
          },
        ];
        $scope.plan = "Here’s the cost to add a new line:";
        $scope.condition = "Price doesn’t include phone.";
        $scope.terms = "See full plan terms";

        /*Line cost*/

        // comment/uncomment variables for various pages

        // for LineCost_dep and MSG_dep use below variable
        /*$scope.todayList = [{'today': 'DUE TODAY', 'totalAmtToday': '75', 'deposit': '$50.00 deposit', 'kitAmt': '$25.00', 'kit': 'SIM starter kit ', 'details': 'Details'}]; */

        // for LineCost_dep+promo and MSG_dep+promo use below variable
        /*$scope.todayList = [{'today': 'DUE TODAY', 'totalAmtToday': '75', 'deposit': '$50.00 deposit', 'kitAmt': '$25.00', 'kit': 'SIM starter kit ', 'discount': '$25.00 off code IMTEST', 'details': 'Details'}]; */

        // for LineCost_Reg and MSG_Reg use below variable
        /* $scope.todayList = [{'today': 'DUE TODAY', 'totalAmtToday': '25', 'kit': 'SIM starter kit'}]; */

        // for LineCost_dep+InstDisc MSG_dep+InstDisc use below variable
        $scope.todayList = [
          {
            today: "DUE TODAY",
            totalAmtToday: "50",
            deposit: "$50.00 deposit",
            strikeAmt: "$25.00",
            kitAmt: "$0.00",
            kit: "SIM starter kit ",
            discount: "$25.00 instant discount",
            details: "Details",
          },
        ];

        // for LineCost_dep+InstDisc MSG_dep + No InstDisc use below variable
        /* $scope.todayList = [{'today': 'DUE TODAY', 'totalAmtToday': '50', 'deposit': '$50.00 deposit', 'strikeAmt': '$25.00', 'kitAmt': '$0.00', 'kit': 'SIM starter kit ', 'details': 'Details'}];*/

        // for LineCost+InstDisc and MSG_Inst Disc use below variable
        /*$scope.todayList = [{'today': 'DUE TODAY', 'totalAmtToday': '0', 'strikeTodayDue': '$25.00', 'kit': 'SIM starter kit ', 'discount': '$25.00 instant discount'}];*/

        // for LineCost+InstDisc and MSG_IMTEST Disc use below variable
        /*$scope.todayList = [{'today': 'DUE TODAY', 'totalAmtToday': '25', 'strikeTodayDue': '', 'kit': 'SIM starter kit ', 'discount': '$25.00 off code IMTEST'}];*/

        // comment/uncomment variables for various modals

        $scope.monthlyPlanModal = function (planDetails) {
          if (planDetails === "promo") {
            //Modal for T-Mobile ONE Plan
            $scope.modalArray = [
              {
                modalplan: "promo",
                modalTitle: "Modal Promo",
                contentTitle: "",
                content:
                  "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.",
              },
            ];
          } else if (planDetails === "monthly") {
            //Modal for T-Mobile ONE Plan
            $scope.modalArray = [
              {
                modalplan: "monthly",
                modalTitle: "T-Mobile ONE Plan",
                contentTitle: "",
                content:
                  "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.",
              },
            ];
          } else if ("daily") {
            // for ModalBYOD_DEP+SIM use following variable
            $scope.modalArray = [
              {
                modalTitle: "Deposit",
                contentTitle: "Why does my order require a deposit?",
                content:
                  "Based on your credit information, we require a deposit to activate your account with a monthly plan. Unless otherwise required by law, you may request a refund of a deposit after 12 months (with simple interest at the rate required by law) if your account has remained in good standing by calling Customer Care at 1-877-453-1305.",
              },
              {
                modalTitle: "3-in-1 SIM starter kit",
                contentTitle: "",
                content:
                  "The 3-in-1 SIM Starter Kit includes a Nano SIM card with Micro and Standard SIM adapters. For use with unlocked, compatible GSM phones. No SIM card activation required. Once you receive your SIM card in the mail, simply slip it into your phone and you’re ready to go.",
              },
            ];

            // for ModalBYOD_SIM use following variable
            /*$scope.modalArray = [{
						'modalTitle': '3-in-1 SIM starter kit',
						'contentTitle': '',
						'content': 'The 3-in-1 SIM Starter Kit includes a Nano SIM card with Micro and Standard SIM adapters. For use with unlocked, compatible GSM phones. No SIM card activation required. Once you receive your SIM card in the mail, simply slip it into your phone and you’re ready to go.'
					}
					]; */

            // for Modal_SIM use following variable
            /*$scope.modalArray = [{
						'modalTitle': 'SIM starter kit',
						'contentTitle': '',
						'content': 'SIM starter kit automatically added with your device purchase'
					}
					]; */

            // for Modal_dep+SIM use following variable
            /*$scope.modalArray = [{
						'modalTitle': 'Deposit',
						'contentTitle': 'Why does my order require a deposit?',
						'content': 'Based on your credit information, we require a deposit to activate your account with a monthly plan. Unless otherwise required by law, you may request a refund of a deposit after 12 months (with simple interest at the rate required by law) if your account has remained in good standing by calling Customer Care at 1-877-453-1305.'
					},
					{
						'modalTitle': 'SIM starter kit',
						'contentTitle': '',
						'content': 'SIM starter kit automatically added with your device purchase'
					}
                    ];*/
          }
        };
      },
    ])
    .controller("tradeInorNotCtrl", [
      "$scope",
      "$window",
      function ($scope, $window) {
        $scope.hideDueToday = true;
        $scope.tradeChoice = [
          {
            imgUrl: "./img/Money_mobile@2x.png",
            title: "Unlimited data",
            desc:
              "Unlimited 5G data for your tablet. Speeds may vary by country.",
            subtitle: "How trade-in works",
          },
          {
            imgUrl: "./img/Bag_mobile@2x.png",
            title: "No thanks, skip trade-in. ",
            desc: "",
            subtitle: "",
          },
        ];

        $scope.todayList = [
          {
            today: "DUE TODAY",
            totalAmtToday: "25",
            kitAmt: "$25.00",
            kit: "SIM starter kit",
            details: "Details.",
          },
        ];
        $scope.monthlyList = [
          {
            monthly: "DUE MONTHLY",
            totalAmtMonthly: "50",
            content1: "For the additional voice line with AutoPay.",
            content2: "Taxes and fees included.",
          },
        ];
      },
    ])
    // trade in tablet data plan starts
    .controller("tradeInDataPlanCtrl", [
      "$scope",
      "$window",
      function ($scope, $window) {
        $scope.showImage = [];
        $scope.showImage[0] = true;

        $scope.showBoxDetails = function (index, details, key) {
          for (var i = 0; i < $scope.tradeChoice.length; i++) {
            if (i == index) {
              $scope.showImage[i] = true;
              //    $scope.manymoreModal();
            } else {
              $scope.showImage[i] = false;
            }
          }
          //  event.preventDefault();
        };

        $scope.tradeChoice = [
          {
            imgUrl: "./img/Money_mobile@2x.png",
            title: "Unlimited data",
            desc:
              "Unlimited 5G data for your tablet. Speeds may vary by country.",
            subtitle: "",
            valid: true,
            totalAmtToday: "10",
            desc1: "",
            disabled: false,
          },
          {
            imgUrl: "./img/Bag_mobile@2x.png",
            title: "6GB per month",
            desc: "6GB 5G data for your tablet. Speeds may vary by country.",
            subtitle: "",
            totalAmtToday: "10",
            desc1: "",
            disabled: true,
          },
          {
            imgUrl: "./img/Bag_mobile@2x.png",
            title: "2GB per month",
            desc: "2GB 5G data for your tablet. Speeds may vary by country.",
            subtitle: "",
            totalAmtToday: "10",
            desc1: "",
            disabled: false,
          },
        ];

        // modal code starts
        $scope.backTabEvent = function (event, mainId) {
          const allElements = document
            .getElementById(mainId)
            .querySelectorAll(
              'button, [href], input, select, textarea, [tabindex]:not([tabindex="-1"])'
            );
          if (document.activeElement === allElements[0]) {
            if (event.keyCode === 9) {
              if (event.shiftKey) {
                event.preventDefault();
                setTimeout(function () {
                  allElements[allElements.length - 1].focus();
                }, 0);
              }
            }
          }
          if (document.activeElement === document.getElementById(mainId)) {
            if (event.keyCode === 9) {
              if (event.shiftKey) {
                event.preventDefault();
                setTimeout(function () {
                  allElements[allElements.length - 1].focus();
                }, 0);
              }
            }
          }
          if (document.activeElement === allElements[allElements.length - 1]) {
            if (event.keyCode === 9) {
              if (event.shiftKey) {
              } else {
                event.preventDefault();
                setTimeout(function () {
                  document.getElementById(mainId).focus();
                }, 0);
              }
            }
          }
        };

        // ends
        $scope.planDetails = "";
        $scope.manymoreTaxModal = function (planDetails) {
          $scope.monthlyPlanModal(planDetails);
          document.body.classList.add("noscroll");
          $scope.visibleLCModal = true;
          $scope.visibleAnimation1 = true;
          if ($window.innerWidth >= 768) {
            document.getElementById("glueDialogContainerId1").focus();
          }
        };
        $scope.closeTaxModal = function () {
          document.body.classList.remove("noscroll");
          $scope.visibleLCModal = false;
          $scope.visibleAnimation1 = false;
        };

        $scope.monthlyPlanModal = function () {
          $scope.modalArray = [
            {
              modalplan: "promo",
              modalTitle: "Modal Promo",
              contentTitle: "",
              content:
                "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.",
            },
          ];
          //                }
        };
      },
    ])
    // trade in tablet data plan ends
    // trade in plan selector  starts
    .controller("tradePlanSelectorCtrl", [
      "$scope",
      "$http",
      "$window",
      function ($scope, $http, $window) {
        $scope.showImage = [];
        $scope.radioChecked = true;
        // plan selector digital json
        $scope.showImage[0] = true; // default  value for  showImage
        var jsonPathforTradeIn = "";
        //var jsonPathforTradeIn = "json/card_deposit.json";
        //var jsonPathforTradeIn = "json/plan_selector_digitals.json";
        //var jsonPathforTradeIn = "json/plan_selector_standalone.json";
        //var jsonPathforTradeIn = "json/morethan_five_single_GSM.json";
        //var jsonPathforTradeIn = "json/no_phone_line.json";

        $scope.loadJson = function () {
          //variant value 0/1/2/3/4 in localstorage
          if (
            localStorage.getItem("variant") === null ||
            localStorage.getItem("variant") === ""
          ) {
            localStorage.setItem("variant", "0");
            jsonPathforTradeIn = "json/card_deposit.json";
          } else {
            var variant = Number(localStorage.getItem("variant"));
            switch (variant) {
              case 0: {
                jsonPathforTradeIn = "json/card_deposit.json";
                break;
              }
              case 1: {
                jsonPathforTradeIn = "json/plan_selector_digitals.json";
                break;
              }
              case 2: {
                jsonPathforTradeIn = "json/plan_selector_standalone.json";
                break;
              }
              case 3: {
                jsonPathforTradeIn = "json/morethan_five_single_GSM.json";
                break;
              }
              case 4: {
                jsonPathforTradeIn = "json/no_phone_line.json";
                break;
              }
              default: {
                jsonPathforTradeIn = "json/card_deposit.json";
                break;
              }
            }
          }
        };

        $scope.loadJson();

        $http.get(jsonPathforTradeIn).then(function (response) {
          if (response != null && response != undefined)
            $scope.title = response.data.title;
          $scope.tradeChoice = response.data.details;
          $scope.phoneList = response.data.phonelist;
          $scope.phoneTitle = response.data.phoneTitle;
        });

        $scope.heightFunc = function (id1, id2) {
          var divHeight;
          var divHeight1;
          if (document.getElementById(id1) != null) {
            divHeight = document.getElementById(id1).offsetHeight;
          }

          if (document.getElementById(id2) != null) {
            divHeight1 = document.getElementById(id2).offsetHeight;
          }
        };
        $scope.warningHeightFunc = function (id1) {
          var divHeight;
          var divHeight1;

          if (id1 == "tradechoice1" && $window.innerWidth >= 768) {
            divHeight = document.getElementById("tradechoice1").offsetHeight;
            divHeight1 = document.getElementById("tradechoice0").offsetHeight;

            if (divHeight > divHeight1) {
              $scope.dynamicHeight = divHeight1 + "px";
            } else {
              $scope.dynamicHeight = "auto";
            }
          }
        };

        $scope.getleftdivheight0 = function (id1, id2) {
          //      console.log(id1,id2)
          $scope.heightFunc(id1, id2);
          return {
            height: $scope.dynamicHeight,
          };
        };
        $scope.getleftdivheight1 = function (id1, id2) {
          //         console.log(id1,id2)
          $scope.heightFunc(id1, id2);
          return {
            height: $scope.dynamicHeight1,
          };
        };
        $scope.getWarningHeight = function (id1) {
          //   console.log(id1)
          $scope.warningHeightFunc(id1);
          return {
            height: $scope.dynamicHeight,
          };
        };

        angular.element($window).bind("resize", function () {
          for (var i = 0; i < $scope.tradeChoice.length; i++) {
            $scope.getleftdivheight0("currentdiv" + 0, "currentdiv" + i);
            $scope.getleftdivheight1("currentdiv" + 0, "currentdiv" + i);
            $scope.$apply();
          }
          $scope.$apply();
          $scope.$digest();
        });
        // ends

        $scope.showBoxDetails = function (index, details) {
          for (var i = 0; i < $scope.tradeChoice.length; i++) {
            if (i == index) {
              $scope.showImage[i] = true;
            } else {
              $scope.showImage[i] = false;
            }
          }
        };
        // checkbox
        $scope.checkRadio = function (event, i) {
          event.preventDefault();
          if (
            !($scope.phoneList[i].notEligible || $scope.phoneList[i].limitError)
          ) {
            $scope.radioChecked = true;
            document.getElementById("radio" + i).checked = true;
          }
        };
        $scope.planDetails = "";
        $scope.manymoreTaxModal = function (planDetails) {
          $scope.monthlyPlanModal(planDetails);
          document.body.classList.add("noscroll");
          $scope.visibleLCModal = true;
          $scope.visibleAnimation1 = true;
          $scope.monthlyPlanModal(planDetails);
          if ($window.innerWidth >= 768) {
            document.getElementById("glueDialogContainer").focus();
          }
        };

        $scope.closeTaxModal = function () {
          document.body.classList.remove("noscroll");
          $scope.visibleLCModal = false;
          $scope.visibleAnimation1 = false;
        };

        $scope.monthlyPlanModal = function (planDetails) {
          if (planDetails === "promo") {
            //Modal for T-Mobile ONE Plan
            $scope.modalArray = [
              {
                modalplan: "promo",
                modalTitle: "Modal Promo",
                contentTitle: "",
                content:
                  "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.",
              },
            ];
          } else if (planDetails == "deposit") {
            $scope.modalArray = [
              {
                modalTitle: "Deposit",
                contentTitle: "Why does my order require a deposit?",
                content:
                  "Based on your credit information, we require a deposit to activate your account with a monthly plan. Unless otherwise required by law, you may request a refund of a deposit after 12 months (with simple interest at the rate required by law) if your account has remained in good standing by calling Customer Care at 1-877-453-1305.",
              },
            ];
          }
        };

        // modal code starts
        $scope.backTabEvent = function (event, mainId) {
          console.log(mainId);
          const allElements = document
            .getElementById(mainId)
            .querySelectorAll(
              'button, [href], input, select, textarea, [tabindex]:not([tabindex="-1"])'
            );
          if (document.activeElement === allElements[0]) {
            if (event.keyCode === 9) {
              if (event.shiftKey) {
                event.preventDefault();
                setTimeout(function () {
                  allElements[allElements.length - 1].focus();
                }, 0);
              }
            }
          }
          if (document.activeElement === document.getElementById(mainId)) {
            if (event.keyCode === 9) {
              if (event.shiftKey) {
                event.preventDefault();
                setTimeout(function () {
                  allElements[allElements.length - 1].focus();
                }, 0);
              }
            }
          }
          if (document.activeElement === allElements[allElements.length - 1]) {
            if (event.keyCode === 9) {
              if (event.shiftKey) {
              } else {
                event.preventDefault();
                setTimeout(function () {
                  document.getElementById(mainId).focus();
                }, 0);
              }
            }
          }
        };

        $scope.openDigitModal = function (i) {
          //if ($scope.showImage[i] == true) {
          document.body.classList.add("noscroll");
          $scope.visibleRPModal = true;
          $scope.visibleAnimation = true;
          setTimeout(function () {
            document.getElementById("glueDialogContainer").focus();
          }, 0);
          //	}
        };
        $scope.closeModal = function () {
          document.body.classList.remove("noscroll");
          $scope.visibleRPModal = false;
          $scope.visibleAnimation = false;
          document.getElementById("detaillink").focus();
        };
        $scope.holdFocus = function (event) {
          if (event.keyCode === 9 || (event.shiftKey && event.keyCode === 9)) {
            //						event.preventDefault();
            document.getElementById("glueDialogContainer").focus();
            event.preventDefault();
          }
        };
        // ends
      },
    ])
    // trade in plan selector  ends
    .controller("deviceIntentPhase2Ctrl", [
      "$scope",
      "$http",
      "$window",
      function ($scope, $http, $window) {
        window.scroll(0, 0);
        $scope.visible = true;
        $scope.closeicon = true;
        $scope.magentabackground = false;
        $scope.hidden = true;
        $scope.planDetails = "";
        var di_phase2 = "di-phase2";
        var di_phase2_credit1 = "di-phase2-credit1";
        var di_phase2_credit2 = "di-phase2-credit2";
        var di_phase2_soc = "di-phase2-soc";

        var alertHeight = document.getElementById(
          "diviceintent_notificationalert"
        ).offsetHeight;

        document.getElementById("mainContentBody").style.paddingTop =
          alertHeight + "px";
        $scope.close = function () {
          $scope.visible = false;
          $scope.myFunction();
          document.getElementById("mainContentBody").style.marginTop =
            -alertHeight + "px";
          return false;
        };

        $scope.myFunction = function () {
          if ($scope.visible) {
            var topHeaderHt = document.getElementById("topHeader").offsetHeight;
            if (window.pageYOffset <= topHeaderHt) {
              document.getElementById(
                "diviceintent_notificationalert"
              ).style.position = "relative";
            } else {
              document.getElementById(
                "diviceintent_notificationalert"
              ).style.top = "0px";
              document.getElementById(
                "diviceintent_notificationalert"
              ).style.position = "fixed";
            }
          } else {
            document.getElementById(
              "diviceintent_notificationalert"
            ).style.top = "0px";
            document.getElementById(
              "diviceintent_notificationalert"
            ).style.position = "fixed";
          }
        };
        angular.element($window).bind("resize", function () {
          alertHeight = document.getElementById(
            "diviceintent_notificationalert"
          ).offsetHeight;

          if ($scope.visible == false) {
            document.getElementById("mainContentBody").style.paddingTop =
              -alertHeight + "px";
          } else {
            document.getElementById("mainContentBody").style.paddingTop =
              alertHeight + "px";
          }
          $scope.$digest();
        });
        window.onscroll = function () {
          $scope.myFunction();
        };
        $http.get("./json/device-intent.json").then(function (response) {
          $scope.data = response.data;
          //console.log($scope.data);
          $scope.deviceDetails = $scope.data[di_phase2_soc][0];
          //console.log($scope.deviceDetails);
        });
        $scope.deviceIntentPhaseErrorModal = false;
        $scope.openDeviceIntentPhaseErrorModal = function () {
          document.getElementById("DeviceIntentErrorContainer").focus();
          document.body.classList.add("noscroll");
          $scope.deviceIntentPhaseErrorModal = true;
        };
        $scope.closeDeviceIntentPhaseErrorModal = function () {
          document.body.classList.remove("noscroll");
          $scope.deviceIntentPhaseErrorModal = false;
          document.getElementById("cancelBtn").focus();
        };
        $scope.keyPress = function (keyCode) {
          if (keyCode === 27 && $scope.deviceIntentPhaseErrorModal) {
            $scope.deviceIntentPhaseErrorModal = false;
            document.getElementById("cancelBtn").focus();
          }
        };
        $scope.backTabEvent = function (event, mainId, lastId) {
          const allElements = document
            .getElementById(mainId)
            .querySelectorAll(
              'button, [href], input, select, textarea, [tabindex]:not([tabindex="-1"])'
            );
          if (document.activeElement === allElements[0]) {
            if (event.keyCode === 9) {
              if (event.shiftKey) {
                event.preventDefault();
                setTimeout(function () {
                  // allElements[allElements.length - 1].focus();
                  document.getElementById(lastId).focus();
                }, 0);
              }
            }
          }
          if (document.activeElement === document.getElementById(mainId)) {
            if (event.keyCode === 9) {
              if (event.shiftKey) {
                event.preventDefault();
                setTimeout(function () {
                  allElements[allElements.length - 1].focus();
                  //     document.getElementById(lastId).focus();
                }, 0);
              }
            }
          }
          if (document.activeElement === allElements[allElements.length - 1]) {
            if (event.keyCode === 9) {
              if (event.shiftKey) {
              } else {
                event.preventDefault();
                setTimeout(function () {
                  //   document.getElementById(mainId).focus();
                  document.getElementById(mainId).focus();
                }, 0);
              }
            }
          }
        };
      },
    ])

    .controller("tradeInConditionUpdatedCtrl", [
      "$scope",
      "$http",
      "$window",
      "$location",
      "$timeout",
      function ($scope, $http, $window, $location, $timeout) {
        $scope.valueSelected = false;

        var cookie;
        $scope.answersArray = [];
        $scope.answersArray1 = [];
        var jsonPathforTradeIn = "json/tradeInUpdated.json";
        $scope.title = "Tell us about the condition of your old device:";

        $http.get(jsonPathforTradeIn).then(function (response) {
          if (response != null && response != undefined)
            $scope.tradeinUpdatedDetails = response.data.details;
        });

        $scope.retainSelection = function (value, count) {
          $scope.object1 = {
            elementid: value,
            index: count,
          };
          $scope.answersArray.push(count);
          for (let i = 0; i < $scope.answersArray.length; i++) {
            if ($scope.answersArray1.indexOf($scope.answersArray[i]) === -1) {
              $scope.answersArray1.push($scope.answersArray[i]);
            }
          }
          if ($scope.answersArray1.length === 4) {
            document.getElementById("continueBtn").classList.remove("disabled");
            document
              .getElementById("continueBtn")
              .setAttribute("aria-label", "click to continue");
          } else {
            document
              .getElementById("continueBtn")
              .setAttribute(
                "aria-label",
                "continue button will be enabled once all the questions are answered"
              );
          }
          document
            .getElementById($scope.object1.elementid)
            .classList.add("activeClass");
          $scope.idSelected = $scope.object1.elementid.substring(
            3,
            $scope.object1.elementid.length - 1
          );
          document
            .getElementById($scope.object1.elementid)
            .setAttribute("aria-label", $scope.idSelected + " option selected");

          if ($scope.object1.elementid.substring(3, 5) === "No") {
            if (
              document
                .getElementById("btnYes" + count)
                .classList.contains("activeClass")
            ) {
              document
                .getElementById("btnYes" + count)
                .classList.remove("activeClass");
              document
                .getElementById("btnYes" + count)
                .setAttribute("aria-label", "Yes");
            }
          } else {
            if (
              document
                .getElementById("btnNo" + count)
                .classList.contains("activeClass")
            ) {
              document
                .getElementById("btnNo" + count)
                .classList.remove("activeClass");
              document
                .getElementById("btnNo" + count)
                .setAttribute("aria-label", "No");
            }
          }
        };
        $scope.backTabEvent = function (event, mainId) {
          console.log(mainId);
          const allElements = document
            .getElementById(mainId)
            .querySelectorAll(
              'button, [href], input, select, textarea, [tabindex]:not([tabindex="-1"])'
            );
          if (document.activeElement === allElements[0]) {
            if (event.keyCode === 9) {
              if (event.shiftKey) {
                event.preventDefault();
                setTimeout(function () {
                  allElements[allElements.length - 1].focus();
                }, 0);
              }
            }
          }
          if (document.activeElement === document.getElementById(mainId)) {
            if (event.keyCode === 9) {
              if (event.shiftKey) {
                event.preventDefault();
                setTimeout(function () {
                  allElements[allElements.length - 1].focus();
                }, 0);
              }
            }
          }
          if (document.activeElement === allElements[allElements.length - 1]) {
            if (event.keyCode === 9) {
              if (event.shiftKey) {
              } else {
                event.preventDefault();
                setTimeout(function () {
                  document.getElementById(mainId).focus();
                }, 0);
              }
            }
          }
        };

        $scope.close = function () {
          document.body.classList.remove("noscroll");
          $scope.visibleAnimation = false;
          //console.log("$scope.lastElemId :::: "+$scope.lastElemId);
          document.getElementById("" + $scope.lastElemId).focus();
        };

        $scope.keyPress = function (keyCode) {
          if (keyCode === 27 && $scope.visibleAnimation) {
            $scope.visibleAnimation = false;
            document.getElementById("modalLink").focus();
          }
          if (
            keyCode === 27 &&
            $scope.visibleAnimation &&
            $scope.visibleLCModal
          ) {
            $scope.visibleAnimation = false;
            $scope.visibleLCModal = false;
            document.getElementById("deviceLink").focus();
          }
        };

        /*   $scope.$apply(function () {
            for (i = 0; i <  $scope.answersArray.length; i++) {
                cookie =  $scope.answersArray[i];
                alert(cookie)
            $scope.value=active;
            document.getElementById(value).active = true;
            }
        }); */

        $scope.phoneModal = function (title, event) {
          event.preventDefault();
          //console.log("id phone "+ event.target.id);
          $scope.lastElemId = event.target.id;
          document.getElementById("close-button").focus();
          document.body.classList.add("noscroll");
          $scope.returndeviceModal(title);
          $scope.visible = true;
          $scope.visibleAnimation = true;
        };
        /*$scope.deviceModal=function ($event,deviceDetails) {
            //  $scope.deviceplanDetails = "" + deviceDetails;
            
             if($event.keyCode==13){
              event.preventDefault();
              console.log("deviceDetails :"+ deviceDetails)
              document.body.classList.add("noscroll");
              $scope.visibleLCModal = true;
              $scope.visible = true;
              $scope.visibleAnimation = true;
              $scope.returndeviceModal(deviceDetails);
            
             
          }              
       
      }*/
        $scope.deviceModal = function (deviceDetails, event) {
          //  $scope.deviceplanDetails = "" + deviceDetails;
          //console.log("id device "+ event.target.id);
          event.preventDefault();
          $scope.lastElemId = event.target.id;
          document.getElementById("close-button").focus();
          document.body.classList.add("noscroll");
          $scope.visibleLCModal = true;
          $scope.visible = true;
          $scope.visibleAnimation = true;
          $scope.returndeviceModal(deviceDetails);
          event.preventDefault();
        };
        $scope.returndeviceModal = function (deviceDetails) {
          //console.log(deviceDetails)
          if (deviceDetails == 0) {
            //Modal for T-Mobile ONE Plan
            $scope.phoneTitle = "";
            $scope.phoneArray = [
              {
                ques: "",
                answer:
                  'Before returning your device disable all security features such as <a tabindex="0" href="javascript:void(0)" class="text-decoration-underline">Find my iPhone</a> and <a tabindex="0" href="javascript:void(0)" class="text-decoration-underline"> Android Anti-theft</a>.',
              },
              {
                ques: "",
                answer:
                  "The simplest way to disable these feature is to perform a factory master reset from the settings menu.",
              },
              {
                ques: "",
                answer:
                  "Important! Performing a factory reset will erase all content, so verify backups have completed prior to taking this step",
              },
            ];
          } else if (deviceDetails == 1) {
            //Modal for T-Mobile ONE Plan
            $scope.phoneTitle = "";
            $scope.phoneArray = [
              {
                ques: "",
                answer:
                  "A device with screen damage may be eligible for a reduced trade-in credit but will not qualify for for promotions",
              },
            ];
          } else {
            $scope.phoneTitle = "Phone condition";
            $scope.phoneArray = [
              {
                quesn:
                  "Curabitur blandit tempus porttitor. Morbi leo risus, porta ac consectetur ac?",
                answer:
                  "Vestibulum at eros. Vivamus sagittis lacus vel augue laoreet rutrum faucibus dolor auctor. Aenean lacinia bibendum nulla sed consectetur.",
              },
              {
                quesn: "Etiam porta sem malesuada magna mollis euismod? ",
                answer:
                  "Cras mattis consectetur purus sit amet fermentum. Aenean lacinia bibendum nulla sed consectetur. Cras mattis consectetur purus sit amet fermentum.",
              },
              {
                quesn: "Maecenas faucibus mollis interdum?",
                answer:
                  "Fusce dapibus, tellus ac cursus commodo, tortor mauris condimentum nibh, ut fermentum massa justo sit amet risus. Nulla vitae elit libero, a pharetra augue. Integer posuere erat a ante venenatis dapibus posuere velit aliquet.",
              },
            ];
          }
        };
        /*$scope.phoneModal = function (title) {
          $scope.phoneModalVisible = !$scope.phoneModalVisible;
          if ($scope.pricingModalVisible) {
            document.getElementById("phoneModalBody").focus();
            document.getElementsByTagName('body')[0].classList.add('noScroll');
            $scope.modalInvokedFrom = title;
          }
          else {
            document.getElementsByTagName('body')[0].classList.remove('noScroll');
            if (!$scope.mobile) {
              document.getElementById($scope.modalInvokedFrom).focus();
            }
          }
        }*/
        $scope.goBack = function () {
          $window.scrollTo(0, 0);
          if ($location.$$url == "/trade-value") {
            $location.path("");
          }
        };
      },
    ])
    .controller("linecostController", [
      "$scope",
      "$http",
      "$window",
      function ($scope, $http, $window) {
        $scope.plan = "Digits Apple Watch plan";
        $scope.planDescription =
          "Get calls and texts right on your watch, even if you leave your phone at home";
        $scope.planLink = "DIGITS Apple Watch.";
        $scope.condition = "Price doesn’t include phone.";
        $scope.terms = "See full plan terms";
        $scope.todayList = [
          {
            today: "DUE TODAY",
            totalAmtToday: "50",
            deposit: "Deposit",
          },
        ];
        $scope.monthlyList = [
          {
            monthly: "DUE MONTHLY",
            totalAmtMonthly: "10",
            content2: "While using AutoPay.",
            content3: "Taxes and fees included.",
            planDetails: "",
          },
        ];
        $scope.hideDueToday = true;
        $scope.phoneList = [
          {
            name: "Eric",
            phone: "(206) 555-1111",
            notEligible: false,
            limitError: true,
            message:
              "This line has reached the five-device limit. Please choose another line or contact us",
          },
          {
            name: "Bob",
            phone: "(206) 555-2222",
            notEligible: true,
            limitError: false,
            message:
              "This line is ineligible. Please choose another line or contact us",
          },
          {
            name: "Natasha",
            phone: "(206) 555-3333",
            notEligible: false,
            limitError: false,
            message: "",
          },
          {
            name: "John",
            phone: "(206) 555-3333",
            notEligible: false,
            limitError: false,
            message: "",
          },
        ];
        $scope.visibleLCModal = false;
        $scope.radioChecked1 = false;
        $scope.visibleDigitErrorModal = false;
        $scope.openDigitErrorModal = function () {
          document.getElementById("DigitErrorContainer").focus();
          document.body.classList.add("noscroll");
          $scope.visibleDigitErrorModal = true;
        };
        $scope.closeDigitErrorModal = function () {
          document.body.classList.remove("noscroll");
          $scope.visibleDigitErrorModal = false;
          document.getElementById("digiterrormd").focus();
        };

        $scope.openModal = function () {
          document.getElementById("glueDialogContainer").focus();
          document.body.classList.add("noscroll");
          $scope.visibleLCModal = true;
        };
        $scope.closeModal = function () {
          document.body.classList.remove("noscroll");
          $scope.visibleLCModal = false;
          document.getElementById("planlink").focus();
        };

        $scope.checkRadio = function (event, i) {
          event.preventDefault();
          if (
            !($scope.phoneList[i].notEligible || $scope.phoneList[i].limitError)
          ) {
            $scope.radioChecked1 = true;
            document.getElementById("radio" + i).checked = true;
          }
        };
        $scope.holdFocus = function (event) {
          if (event.keyCode === 9) {
            document.getElementById("glueDialogContainer").focus();
          }
        };
        $scope.close = function (event) {
          if (event.keyCode === 27) {
            this.closeModal();
            this.closeDigitErrorModal();
          }
        };
        $scope.backTabEvent = function (event, mainId, lastId) {
          const allElements = document
            .getElementById(mainId)
            .querySelectorAll(
              'button, [href], input, select, textarea, [tabindex]:not([tabindex="-1"])'
            );
          if (document.activeElement === allElements[0]) {
            if (event.keyCode === 9) {
              if (event.shiftKey) {
                event.preventDefault();
                setTimeout(function () {
                  document.getElementById(lastId).focus();
                }, 0);
              }
            }
          }
          if (document.activeElement === document.getElementById(mainId)) {
            if (event.keyCode === 9) {
              if (event.shiftKey) {
                event.preventDefault();
                setTimeout(function () {
                  document.getElementById(lastId).focus();
                }, 0);
              }
            }
          }
          if (document.activeElement === document.getElementById(lastId)) {
            if (event.keyCode === 9) {
              if (event.shiftKey) {
              } else {
                event.preventDefault();
                setTimeout(function () {
                  document.getElementById(mainId).focus();
                }, 0);
              }
            }
          }
        };
      },
    ])
    .controller("eligibleDeviceCtrl", [
      "$scope",
      "$http",
      "$window",
      function ($scope, $http, $window) {
        $scope.title = "Which device do you want to upgrade?";
        $scope.description = "Only eligible devices are shown.";
        $scope.viewChange = function () {
          if ($window.innerWidth >= 768) {
            $scope.isDesktop = true;
          } else {
            $scope.isDesktop = false;
          }
        };

        $scope.details = [
          {
            name: "Vinny",
            number: "(206) 555-1111",
            manufacture: "Apple",
            phoneModel: "iPad",
            image: "./img/square_boundingbox.png",
          },
          {
            name: "Chris",
            number: "(555) 555-1234",
            manufacture: "Apple",
            phoneModel: "iPad",
            image: "./img/square_boundingbox.png",
          },
          {
            name: "David",
            number: "(555) 555-1234",
            manufacture: "Apple",
            phoneModel: "iPad",
            image: "./img/square_boundingbox.png",
          },
        ];
      },
    ])
    .controller("deviceIntentNewCtrl", [
      "$scope",
      "$http",
      "$window",
      "$timeout",
      "$location",
      function ($scope, $http, $window, $timeout, $location) {
        window.scroll(0, 0);
        $scope.visible = true;
        $scope.closeicon = true;

        $scope.hidden = true;
        $scope.planDetails = "";
        var di_intent_new = "di-intent-new";
        //var di_intent_new_d="di-intent-new-d";
        var alertHeight = document.getElementById(
          "diviceintent_notificationalert"
        ).offsetHeight;

        $scope.hideDeviceIntent = false;
        $scope.hideContent = true;

        $scope.goToNext = function (val, warn) {
          console.log("warn " + warn);
          if ((val === "phone" || val === "tablet") && warn === "") {
            $timeout(function () {
              $scope.hideDeviceIntent = true;
              $scope.hideContent = false;
              $location.path("/" + val);
              document.getElementById("contentPage").style.animation =
                "fadeInEffect 2s";
              document.getElementById("contentPage").style.display = "block";
              document.getElementById("deviceIntent").style.display = "none";
              if (val === "phone") {
                $scope.contentData = $scope.data["di-intent-new-st2-phone"][0];
              } else if (val === "tablet") {
                $scope.contentData = $scope.data["di-intent-new-st2-tab"][0];
              }
            }, 300);
          }
        };
        $scope.goBack = function () {
          $window.scrollTo(0, 0);
          // if ($location.$$url == "'/' + url") {
          $location.path("");
          $scope.hideDeviceIntent = false;
          $scope.hideContent = true;
          document.getElementById("deviceIntent").style.display = "block";
          document.getElementById("deviceIntent").style.animation =
            "fadeInEffect 2s";
          document.getElementById("contentPage").style.animation =
            "fadeInEffect 2s";
          document.getElementById("contentPage").style.display = "none";
          // }
        };

        document.getElementById("mainContentBody").style.paddingTop =
          alertHeight + "px";
        $scope.close = function () {
          $scope.visible = false;
          $scope.myFunction();
          document.getElementById("mainContentBody").style.marginTop =
            -alertHeight + "px";
          return false;
        };

        $scope.myFunction = function () {
          if ($scope.visible) {
            var topHeaderHt = document.getElementById("topHeader").offsetHeight;
            if (window.pageYOffset <= topHeaderHt) {
              document.getElementById(
                "diviceintent_notificationalert"
              ).style.position = "relative";
            } else {
              document.getElementById(
                "diviceintent_notificationalert"
              ).style.top = "0px";
              document.getElementById(
                "diviceintent_notificationalert"
              ).style.position = "fixed";
            }
          } else {
            document.getElementById(
              "diviceintent_notificationalert"
            ).style.top = "0px";
            document.getElementById(
              "diviceintent_notificationalert"
            ).style.position = "fixed";
          }
        };
        angular.element($window).bind("resize", function () {
          alertHeight = document.getElementById(
            "diviceintent_notificationalert"
          ).offsetHeight;

          if ($scope.visible == false) {
            document.getElementById("mainContentBody").style.paddingTop =
              -alertHeight + "px";
          } else {
            document.getElementById("mainContentBody").style.paddingTop =
              alertHeight + "px";
          }
          $scope.$digest();
        });
        window.onscroll = function () {
          $scope.myFunction();
        };
        $http.get("./json/device-intent.json").then(function (response) {
          $scope.data = response.data;
          //console.log($scope.data);
          $scope.deviceDetails = $scope.data[di_intent_new][0];
          //console.log($scope.deviceDetails);
        });

        $scope.deviceIntentPhaseErrorModal = false;
        $scope.openDeviceIntentPhaseErrorModal = function () {
          document.getElementById("DeviceIntentErrorContainer").focus();
          document.body.classList.add("noscroll");
          $scope.deviceIntentPhaseErrorModal = true;
        };
        $scope.closeDeviceIntentPhaseErrorModal = function () {
          document.body.classList.remove("noscroll");
          $scope.deviceIntentPhaseErrorModal = false;
          document.getElementById("cancelBtn").focus();
        };
        $scope.keyPress = function (keyCode) {
          if (keyCode === 27 && $scope.deviceIntentPhaseErrorModal) {
            $scope.deviceIntentPhaseErrorModal = false;
            document.getElementById("cancelBtn").focus();
          }
        };
        $scope.backTabEvent = function (event, mainId, lastId) {
          const allElements = document
            .getElementById(mainId)
            .querySelectorAll(
              'button, [href], input, select, textarea, [tabindex]:not([tabindex="-1"])'
            );
          if (document.activeElement === allElements[0]) {
            if (event.keyCode === 9) {
              if (event.shiftKey) {
                event.preventDefault();
                setTimeout(function () {
                  // allElements[allElements.length - 1].focus();
                  document.getElementById(lastId).focus();
                }, 0);
              }
            }
          }
          if (document.activeElement === document.getElementById(mainId)) {
            if (event.keyCode === 9) {
              if (event.shiftKey) {
                event.preventDefault();
                setTimeout(function () {
                  allElements[allElements.length - 1].focus();
                  //     document.getElementById(lastId).focus();
                }, 0);
              }
            }
          }
          if (document.activeElement === allElements[allElements.length - 1]) {
            if (event.keyCode === 9) {
              if (event.shiftKey) {
              } else {
                event.preventDefault();
                setTimeout(function () {
                  //   document.getElementById(mainId).focus();
                  document.getElementById(mainId).focus();
                }, 0);
              }
            }
          }
        };
      },
    ])
    // Rate plan migrtation starts
    .controller("ratePlanCtrl", [
      "$scope",
      "$window",
      "$http",
      function ($scope, $window) {
        window.scroll(0, 0);
        $scope.visible = true;
        $scope.hideDueToday = false;
        var alertHeight = document.getElementById("notificationsAlert")
          .offsetHeight;
        var header = document.getElementById("notificationsAlert");
        document.getElementById("mainContentBody").style.paddingTop =
          alertHeight + "px";
        $scope.close = function () {
          $scope.visible = false;
          $scope.myFunction();
          document.getElementById("mainContentBody").style.marginTop =
            -alertHeight + "px";
          return false;
        };
        var acc = document.getElementsByClassName("accordion");
        var count = 0;
        $scope.init = function () {
          if (acc && count == 0) {
            count++;
            setTimeout(function () {
              acc[0].classList.add("active");
              var panel = acc[0].nextElementSibling;
              panel.style.maxHeight = panel.scrollHeight + "px";
            }, 1000);
          }
        };
        $scope.toggle = function (index) {
          acc[index].classList.toggle("active");
          var panel = acc[index].nextElementSibling;
          if (panel.style.maxHeight) {
            panel.style.maxHeight = null;
          } else {
            panel.style.maxHeight = panel.scrollHeight + "px";
          }
        };

        $scope.myFunction = function () {
          if ($scope.visible) {
            var topHeaderHt = document.getElementById("topHeader").offsetHeight;
            if (window.pageYOffset <= topHeaderHt) {
              document.getElementById("notificationsAlert").style.position =
                "relative";
            } else {
              document.getElementById("notificationsAlert").style.position =
                "fixed";
              document.getElementById("notificationsAlert").style.top = "0px";
            }
          } else {
            document.getElementById("notificationsAlert").style.position =
              "fixed";
            document.getElementById("notificationsAlert").style.top = "0px";
          }
        };
        angular.element($window).bind("resize", function () {
          alertHeight = document.getElementById("notificationsAlert")
            .offsetHeight;
          if ($scope.visible == false) {
            document.getElementById("mainContentBody").style.paddingTop =
              -alertHeight + "px";
          } else {
            document.getElementById("mainContentBody").style.paddingTop =
              alertHeight + "px";
          }
          for (var i = 0; i < acc.length; i++) {
            var panel = acc[i].nextElementSibling;
            if (panel.style.maxHeight) {
              panel.style.maxHeight = panel.scrollHeight + "px";
            }
          }
          $scope.$digest();
        });
        window.onscroll = function () {
          $scope.myFunction();
        };

        $scope.hidden = true;
        $scope.plan = "Here’s the cost to add a new line:";
        $scope.detailsModal = function (event) {
          $scope.modalArray = [
            {
              modalTitle: "Deposit",
              contentTitle: "Why does my order require a deposit?",
              content:
                "Based on your credit information, we require a deposit to activate your account with a monthly plan. Unless otherwise required by law, you may request a refund of a deposit after 12 months (with simple interest at the rate required by law) if your account has remained in good standing by calling Customer Care at 1-877-453-1305.",
            },
            {
              modalTitle: "3-in-1 SIM starter kit",
              contentTitle: "",
              content:
                "The 3-in-1 SIM Starter Kit includes a Nano SIM card with Micro and Standard SIM adapters. For use with unlocked, compatible GSM phones. No SIM card activation required. Once you receive your SIM card in the mail, simply slip it into your phone and you’re ready to go.",
            },
          ];
          document.body.classList.add("noscroll");
          $scope.visibleRPModal = true;
          $scope.visibleAnimation = true;
          setTimeout(function () {
            document.getElementById("glueDialogContainer").focus();
          }, 0);
        };
        $scope.closeModal = function () {
          document.body.classList.remove("noscroll");
          $scope.visibleRPModal = false;
          $scope.visibleAnimation = false;
          document.getElementById("detaillink").focus();
        };
        $scope.holdFocus = function (event) {
          if (event.keyCode === 9 || (event.shiftKey && event.keyCode === 9)) {
            event.preventDefault();
            document.getElementById("glueDialogContainer").focus();
          }
        };
        $scope.todayList = [
          {
            today: "DUE TODAY",
            totalAmtToday: "25",
            kitAmt: "$25.00",
            kit: "SIM starter kit ",
            details: "Details.",
          },
        ];
        $scope.monthlyList = [
          {
            monthly: "DUE MONTHLY",
            totalAmtMonthly: "50",
            content1: "For the additional voice line with AutoPay",
            content2: "Taxes and fees included.",
          },
        ];
        $scope.listArray = [
          {
            listTitle: "Plan details",
            info1:
              "The Magenta plan provides unlimited talk, text, and high-speed data with all the taxes and fees included in the monthly charge. The price you see is inclusive of all these great benefits:",
            benefitsDetails: [
              {
                planInfo:
                  "Watch all your favorite TV shows and movies with a <b>Basic Netflix subscription on us when you have 2 or more lines.</b> You can upgrade to other Netflix plans for an additional charge.",
              },
              {
                planInfo:
                  "<b>Texting and unlimited data</b> at up to 2G speeds when traveling abroad in 210+ countries and destinations.",
              },
              {
                planInfo:
                  "<b>Unlimited texting and 1 hour of data</b> included on Gogo-enabled flights.",
              },
              {
                planInfo:
                  "<b>3GB of 4G LTE Mobile Hotspot</b> and then unlimited 3G/ 600 kbps after that.",
              },
              {
                planInfo:
                  "<b>Unlimited calling, texting, and data in  Mexico and Canada</b> with up to 5GB of 4G LTE.",
              },
              {
                planInfo:
                  "<b>Use AutoPay to get $5 off each line</b> of service up to 8 lines.",
              },
              {
                planInfo:
                  "Video typically streams on smartphone/tablet at DVD quality (480p) unless you add an HD Day Pass or PlusUp.",
              },
            ],
            info2:
              "On all T-Mobile plans, during congestion, the small fraction of customers using >50GB/mo. may notice reduced speeds until next bill cycle due to data prioritization.On all T-Mobile plans, during congestion, the small fraction of customers using >50GB/mo. may notice reduced speeds until next bill cycle due to data prioritization.On all T-Mobile plans, during congestion, the small fraction of customers using >50GB/mo. may notice reduced speeds until next bill cycle due to data prioritization.On all T-Mobile plans, during congestion, the small fraction of customers using >50GB/mo. may notice reduced speeds until next bill cycle due to data prioritization.",
          },
          {
            listTitle: "Plan updates",
            updateTitle:
              "The following items will be removed from your account.",
            info2:
              "You may still be eligible for some of these features—you’ll just need to add them to your account again once the new line has been added.",
            updateDetail: [
              {
                planIcon: "./img/alert-outline.svg",
                planDetail: "T-Mobile ONE",
              },
              {
                planIcon: "./img/alert-outline.svg",
                planDetail: "Family Discount",
              },
              {
                planIcon: "./img/alert-outline.svg",
                planDetail:
                  "Netflix Premium $15.99 with Fam Allowances ($21 value)",
              },
            ],
          },
        ];
      },
    ])
    // Rate plan migration ends
    // Rate plan change starts
    .controller("ratePlanChangeCtrl", [
      "$scope",
      "$window",
      "$http",
      function ($scope, $window) {
        window.scroll(0, 0);
        $scope.visible = true;
        $scope.hideDueToday = false;
        $scope.sticktop = false;
        var alertHeight = document.getElementById("notificationsAlert")
          .offsetHeight;
        var header = document.getElementById("notificationsAlert");
        document.getElementById("mainContentBody").style.paddingTop =
          alertHeight + "px";

        $scope.close = function () {
          $scope.visible = false;
          $scope.myFunction();
          document.getElementById("mainContentBody").style.paddingTop = 0;
          return false;
        };
        var acc = document.getElementsByClassName("accordion");
        var count = 0;
        $scope.init = function () {
          $scope.myFunction();
          if (acc && count == 0) {
            count++;
          }
        };

        $scope.myFunction = function () {
          var notificationheight = 0;
          if ($scope.visible) {
            notificationheight = document
              .getElementById("notificationsAlert")
              .getBoundingClientRect().height;
            var topHeaderHt = document.getElementById("topHeader").offsetHeight;
            if (window.pageYOffset <= topHeaderHt) {
              document.getElementById("notificationsAlert").style.position =
                "relative";
            } else {
              document.getElementById("notificationsAlert").style.position =
                "fixed";
              document.getElementById("notificationsAlert").style.top = "0px";
            }
          } else {
            document.getElementById("notificationsAlert").style.position =
              "fixed";
            document.getElementById("notificationsAlert").style.top = "0px";
          }

          var header = document.getElementById("plan-content");
          var sticky = header.offsetTop;
          var consolidateHt = sticky - notificationheight;
          if (window.pageYOffset <= consolidateHt + 20) {
            $scope.sticktop = false;
            document.getElementById("fixed-content").style.position =
              "relative";
            document.getElementById("fixed-content").style.height = "0px";
            document.getElementById("fixed-content").style.top = "0px";
            if (
              document
                .getElementById("fixed-content")
                .classList.contains("sticky-top")
            ) {
              document
                .getElementById("fixed-content")
                .classList.remove("sticky-top");
            }
          } else {
            $scope.sticktop = true;
            document.getElementById("fixed-content").style.position = "fixed";
            document.getElementById("fixed-content").style.top =
              -30 + notificationheight + "px";
            document.getElementById("fixed-content").style.height = "48px";
            if (
              document
                .getElementById("fixed-content")
                .classList.contains("sticky-top")
            ) {
            } else {
              document
                .getElementById("fixed-content")
                .classList.add("sticky-top");
            }
          }
        };

        angular.element($window).bind("resize", function () {
          alertHeight = document.getElementById("notificationsAlert")
            .offsetHeight;
          if ($scope.visible == false) {
            document.getElementById("mainContentBody").style.paddingTop =
              -alertHeight + "px";
          } else {
            document.getElementById("mainContentBody").style.paddingTop =
              alertHeight + "px";
          }
          $scope.myFunction();
          setTimeout(function () {
            for (var i = 0; i < $scope.numLimit; i++) {
              $scope.getrightdivheight("newdiv" + i, "currentdiv" + i, i);
              $scope.getleftdivheight("newdiv" + i, "currentdiv" + i, i);
              $scope.$apply();
            }
          }, 300);
          $scope.$apply();
          $scope.$digest();
        });

        window.onscroll = function () {
          $scope.myFunction();
        };
        $scope.hidden = true;
        $scope.plan = "Here’s the cost to add a new line:";
        $scope.detailsModal = function (event) {
          $scope.modalArray = [
            {
              modalTitle: "Deposit",
              contentTitle: "Why does my order require a deposit?",
              content:
                "Based on your credit information, we require a deposit to activate your account with a monthly plan. Unless otherwise required by law, you may request a refund of a deposit after 12 months (with simple interest at the rate required by law) if your account has remained in good standing by calling Customer Care at 1-877-453-1305.",
            },
            {
              modalTitle: "3-in-1 SIM starter kit",
              contentTitle: "",
              content:
                "The 3-in-1 SIM Starter Kit includes a Nano SIM card with Micro and Standard SIM adapters. For use with unlocked, compatible GSM phones. No SIM card activation required. Once you receive your SIM card in the mail, simply slip it into your phone and you’re ready to go.",
            },
          ];
          document.body.classList.add("noscroll");
          $scope.visibleRPModal = true;
          $scope.visibleAnimation = true;
          document.getElementById("glueDialogContainerId").focus();
        };
        $scope.closeModal = function () {
          document.body.classList.remove("noscroll");
          $scope.visibleRPModal = false;
          $scope.visibleAnimation = false;
          document.getElementById("detaillink").focus();
        };
        $scope.holdFocus = function (event) {
          if (event.keyCode === 9 || (event.shiftKey && event.keyCode === 9)) {
            event.preventDefault();
            document.getElementById("glueDialogContainerId").focus();
          }
        };

        $scope.leftListDetails = [
          {
            amount: "$67.23",
            lines: "for 1 voice line.",
            content: " with AutoPay. Taxes & Fees included.",
          },
          {
            title: "Talk & Text",
            value: "Unlimited",
          },
          {
            title: "Data",
            value: "Unlimited 4G LTE",
          },
          {
            title: "Netflix on Us",
            value: "Standard, 2 screens (HD) for $2/mo",
          },
          {
            title: "Mobile Hotspot",
            value: "Included",
          },
          {
            title: "No annual service contract",
            value: "true",
          },
          {
            title: "Quibi with T-Mobile",
            value: "true",
          },
          {
            title: "HD video streaming",
            value: "Optional with an HD Day Pass or PlusUp Data.",
          },
          {
            title: "Optimized video streaming",
            value: "true",
          },
          {
            title: "Unlimited international texting from home",
            value: "true",
          },
          {
            title: "Simple Global",
            value: "Texting and 2G (128kbsp) data speeds in 210+ destinations.",
          },
          {
            title: "Overseas voice calling",
            value: "$0.25/minute in Simple Global Countries.",
          },
          {
            title: "Use your device in Mexico and Canada",
            value: "Unlimited talk and text, 5GB of 4G LTE data included.",
          },
          {
            title: "Gogo in-flight benefits",
            value: "In-flight texting and 1 hour of Wi-Fi. ",
          },
          {
            title: "Wi-Fi calling",
            value: "true",
          },
          {
            title: "Family Allowances",
            value: "true",
          },
          {
            title: "AutoPay discount",
            value: "$5 discount per line up to 8 lines with AutoPay enabled.",
          },
        ];
        $scope.rightListDetails = [
          {
            amount: "$120.00",
            lines: "for 2 voice lines.",
            content: " with AutoPay. Taxes & Fees included.",
          },
          {
            title: "Talk & Text",
            value: "Unlimited",
          },
          {
            title: "Data",
            value: "Unlimited 4G LTE",
          },
          {
            title: "Netflix on Us",
            value: "Basic, 1 screen (SD) ",
          },
          {
            title: "Mobile Hotspot",
            value: "Included",
          },
          {
            title: "No annual service contract",
            value: "true",
          },
          {
            title: "Quibi with T-Mobile",
            value: "true",
          },
          {
            title: "HD video streaming",
            value: "Optional with an HD Day Pass or PlusUp Data.",
          },
          {
            title: "Optimized video streaming",
            value: "true",
          },
          {
            title: "Unlimited international texting from home",
            value: "true",
          },
          {
            title: "Simple Global",
            value: "Texting and 2G (128kbsp) data speeds in 210+ destinations.",
          },
          {
            title: "Overseas voice calling",
            value: "$0.25/minute in Simple Global Countries.",
          },
          {
            title: "Use your device in Mexico and Canada",
            value: "Unlimited talk and text, 5GB of 4G LTE data included.",
          },
          {
            title: "Gogo in-flight benefits",
            value: "In-flight texting and 1 hour of Wi-Fi.",
          },
          {
            title: "Wi-Fi calling",
            value: "true",
          },
          {
            title: "Family Allowances",
            value: "true",
          },
          {
            title: "AutoPay discount",
            value: "$5 discount per line up to 8 lines with AutoPay enabled.",
          },
        ];
        $scope.numLimit = 5;
        $scope.todayList = [
          {
            today: "DUE TODAY",
            totalAmtToday: "25",
            kitAmt: "$25.00",
            kit: "SIM starter kit",
            details: "Details.",
          },
        ];
        $scope.monthlyList = [
          {
            monthly: "DUE MONTHLY",
            totalAmtMonthly: "50",
            content1: "For the additional voice line with AutoPay.",
            content2: "Taxes and fees included.",
          },
        ];
        $scope.focusShowBenefits = function (event) {
          if (event.keyCode == 13) {
            $scope.showadditionalbenibits();
          }
        };
        $scope.focusHideBenefits = function (event) {
          if (event.keyCode == 13) {
            $scope.hideadditionalbenibits();
          }
        };
        $scope.showadditionalbenibits = function () {
          $scope.numLimit = $scope.rightListDetails.length;
          $scope.myFunction();
          $scope.showBenefits = true;
        };
        $scope.hideadditionalbenibits = function () {
          $scope.numLimit = 5;
          $scope.showBenefits = false;
          $scope.myFunction();
        };

        $scope.heightFunc = function (id1, id2, attribute) {
          var divHeight;
          var divHeight1;
          if (document.getElementById(id1) != null) {
            divHeight = document.getElementById(id1).offsetHeight;
          }
          if (document.getElementById(id2) != null) {
            divHeight1 = document.getElementById(id2).offsetHeight;
          }
          var text1 = "";
          var text2 = "";
          if (
            $scope.leftListDetails[attribute].value != null &&
            $scope.leftListDetails[attribute].value !== undefined
          ) {
            text1 = $scope.leftListDetails[attribute].value;
          }
          if (
            $scope.rightListDetails[attribute].value != null &&
            $scope.rightListDetails[attribute].value !== undefined
          ) {
            text2 = $scope.rightListDetails[attribute].value;
          }

          if (text1.length >= text2.length) {
            $scope.dynamicHeight = "auto";
            $scope.dynamicHeight1 = divHeight + "px";
          } else {
            $scope.dynamicHeight1 = "auto";
            $scope.dynamicHeight = divHeight1 + "px";
          }
        };
        $scope.getleftdivheight = function (id1, id2, attribute) {
          $scope.heightFunc(id1, id2, attribute);
          return {
            height: $scope.dynamicHeight,
          };
        };
        $scope.getrightdivheight = function (id1, id2, attribute) {
          $scope.heightFunc(id1, id2, attribute);
          return {
            height: $scope.dynamicHeight1,
          };
        };
        /*Accordian code*/

        $scope.listArray = [
          {
            listTitle: "Plan updates",
            updateTitle:
              "The following items will be removed from your account.",
            info2:
              "You may still be eligible for some of these features—you’ll just need to add them to your account again once the new line has been added.",
            updateDetail: [
              {
                planIcon: "./img/alert-outline.svg",
                planDetail: "T-Mobile ONE",
              },
              {
                planIcon: "./img/alert-outline.svg",
                planDetail: "Family Discount",
              },
              {
                planIcon: "./img/alert-outline.svg",
                planDetail:
                  "Netflix Premium $15.99 with Fam Allowances ($21 value)",
              },
            ],
          },
        ];
        var acc = document.getElementsByClassName("accordion");
        var count = 0;
        $scope.init = function () {
          if (acc && count == 0) {
            count++;
            setTimeout(function () {
              acc[0].classList.add("active");
              var panel = acc[0].nextElementSibling;
              panel.style.maxHeight = panel.scrollHeight + "px";
            }, 1000);
          }
        };
        $scope.toggle = function (index) {
          acc[index].classList.toggle("active");
          var panel = acc[index].nextElementSibling;
          if (panel.style.maxHeight) {
            panel.style.maxHeight = null;
          } else {
            panel.style.maxHeight = panel.scrollHeight + "px";
          }
        };
      },
    ])
     // Rate plan change ends
    .controller("deviceconditionCtrl", [
      "$scope",
      "$http",
      "$window",
      "$location",
      "$timeout",
      function ($scope, $http, $window, $location, $timeout) {
        
        $scope.phonecondtion_fullImage = "./img/phone-condition-desktop.png";
        $scope.promodetails=[
        "When you get your new device, send us the old one. Shipping’s on us.",
        "We’ll give you $350.00 credit on your bill (determined upon receipt of your device).",
        "Remove any passwords, location services, or locking features before sending."
        ]
       
      
        $scope.goBack = function () {
          $window.scrollTo(0, 0);
          $scope.hidden = true;
          $location.path("");
        };
  
      },
    ]);
 
})();
