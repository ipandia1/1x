(function () {
    'use strict';
    var app = angular.module('app', ['ngAnimate', 'ngAria', 'ngAria', 'ngCookies', 'ngMessages', 'ngSanitize', 'ngTouch', 'ui.bootstrap']);
    app.controller('generalController', ['$scope', function ($scope) {

    }])

        .controller('AccordionDemoCtrl', ['$scope', function ($scope) {
            $scope.oneAtATime = true;

            $scope.groups = [{
                title: 'Dynamic Group Header - 1',
                content: 'Dynamic Group Body - 1'
            }, {
                title: 'Dynamic Group Header - 2',
                content: 'Dynamic Group Body - 2'
            }];

            $scope.items = ['Item 1', 'Item 2', 'Item 3'];

            $scope.addItem = function () {
                var newItemNo = $scope.items.length + 1;
                $scope.items.push('Item ' + newItemNo);
            };

            $scope.status = {
                isCustomHeaderOpen: false,
                isFirstOpen: true,
                isFirstDisabled: false
            };
        }])


        .controller('AlertDemoCtrl', ['$scope', function ($scope) {
            $scope.alerts = [
                { type: 'danger', msg: 'Oh snap! Change a few things up and try submitting again.' },
                { type: 'success', msg: 'Well done! You successfully read this important alert message.' }
            ];

            $scope.addAlert = function () {
                $scope.alerts.push({ msg: 'Another alert!' });
            };

            $scope.closeAlert = function (index) {
                $scope.alerts.splice(index, 1);
            };
        }])

        .controller('ButtonsCtrl', ['$scope', function ($scope) {
            $scope.singleModel = 1;

            $scope.radioModel = 'Middle';

            $scope.checkModel = {
                left: false,
                middle: true,
                right: false
            };

            $scope.checkResults = [];

            $scope.$watchCollection('checkModel', function () {
                $scope.checkResults = [];
                angular.forEach($scope.checkModel, function (value, key) {
                    if (value) {
                        $scope.checkResults.push(key);
                    }
                });
            });
        }])

        .controller('CarouselDemoCtrl', ['$scope', function ($scope) {
            $scope.myInterval = 5000;
            $scope.noWrapSlides = false;
            $scope.active = 0;
            var slides = $scope.slides = [];
            var currIndex = 0;
            $scope.addSlide = function () {
                slides.push({
                    image: 'http://placehold.it/140x227',
                    //text: ['Nice image', 'Awesome photograph', 'That is so cool'][slides.length % 3],
                    id: currIndex++
                });
            };

            for (var i = 0; i < 3; i++) {
                $scope.addSlide();
            }

            $scope.next = function () {
                alert("next");
            }

        }])

        .controller('CarouselDemoCtrl1', ['$scope', function ($scope) {
            $scope.myInterval = 5000;
            $scope.noWrapSlides = false;
            $scope.active = 0;
            var slides = $scope.slides = [];
            var currIndex = 0;
            $scope.addSlide = function () {
                slides.push({
                    image: 'http://placehold.it/226x121',
                    //text: ['Nice image', 'Awesome photograph', 'That is so cool'][slides.length % 3],
                    id: currIndex++
                });
            };

            for (var i = 0; i < 3; i++) {
                $scope.addSlide();
            }

            $scope.next = function () {
                alert("next");
            }

        }])


        .controller('DropdownCtrl', ['$scope', '$log', function ($scope, $log) {
            $scope.status = {
                isopen: false
            };

            $scope.toggled = function (open) {
                $log.log('Dropdown is now: ', open);
            };

            $scope.toggleDropdown = function ($event) {
                $event.preventDefault();
                $event.stopPropagation();
                $scope.status.isopen = !$scope.status.isopen;
            };

            $scope.appendToEl = angular.element(document.querySelector('#dropdown-long-content'));
            $scope.selectedOption = "";
            $scope.change = function (name) {
                $scope.selectedOption = name;
            }
            $scope.onKeyDownDropDown = function (event) {
                if (event.keyCode == 13) {
                    $scope.status.isopen = !$scope.status.isopen;
                }
            }
            //    $scope.onBlurDropdown = function(evenr) {
            //     $scope.status.isopen = !$scope.status.isopen;
            //    }
        }])


        .controller('ModalDemoCtrl', ['$scope', '$uibModal', '$log', function ($scope, $uibModal, $log) {

            $scope.openOneButton = function () {

                var modalInstance = $uibModal.open({
                    animation: $scope.animationsEnabled,
                    templateUrl: 'myOneModalContent.html',
                    controller: 'ModalInstanceCtrl',
                    resolve: {
                        items: function () {
                            return $scope.items;
                        }
                    }
                });

                modalInstance.result.then(function (selectedItem) {
                    $scope.selected = selectedItem;
                }, function () {
                    $log.info('Modal dismissed at: ' + new Date());
                });
            };

            $scope.openTwoButton = function () {

                var modalInstance = $uibModal.open({
                    animation: $scope.animationsEnabled,
                    templateUrl: 'myTwoModalContent.html',
                    controller: 'ModalInstanceCtrl',
                    resolve: {
                        items: function () {
                            return $scope.items;
                        }
                    }
                });

                modalInstance.result.then(function (selectedItem) {
                    $scope.selected = selectedItem;
                }, function () {
                    $log.info('Modal dismissed at: ' + new Date());
                });
            };


        }])

        .controller('ModalInstanceCtrl', ['$scope', '$uibModalInstance', function ($scope, $uibModalInstance) {

            $scope.ok = function () {
                $uibModalInstance.close();
            };

            $scope.cancel = function () {
                $uibModalInstance.dismiss('cancel');
            };
        }])

        /*.controller('ProgressDemoCtrl', ['$scope', function($scope) {
            $scope.max = 200;
    
            $scope.random = function() {
                var value = Math.floor(Math.random() * 100 + 1);
                var type;
    
                if (value < 25) {
                    type = 'success';
                } else if (value < 50) {
                    type = 'info';
                } else if (value < 75) {
                    type = 'warning';
                } else {
                    type = 'danger';
                }
    
                $scope.showWarning = type === 'danger' || type === 'warning';
    
                $scope.dynamic = value;
                $scope.type = type;
            };
    
            $scope.random();
    
            $scope.randomStacked = function() {
                $scope.stacked = [];
                var types = ['success', 'info', 'warning', 'danger'];
    
                for (var i = 0, n = Math.floor(Math.random() * 4 + 1); i < n; i++) {
                    var index = Math.floor(Math.random() * 4);
                    $scope.stacked.push({
                        value: Math.floor(Math.random() * 30 + 1),
                        type: types[index]
                    });
                }
            };
    
            $scope.randomStacked();
        }])*/

        /*    .controller('TabsDemoCtrl', ['$scope', '$window', function($scope, $window) {
                $scope.tabs = [
                    { title: 'Dynamic Title 1', content: 'Dynamic content 1' },
                    { title: 'Dynamic Title 2', content: 'Dynamic content 2', disabled: true }
                ];
        
                $scope.model = {
                    name: 'Tabs'
                };
            }])*/

        /*    .controller('TooltipDemoCtrl', ['$scope', '$sce', function($scope, $sce) {
                $scope.dynamicTooltip = 'Hello, World!';
                $scope.dynamicTooltipText = 'dynamic';
                $scope.htmlTooltip = $sce.trustAsHtml('I\'ve been made <b>bold</b>!');
                $scope.placement = {
                    options: [
                        'top',
                        'top-left',
                        'top-right',
                        'bottom',
                        'bottom-left',
                        'bottom-right',
                        'left',
                        'left-top',
                        'left-bottom',
                        'right',
                        'right-top',
                        'right-bottom'
                    ],
                    selected: 'top'
                };
            }])*/

        .controller('TabsDemoCtrl', ['$scope', '$window', function ($scope, $window) {
            $scope.tabs = [
                { title: 'Dynamic Title 1', content: 'Dynamic content 1' },
                { title: 'Dynamic Title 2', content: 'Dynamic content 2', disabled: true }
            ];

            $scope.model = {
                name: 'Tabs'
            };
            /*Hyd SEZ*/
            //$scope.tab = 1;
            $scope.isSet1 = true;
            $scope.rating1 = {
                "color": "#e20074"
            }
            $scope.setTab = function (newTab) {
                if (newTab == 1) {
                    $scope.isSet1 = true;
                    $scope.isSet2 = false;
                    $scope.isSet3 = false;
                    $scope.isSet4 = false;
                    $scope.isSet5 = false;
                    $scope.rating1 = {
                        "color": "#e20074"
                    }
                    $scope.rating2 = {
                        "color": "#bebebe"
                    }
                    $scope.rating3 = {
                        "color": "#bebebe"
                    }
                    $scope.rating4 = {
                        "color": "#bebebe"
                    }
                    $scope.rating5 = {
                        "color": "#bebebe"
                    }

                }
                else if (newTab == 2) {
                    $scope.isSet2 = true;
                    $scope.isSet1 = false;
                    $scope.isSet3 = false;
                    $scope.isSet4 = false;
                    $scope.isSet5 = false;
                    $scope.rating2 = {
                        "color": "#e20074"
                    }
                    $scope.rating1 = {
                        "color": "#bebebe"
                    }
                    $scope.rating3 = {
                        "color": "#bebebe"
                    }
                    $scope.rating4 = {
                        "color": "#bebebe"
                    }
                    $scope.rating5 = {
                        "color": "#bebebe"
                    }
                }
                else if (newTab == 3) {
                    $scope.isSet3 = true;
                    $scope.isSet2 = false;
                    $scope.isSet1 = false;
                    $scope.isSet4 = false;
                    $scope.isSet5 = false;
                    $scope.rating3 = {
                        "color": "#e20074"
                    }
                    $scope.rating2 = {
                        "color": "#bebebe"
                    }
                    $scope.rating1 = {
                        "color": "#bebebe"
                    }
                    $scope.rating4 = {
                        "color": "#bebebe"
                    }
                    $scope.rating5 = {
                        "color": "#bebebe"
                    }
                }
                else if (newTab == 4) {
                    $scope.isSet4 = true;
                    $scope.isSet2 = false;
                    $scope.isSet3 = false;
                    $scope.isSet1 = false;
                    $scope.isSet5 = false;
                    $scope.rating4 = {
                        "color": "#e20074"
                    }
                    $scope.rating2 = {
                        "color": "#bebebe"
                    }
                    $scope.rating3 = {
                        "color": "#bebebe"
                    }
                    $scope.rating1 = {
                        "color": "#bebebe"
                    }
                    $scope.rating5 = {
                        "color": "#bebebe"
                    }
                }
                else if (newTab == 5) {
                    $scope.isSet5 = true;
                    $scope.isSet2 = false;
                    $scope.isSet3 = false;
                    $scope.isSet4 = false;
                    $scope.isSet1 = false;
                    $scope.rating5 = {
                        "color": "#e20074"
                    }
                    $scope.rating2 = {
                        "color": "#bebebe"
                    }
                    $scope.rating3 = {
                        "color": "#bebebe"
                    }
                    $scope.rating4 = {
                        "color": "#bebebe"
                    }
                    $scope.rating1 = {
                        "color": "#bebebe"
                    }
                }
            };



            //$scope.isSet = function(tabNum){
            //return $scope.tab === tabNum;
            //};

            $scope.maxSize = 5;
            $scope.bigTotalItems = 175;
            /*Hyd SEZ ends here*/

        }])

        /*copyed by koushik starts*/
        .controller('SeemoreCtrl1', ['$scope', function ($scope) {
            $scope.description = "With 3D Touch, Live Photos, 7000 series aluminum, A9 chip, advanced cameras, 4.7-inch Retina HD display and so much more, you'll see how - with IPhone 6s - the only thing that's changed is everything.With 3D Touch, Live Photos, 7000 series aluminum, A9 chip, advanced cameras, 4.7-inch Retina HD display and so much more, you'll see how - with IPhone 6s - the only thing that's changed is everything";

            /*$scope.legal="With 3D Touch, Live Photos, 7000 series aluminum, A9 chip, advanced cameras, 4.7-inch Retina HD display and so much more, you'll see how - with IPhone 6s - the only thing that's changed is everything.With 3D Touch, Live Photos, 7000 series aluminum, A9 chip, advanced cameras, 4.7-inch Retina HD display and so much more, you'll see how - with IPhone 6s - the only thing that's changed is everything"*/
            $scope.isAddALineEnabled = true;
            $scope.isLimitedAccess = true;

        }])
        /*copyed by koushik ends*/
        /* Offshore Team */
        .controller('CheckListProsCtrl', function ($scope) {
            $scope.pros = [
                'Great for texting',
                'Great web browsing',
                'Long Battery life',
                'Easy of use',
                'Great camera',
                'Useful apps',
                'Fun games',
                'WiFi',
                'GPS',
                '4G LTE',
                'Durable',
                'Large screen',
                'Touch screen',
                'Music',
                'Keyboard',
                'Speakerphone',
                'Volume level',
                'Processor',
                'Memory'
            ];
            $scope.maxProTextBox = 10;
            $scope.proTextBoxArray = []; //to store data of textbox
            var ZERO = 0;
            //create new textbox(ng-repeat logic) when clicked on last text box and is empty
            $scope.insertProTextBox = function (index) {
                var noOfTextBoxes = $scope.proTextBoxArray.length;
                if (noOfTextBoxes <= $scope.maxProTextBox) {
                    if (noOfTextBoxes == ZERO && $scope.proTextBoxArray[ZERO].length > ZERO) {
                        $scope.proTextBoxArray.push('');
                    }
                    else if (index == (noOfTextBoxes) && $scope.proTextBoxArray[noOfTextBoxes].length > ZERO) {
                        $scope.proTextBoxArray.push('');
                    }

                }
            }
        })

        .controller('CheckListConsCtrl', function ($scope) {
            $scope.cons = [
                'Expensive',
                'Hard to use',
                'Web browsing',
                'Battery',
                'Camera',
                'Screen',
                'Touchscreen',
                'Keyboard',
                'Heavy',
                'Bulky',
                'Speakerphone',
                'Volume level',
                'Processor',
                'Memory',
                'Buggy'
            ];
            $scope.maxConTextBox = 10;
            $scope.conTextBoxArray = [];//to store data of textbox
            var ZERO = 0;
            //create new textbox(ng-repeat logic) when clicked on last text box and is empty
            $scope.insertConTextBox = function (index) {
                var noOfTextBoxes = $scope.conTextBoxArray.length;
                if (noOfTextBoxes <= $scope.maxConTextBox) {
                    if (noOfTextBoxes == ZERO && $scope.conTextBoxArray[ZERO].length > ZERO) {
                        $scope.conTextBoxArray.push('');
                    }
                    else if (index == (noOfTextBoxes) && $scope.conTextBoxArray[noOfTextBoxes].length > ZERO) {
                        $scope.conTextBoxArray.push('');
                    }
                }
            }
        })

        .controller('myInfoReview', function ($scope) {
        })
        .controller('starCtrl', ['$scope', function ($scope) {
            $scope.rate = 0;
            $scope.EaseofUserate = 0;
            $scope.Batteryliferate = 0;
            $scope.Featuresrate = 0;
            $scope.CallQualityrate = 0;
            $scope.max = 5;
            $scope.isReadonly = false;

            var ratings = [{
                "name": "poor",
                "id": 1
            },
            {
                "name": "Fair",
                "id": 2
            },
            {
                "name": "Average",
                "id": 3
            }, {
                "name": "Good",
                "id": 4
            },
            {
                "name": "Excellent",
                "id": 5
            }];
            $scope.hoveringOver = function (value) {
                $scope.overStar = ratings[value - 1].name;
            };
            $scope.setvalue = function (index) {
                $scope.rate = index;
                $scope.test = ratings[$scope.rate - 1].name;
            }


            $scope.onEaseofUsehover = function (value) {
                $scope.EaseofUsemouseovervalue = ratings[value - 1].name;
            };

            $scope.setEaseofUsevalue = function (index) {
                $scope.EaseofUserate = index;
                $scope.EaseofUseratedvalue = ratings[$scope.EaseofUserate - 1].name;
            }
            $scope.onEaseofUsemouseout = function () {
                $scope.EaseofUsemouseovervalue = '';
                $scope.EaseofUseratedvalue = ratings[$scope.EaseofUserate - 1].name;

            };

            $scope.onBatterylifehover = function (value) {
                $scope.Batterylifemouseovervalue = ratings[value - 1].name;
            };

            $scope.setBatterylifevalue = function (index) {
                $scope.Batteryliferate = index;
                $scope.Batteryliferatedvalue = ratings[$scope.Batteryliferate - 1].name;
            }
            $scope.onBatterylifemouseout = function () {
                $scope.Batterylifemouseovervalue = '';
                $scope.Batteryliferatedvalue = ratings[$scope.Batteryliferate - 1].name;

            };

            $scope.onFeatureshover = function (value) {
                $scope.Featuresmouseovervalue = ratings[value - 1].name;
            };

            $scope.setFeaturesvalue = function (index) {
                $scope.Featuresrate = index;
                $scope.Featuresratedvalue = ratings[$scope.Featuresrate - 1].name;
            }
            $scope.onFeaturesmouseout = function () {
                $scope.Featuresmouseovervalue = '';
                $scope.Featuresratedvalue = ratings[$scope.Featuresrate - 1].name;

            };

            $scope.onCallQualityhover = function (value) {
                $scope.CallQualitymouseovervalue = ratings[value - 1].name;
            };

            $scope.setCallQualityvalue = function (index) {
                $scope.CallQualityrate = index;
                $scope.CallQualityratedvalue = ratings[$scope.CallQualityrate - 1].name;
            }
            $scope.onCallQualitymouseout = function () {
                $scope.CallQualitymouseovervalue = '';
                $scope.CallQualityratedvalue = ratings[$scope.CallQualityrate - 1].name;

            };


            $scope.ratingStates = [
                { stateOn: 'glyphicon-ok-sign', stateOff: 'glyphicon-ok-circle' },
                { stateOn: 'fa fa-star p-l-5-md p-l-5-lg fa-2x', stateOff: 'fa fa-star-o p-l-5-md p-l-5-lg fa-2x' },


            ];


        }])
        .controller('progressiveCtrl', ['$scope', function ($scope) {
            $scope.isReadonly = false;
            $scope.changeOnHover = true;
            $scope.maxValue = 5;
            $scope.star = 2;
            $scope.ratingValue = 1;
            $scope.currentindex = 1;

        }])
        /*Offshore Team */
        .controller('storeModalCtrl', ['$scope', function ($scope) {
            $scope.visible = false;
            $scope.isVisible = function () {
                $scope.visible = true;
                document.body.scrollTop = 0; /*for safari*/
                document.documentElement.scrollTop = 0; /*for chrome*/
            }
            $scope.close = function () {
                $scope.visible = false;
            };
            $scope.inputtextVal = 'Seattle, WA, USA';
            $scope.locationDetails = [
                { 'street1': 'Downtown - Seattle (430)', 'miles': '∙ 0.2 miles', 'street2': '1527 6th Ave, Seattle, WA, 98101', 'timing': 'Open now ∙ 10am-8pm', 'waitTime': 'In-store wait: 5min', 'text1': 'Appointment', 'text2': 'Get in line', 'text3': 'Call us', 'stock': 'In stock', 'isInStock': true },
                { 'street1': '4th & Main', 'miles': '∙ 0.7 miles', 'street2': '308 4th Ave S, Seattle, WA, 98104', 'timing': 'Open now ∙ 10am-8pm', 'waitTime': 'In-store wait: 5min', 'text1': 'Appointment', 'text2': 'Get in line', 'text3': 'Call us', 'stock': 'Only a few left', 'isInStock': true },
                { 'street1': 'Republican St', 'miles': '∙ 1.2 miles', 'street2': '431 Broadway E Ste F, Seattle, WA 98102', 'timing': 'Open now ∙ 10am-8pm', 'waitTime': 'In-store wait: 5min', 'stock': 'Out of stock', 'isInStock': false },
                { 'street1': 'Stone Way', 'miles': '∙ 0.7 miles', 'street2': '1216 N 45th St, Seattle, WA, 98103', 'timing': 'Open now ∙ 10am-8pm', 'waitTime': 'In-store wait: 5min', 'stock': 'Inventory status not available', 'isInStock': false },
                { 'street1': 'Broadway E & E Republican St', 'miles': '∙ 1.2 miles', 'street2': '431 Broadway E Ste F, Seattle, WA 98102', 'timing': 'Open now ∙ 10am-8pm', 'waitTime': 'In-store wait: 5min', 'text1': 'Appointment', 'text2': 'Get in line', 'text3': 'Call us', 'stock': 'In stock', 'isInStock': true }
            ];
            $scope.title = 'More stores nearby.';
            $scope.content1 = 'Only show stores with all products in stock.';
            $scope.content2 = 'Inventory status doesn’t hold or guarantee products (even with an appt.).';
        }])

    app.controller('cartController', ['$scope', function ($scope) {
        $scope.inputtextVal = 'JayElle@gmail.com';
        $scope.title1 = 'Save your cart for later.';
        $scope.title2 = 'Save your cart.';
        $scope.location = 'Downtown - Seattle (430)';
        $scope.visible = false;
        $scope.isVisible = function () {
            $scope.visible = true;
            document.body.scrollTop = 0; /*for safari*/
            document.documentElement.scrollTop = 0; /*for chrome*/
        };
        $scope.close = function () {
            $scope.visible = false;
        };
    }])
    app.controller('InStoreCtrl', ['$scope', function ($scope) {
        $scope.visible = false;
        $scope.isVisible = function () {
            $scope.visible = true;
            document.body.scrollTop = 0; /*for safari*/
            document.documentElement.scrollTop = 0; /*for chrome*/
        }
        $scope.close = function () {
            $scope.visible = false;
        };
        $scope.inputtextVal = 'Seattle, WA, USA';
        $scope.locationDetails = [
            { 'street1': 'Downtown - Seattle (430)', 'miles': '∙ 0.2 miles', 'street2': '1527 6th Ave, Seattle, WA, 98101', 'timing': 'Open now ∙ 10am-8pm', 'waitTime': 'In-store wait: 5min', 'text1': 'Appointment', 'text2': 'Get in line', 'text3': 'Call us', 'stock': 'In stock', 'isInStock': true }
        ];
        $scope.title = 'More stores nearby.';
        $scope.content1 = 'Only show stores with all products in stock.';
        $scope.content2 = 'Inventory status doesn’t hold or guarantee products (even with an appt.).';
    }])
    app.controller('accessoryCardCtrl', ['$scope', '$http', function ($scope, $http) {
        $scope.WithEIPCondition = true;
        $scope.stockDetails = false;
        $http.get("./json/acard.json").then(function (response) {
            if ($scope.WithEIPCondition) {
                $scope.details = response.data.WithEIP;
            } else {
                $scope.details = response.data.NoEIP;
            }
        });

    }])
        .filter('priceFilter', function () {
            return function (data, position) {
                var output;
                var price = data;
                var i = price.indexOf('.');
                var partOne = price.slice(0, i).trim();
                var partTwo = price.slice(i + 1, price.length).trim();
                if (position == "dollar") {
                    output = partOne;
                }
                else if (position == "cent") {
                    output = partTwo;
                }
                return output;
            }
        })
        .directive('accessoryCard', function () {
            return {
                restrict: 'E',
                scope: true,
                templateUrl: 'partials/accessory-card.html'
            };
        })
        .controller('modalController', ['$scope', '$http', function ($scope, $http) {
            $scope.byod = true;
            $scope.visible = false;

            $http.get("./json/modal.json").then(function (response) {
                $scope.productDetails = response.data
                if ($scope.byod) {
                    $scope.details = response.data.imagesB;
                } else {
                    $scope.details = response.data.imagesA;
                }
            });
                $scope.isVisible = function () {
                    $scope.visible = true;
                    document.body.scrollTop = 0; /*for safari*/
                    document.documentElement.scrollTop = 0; /*for chrome*/
                };
                $scope.close = function () {
                    $scope.visible = false;
                };
        }]);
    app.controller('mycartControlller', ['$scope', '$http', '$window', function ($scope, $http, $window) {
        $scope.checked = [false, false];
        $scope.mobile = false;
        $scope.init = function () {
            if (window.innerWidth < 768) {
             $scope.mobile = true;
            } else {
                $scope.mobile = false;
            }
        };
        angular.element($window).on('resize', function (scope) {
            $scope.$apply(function(){
                if ($window.innerWidth < 768) {
                    $scope.mobile = true;
                   } else {
                       $scope.mobile = false;
                   }
            })
        });
        $http.get("./json/myCart.json").then(function (response) {
            $scope.data = response.data;
            $scope.planDetails = response.data.plans;
            $scope.linesDevices = response.data.moreLineDevices;
            $scope.setBorder($scope.planDetails);
        });
        $http.get("./json/myCartUpdate.json").then(function (response) {
            $scope.mycartData = response.data;
            $scope.modalData = response.data.plans;
            $scope.planDetails = response.data.plans;
            $scope.linesDevices = response.data.moreLineDevices;
            $scope.lineDetails = $scope.planDetails.filter(function(obj){ 
                return obj.bestValue === true
            })[0];
            $scope.selectedOption = "";
            $scope.comparedData = $scope.modalData[0];
        });
        $scope.changeDropdown = function (name, i) {
         
            $scope.selectedOption = name;
            $scope.comparedData = $scope.modalData[i];
            const changeDropdown1 = document.getElementById('changeDropdown1');
            changeDropdown1.focus();
        }
        $scope.choose = function (name, i) {
            $scope.selectedOption2 = name;
            $scope.comparedData2 = $scope.modalData[i];
            const chooseDropdown = document.getElementById('chooseDropdown');
            chooseDropdown.focus();
        }
        $scope.compareEnable = function (name) {
            $scope.compare = true;
            const comparePlanSection = document.getElementById('comparePlanSection');
            setTimeout ("comparePlanSection.focus()", 100 );
         
        }
        $scope.activeIndex = function (data) {
            $scope.activeIndexItem = data;
        }
        $scope.setBorder = function (data) {
            for (var i = 0; i < data.length; i++) {
                if (data[i].bestValue) {
                    $scope.activeIndex(i);
                }
            }
        }
        $scope.onKeyDown = function (event, index) {
            if ((event.keyCode == 37 && !$scope.mobile) || (event.keyCode == 38 && !$scope.mobile)) {
                if ($scope.activeIndexItem == 0) {
                    $scope.activeIndexItem = $scope.planDetails.length - 1;
                }
                else {
                    $scope.activeIndexItem -= 1;
                }
                document.getElementsByClassName('plan-container')[$scope.activeIndexItem].focus();
            }
            if ((event.keyCode == 39 && !$scope.mobile) || (event.keyCode == 40 && !$scope.mobile)) {
                if ($scope.activeIndexItem == $scope.planDetails.length - 1) {
                    $scope.activeIndexItem = 0;
                }
                else {
                    $scope.activeIndexItem += 1;
                }
                document.getElementsByClassName('plan-container')[$scope.activeIndexItem].focus();
            }

        }
        $scope.onKeyDownCheckbox = function (event, index) {
            if (event.keyCode == 13) {
                $scope.checked[index] = !$scope.checked[index];
            }
        }

        $scope.modalOpen = function () {
            const body = document.getElementsByTagName('body')[0];
            body.classList.add('noScroll');
            $scope.modelTab();
        }
        $scope.modelFocus = function (e, status) {
            if (e.keyCode == 9 && !status && !$scope.selectedOption2) {
                          $scope.modelTab();
            }
            if (e.keyCode == 9 && $scope.compare) {
                const compareButton = document.getElementById('compareButton');
                if(compareButton){
                compareButton.focus();
                }
            }

        }
        $scope.modelTab = function () {
            const dialog = document.getElementById('modalBody');
            dialog.focus();
        }
        $scope.trapFocus = function (e) {
            if (e.keyCode == 9) {
                $scope.modelTab();
            }
        }
        $scope.close1 = function () {
            this.visible = false;
            const body = document.getElementsByTagName('body')[0];
            $scope.selectedOption2 = '';
            $scope.compare = false;
            body.classList.remove('noScroll');
        }

    }])
})();
